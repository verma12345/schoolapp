
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { Component } from 'react';
import { StatusBar, StyleSheet, View } from 'react-native';
import { Provider } from 'react-redux';
import Colors from './src/common/Colors';
import store from './src/redux_store/store';
import DashboardScreen from './src/screen/DashboardScreen';
import EmailVerficationScreen from './src/screen/EmailVerficationScreen ';
import LoginHomeScreen from './src/screen/LoginHomeScreen';
import LoginScreen from './src/screen/LoginScreen';
import MobileVerficationScreen from './src/screen/MobileVerficationScreen';
import ProfileScreen from './src/screen/ProfileScreen';
import SignupScreen from './src/screen/SignupScreen';
import SplashScreen from './src/screen/SplashScreen';
import SuccessScreen from './src/screen/SuccessScreen';
import VarificationScreen from './src/screen/VarificationScreen';
import StudentDetailsScreen from './src/screen/StudentDetailsScreen';
import ViewPagerScreen from './src/screen/ViewPagerScreen';
import LectureTimeTableScreen from './src/screen/LectureTimeTableScreen';
import TeacherScreen from './src/screen/TeacherScreen';
import AddStudentScreen from './src/screen/AddStudentScreen';
import { FeeScreen1Memo } from './src/screen/FeeScreen1';
import { FeeStructureScreenMemo } from './src/screen/FeeStructureScreen';
import PaymentScreen from './src/screen/PaymentScreen';
import InvoiceScreen from './src/screen/InvoiceScreen';
import MapTestingNew from './src/screen/MapTestingNew';
import ChangePasswordScreen from './src/screen/ChangePasswordScreen';
import ForgotPasswordScreen from './src/screen/ForgotPasswordScreen';
import MoreScreen from './src/screen/MoreScreen';
import MoreNoticationScreen from './src/screen/MoreNoticationScreen';
import { FeeSlipMemo } from './src/screen/FeeSlip';
import NotificationScreen from './src/screen/NotificationScreen';
import ReportCardListScreen from './src/screen/ReportCardListScreen';
import ReportCardScreen from './src/screen/ReportCardScreen';
import ClassTestScreen1 from './src/screen/ClassTestScreen1';
import GalleryScreen from './src/screen/GalleryScreen';
import ShowImageScreen from './src/screen/ShowImageScreen';
import SelectedGalleryList from './src/screen/SelectedGalleryList';
import EventScreen from './src/screen/EventScreen';
import EventInfo from './src/screen/EventInfo';
import SchoolDetailsScreen from './src/screen/SchoolDetailsScreen';
import { MyCalanderMemo } from './src/screen/MyCalanderMemo';
import InvoiceListScreen from './src/screen/InvoiceListScreen';
import ELearningScreen from './src/screen/ELearningScreen';
import ELearningSyllabus from './src/screen/ELearningSyllabus';
import VideoPlayerScreen from './src/video_player/screens/VideoPlayerScreen';
import VideoListScreen from './src/video_player/screens/VideoListScreen';
import AboutQuizScreen from './src/screen/AboutQuizScreen';






// console.disableYellowBox = true;
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

class App extends Component {
  constructor(props) {
    super(props);

  }



  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <StatusBar
            // translucent={true}
            backgroundColor={Colors.whiteText}
            barStyle="dark-content" />
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{ animationEnabled: false, headerShown: false }}
            >

              {/* <Stack.Screen name='Test' component={Test} /> */}

              <Stack.Screen name="SplashScreen" component={SplashScreen} />
              <Stack.Screen name='ViewPagerScreen' component={ViewPagerScreen} options={{ swipeEnabled: false }} />
              <Stack.Screen name="LoginHomeScreen" component={LoginHomeScreen} options={{ swipeEnabled: false }} />
              <Stack.Screen name="LoginScreen" component={LoginScreen} />
              <Stack.Screen name='DashboardScreen' component={DashboardScreen} />
              <Stack.Screen name='NotificationScreen' component={NotificationScreen} />

              <Stack.Screen name="SignupScreen" component={SignupScreen} />
              <Stack.Screen name="ChangePasswordScreen" component={ChangePasswordScreen} />
              <Stack.Screen name="ForgotPasswordScreen" component={ForgotPasswordScreen} />
              <Stack.Screen name="MobileVerficationScreen" component={MobileVerficationScreen} />
              <Stack.Screen name='EmailVerficationScreen' component={EmailVerficationScreen} />
              <Stack.Screen name="VarificationScreen" component={VarificationScreen} />
              <Stack.Screen name="SuccessScreen" component={SuccessScreen} />
              <Stack.Screen name='ProfileScreen' component={ProfileScreen} />
              <Stack.Screen name='StudentDetailsScreen' component={StudentDetailsScreen} />
              <Stack.Screen name='LectureTimeTableScreen' component={LectureTimeTableScreen} />
              <Stack.Screen name='TeacherScreen' component={TeacherScreen} />
              <Stack.Screen name='AddStudentScreen' component={AddStudentScreen} />
              <Stack.Screen name="FeeScreen1Memo" component={FeeScreen1Memo} />
              <Stack.Screen name='FeeSlipMemo' component={FeeSlipMemo} />
              <Stack.Screen name='FeeStructureScreenMemo' component={FeeStructureScreenMemo} />
              <Stack.Screen name='PaymentScreen' component={PaymentScreen} />
              <Stack.Screen name="InvoiceScreen" component={InvoiceScreen} />
              <Stack.Screen name='MapTestingNew' component={MapTestingNew} />
              <Stack.Screen name="MoreScreen" component={MoreScreen} />
              <Stack.Screen name="MoreNoticationScreen" component={MoreNoticationScreen} />
              <Stack.Screen name="GalleryScreen" component={GalleryScreen} />
              <Stack.Screen name="ShowImageScreen" component={ShowImageScreen} />
              <Stack.Screen name="SelectedGalleryList" component={SelectedGalleryList} />
              <Stack.Screen name="EventScreen" component={EventScreen} />
              <Stack.Screen name="EventInfo" component={EventInfo} />
              <Stack.Screen name="SchoolDetailsScreen" component={SchoolDetailsScreen} />
              <Stack.Screen name="InvoiceListScreen" component={InvoiceListScreen} />


              
              {/* JAswant */}
              <Stack.Screen name='ReportCardListScreen' component={ReportCardListScreen} />
              <Stack.Screen name='ReportCardScreen' component={ReportCardScreen} />
              <Stack.Screen name="ClassTestScreen1" component={ClassTestScreen1} />
             
              <Stack.Screen name="MyCalanderMemo" component={MyCalanderMemo} />
              <Stack.Screen name="ELearningScreen" component={ELearningScreen} />
              <Stack.Screen name="ELearningSyllabus" component={ELearningSyllabus} />
              <Stack.Screen name='VideoListScreen' component={VideoListScreen} />
              <Stack.Screen name='VideoPlayerScreen' component={VideoPlayerScreen} />
              <Stack.Screen name="AboutQuizScreen" component={AboutQuizScreen} />

              {/* 
              <Stack.Screen name="PagerMemo" component={PagerMemo} />
              <Stack.Screen name='TipComponent' component={TipComponent} />
              <Stack.Screen name='WSTesting' component={WSTesting} />
              <Stack.Screen name='ChooseLocation' component={ChooseLocation} />
              <Stack.Screen name='ReportCardScreen' component={ReportCardScreen} />
              <Stack.Screen name='FeeStructure' component={FeeStructure} />
              <Stack.Screen name='MapTestingNew' component={MapTestingNew} />
              <Stack.Screen name='TeacherScreen' component={TeacherScreen} />
              <Stack.Screen name='FeeStructure' component={FeeStructure} />


              <Stack.Screen name='FeeSlipMemo' component={FeeSlipMemo} />
              <Stack.Screen name='PaymentScreen' component={PaymentScreen} />


              <Stack.Screen name='FeeStructureScreenMemo' component={FeeStructureScreenMemo} />

              <Stack.Screen name="FeeScreen1Memo" component={FeeScreen1Memo} />
              <Stack.Screen name="InvoiceScreen" component={InvoiceScreen} />

              <Stack.Screen name="DemoUi" component={DemoUi} />
              <Stack.Screen name="QuizTestScreen" component={QuizTestScreen} />
              <Stack.Screen name='QuizSheetScreen' component={QuizSheetScreen} />
              <Stack.Screen name='QuizResultScreen' component={QuizResultScreen} />
              <Stack.Screen name='QuizSolutionListScreen' component={QuizSolutionListScreen} />
              <Stack.Screen name='QuizSolutionDescription' component={QuizSolutionDescription} />
           
              <Stack.Screen name='Testing' component={Testing} />

              <Stack.Screen name='QuizFinalSubmitPopUp' component={QuizFinalSubmitPopUp} />
              <Stack.Screen name='QuizLoginScreen' component={QuizLoginScreen} />
              <Stack.Screen name='QuizSignupScreen' component={QuizSignupScreen} />
              <Stack.Screen name='QuizDashboard' component={QuizDashboard} />
              <Stack.Screen name='QuizTncScreen' component={QuizTncScreen} />
              <Stack.Screen name='QuizSplashScreen' component={QuizSplashScreen} />
              */}


            </Stack.Navigator>
          </NavigationContainer>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default App;
