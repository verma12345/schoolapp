import { TouchableOpacity, Text, View, Image, StyleSheet, TextInput } from 'react-native';
import React from 'react';
import { getWidth } from '../common/Layout';
import Colors from '../common/Colors';
import OTPInput from 'react-native-otp';


const OTPTextInput = (props) => {
  return (
    <OTPInput
      value={props.value}
      onChange={props.onChange}
      tintColor="#FB6C6A"
      offTintColor="#BBBCBE"
      otpLength={5}
      cellStyle={{
        width: 55,
        height: 55,
        fontSize: 22,
        borderRadius: 14,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: "center",
        borderColor: "red",
        borderWidth: 0,

        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowRadius: 8.30,
        elevation: 10,
      }}
    />
  );
};

const styles = StyleSheet.create({

  myshadow: {
    shadowColor: "#1050e6",
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8.30,
    elevation: 10,
  },
});
export default OTPTextInput;
