import React from 'react';
import { Dimensions, FlatList, Image, PixelRatio, Text, TouchableOpacity, View } from 'react-native';
import { myStyle } from '../common/myStyle';


const FeeBillPopUp = (props) => {

    const renderItem = (dataItem) => {
        return (
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15,marginTop:12 }} >
                <Text style={[myStyle.textNormal, { fontSize: 14 }]} >{dataItem.item.fee_head } </Text>
                <Text style={[myStyle.textNormal, { fontSize: 14 }]} >{dataItem.item.amount} </Text>
            </View>
        )
    }

    return (
        <View
            style={{ width, height, backgroundColor: '#0004', position: 'absolute' }}
        >
            <View style={{ flex: 1, backgroundColor: 'white', marginHorizontal: PixelRatio.getPixelSizeForLayoutSize(10), borderRadius: 20, marginVertical: Dimensions.get('window').height / 7 }} >

                <Text style={[myStyle.textBold, { fontSize: 16, textAlign: 'center', paddingVertical: 10, marginTop: 15 }]} >
                    {'Fee Bill - March'}
                </Text>
                <View style={{ height: 40, width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >

                    <Image
                        source={require('../../assets/left.png')}
                        style={{ height: 30, width: 30, marginLeft: -7.5, resizeMode: 'contain', borderTopRightRadius: 100, tintColor: '#0004', borderBottomRightRadius: 100 }}
                    />

                    <Image
                        source={require('../../assets/line.png')}
                        style={{ flex: 1, resizeMode: 'contain', tintColor: 'lightgray' }}
                    />

                    <Image
                        source={require('../../assets/right.png')}
                        style={{ height: 30, width: 30, resizeMode: 'contain', tintColor: '#0004', marginRight: -7.5, borderTopLeftRadius: 100, borderBottomLeftRadius: 100 }}
                    />

                </View>

                <FlatList
                    data={props.bill}
                    renderItem={renderItem}
                />


                <TouchableOpacity
                    onPress={props.onOk}
                    // onPress={() => { debugLog(props.bill) }}
                    style={{ backgroundColor: '#000D83', width: '80%', alignSelf: 'center', marginVertical: 20, justifyContent: 'center', alignItems: 'center', paddingVertical: 8, borderRadius: 10 }}
                >
                    <Text style={[myStyle.textNormal, { fontSize: 18, color: 'white' }]} >{'OK'}</Text>
                </TouchableOpacity>
            </View>


        </View>
    )
}

const { width, height } = Dimensions.get("window");

export default FeeBillPopUp;
