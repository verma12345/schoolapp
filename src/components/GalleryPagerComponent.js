import React, { useState, useRef, Component } from 'react';
import { StyleSheet, View, Platform, Text, TouchableOpacity, Image, ImageBackground, Dimensions } from 'react-native';
import Colors from '../common/Colors';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PagerView from 'react-native-pager-view';

class GalleryPagerComponent extends Component {


    render() {
        let row = this.props.pager_list.map((item, index) => {
            return (
                <View
                    key={index}
                    style={[{
                        flex: 1,
                        backgroundColor: Colors.backgroundColor,
                        alignItems: 'center',
                        justifyContent: 'center',
                        // borderRadius:10
                    }]}
                >
                    <Image
                        source={item.image}
                        style={{ marginTop: 10, borderRadius: 20, flex: 1 }}
                    />


                </View>
            )
        })


        return (

            < View style={{ flex: 1 }} >

                <PagerView
                    onPageSelected={this.props.onPageSelected}
                    style={styles.viewPager}
                    initialPage={0}>

                    {row}

                </PagerView>

            </View >
        );
    }


};

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    viewPager: {
        flex: 1
    },
});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jaswant = state.indexReducerJaswant;
    const akshita = state.indexReducerAkshita;

    return {
        gallery_pager: akshita.gallery_pager,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setUserId: (user_id) => setUserId(user_id),

            // setWorkoutOfTheWeekData: (res) => setWorkoutOfTheWeekData(res),
            // hitWorkoutOfTheWeek: (param) => hitWorkoutOfTheWeek(param),



        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(GalleryPagerComponent);
