import React, { useState } from 'react'
import { Button, Dimensions, Image, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

const { height, width } = Dimensions.get('window')

const TipComponent = (props) => {

    const [isTip, setTip] = useState(false)

    return (
        <View style={{
            backgroundColor: Colors.backgroundColor,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: 'row',
            paddingHorizontal: 15,
            // width, height,
            elevation: 12
        }}>
            <TouchableOpacity
                onPress={() => setTip(!isTip)}
                // style={{ marginRight: 20, backgroundColor: 'red', padding: 10 }}
            >
                <Text style={[myStyle.textBold]}>
                    {'School App'}
                </Text>
            </TouchableOpacity>


            {
                isTip ?
                    <TouchableOpacity
                        onPress={() => setTip(!isTip)}
                        style={{
                            // height,
                            // width,
                            backgroundColor: '#0008',
                            position: 'absolute',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                        <View style={{
                            // height: 100,
                            // width: 200,
                            // backgroundColor: 'white',
                            marginRight: 100,
                            marginBottom: 150,
                            borderRadius: 20
                        }}>
                            <View style={{
                                backgroundColor: Colors.whiteText,
                                flex: 1,
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 20
                            }}>
                                <Text>{'hlwwwwwwwwwww'} </Text>
                            </View>
                            <View style={{
                                backgroundColor: Colors.whiteText,
                                // backgroundColor: 'red',
                                flexDirection: 'row',
                                justifyContent: 'center',
                                alignItems: 'center',
                                height: 20
                            }}>
                                <View style={{
                                    // backgroundColor: Colors.whiteText,
                                    backgroundColor: '#0008',
                                    borderTopRightRadius: 30,
                                    height: 20, width: '80%'

                                }} />
                                <View style={{
                                    backgroundColor: Colors.whiteText,
                                    backgroundColor: '#0008',
                                    height: 20, width: '20%',
                                    borderTopLeftRadius: 30,
                                }} />


                            </View>
                        </View>

                        {/* ============================================================ */}

                    </TouchableOpacity>

                    :
                    null
            }

        </View>
    )
}


export default TipComponent;
