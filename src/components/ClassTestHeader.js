import React, { useState } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';



export const ClassTestHeader = (props) => {
    const [event, setEvent] = useState({})
    const onLayout = (event) => {
        setEvent(event.nativeEvent)
    }


    return (

        <View style={{
            elevation: 3,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            alignItems: 'center',
            backgroundColor: Colors.whiteText,
            paddingVertical: 15
        }} >

            <View style={[{
                // height: 80,
                width: '100%',
                justifyContent: "center",
                alignItems: "center",
                flexDirection: 'row',
                paddingHorizontal: 15,
            }, props.style]}>
                <TouchableOpacity
                    onPress={props.onPressBack}
                    style={{ position: 'absolute', left: 10, padding: 15 }}
                >
                    <Image
                        source={require('../../assets/back.png')}
                        style={{ height: 16, width: 16, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>

                <View style={{
                    justifyContent: "space-between",
                    alignItems: "center",
                    flexDirection: 'row',
                }} >
                    <Image
                        source={props.icon1}
                        style={{ height: 28, width: 28, marginRight: 10, borderRadius: 11 }}
                    />
                    <Text style={[myStyle.textBold, { color: Colors.textDarkColor, fontSize: 14, }]}>{props.txt}</Text>
                </View>

                {props.isMenu != true ?
                    null
                    : <TouchableOpacity
                        onLayout={onLayout}
                        onPress={() => props.onPressMenu(event)}
                        style={{ position: 'absolute', right: 10,padding:10 }} >
                        <Image
                            source={props.icon2}
                            style={{ height: 25, width: 25 }}
                        />
                    </TouchableOpacity>
                }

            </View>
            {
                props.year ?
                    <Text style={[myStyle.textBold, {marginTop:16, color: Colors.textDarkColor, fontSize: 12, }]}>{props.year}</Text>
                    : null
            }
        </View>


    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20
    },
    btn: {
        height: 35,
        width: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})