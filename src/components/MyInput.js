import React from "react";
import { Image, PixelRatio, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import TextInput from 'react-native-material-textinput'

const MyInput = (props) => {

    return (
        props.type == 'password' ?
            <View style={[styles.view5,props.style]}>

                <TextInput
                    width={200}
                    label='Your Password'
                    value={props.value}
                    underlineHeight={1}
                    underlineColor={Colors.backgroundColor}
                    labelActiveColor={Colors.placeHolderColor}
                    underlineActiveColor={Colors.backgroundColor}
                    onChangeText={props.onChangeText}
                    secureTextEntry={props.showpass}
                    maxLength={15}
                />
                <View>

                    <TouchableOpacity onPress={props.onPress}
                        style={{ alignSelf:'center', }}
                    >
                        {
                            props.showpass ?
                                <Image
                                    style={{ height: 20, width: 20, tintColor: 'gray' }}
                                    source={require('../../assets/show.png')}
                                />
                                : <Image
                                    style={{ height: 20, width: 20, tintColor: 'gray' }}
                                    source={require('../../assets/passhide.png')}
                                />
                        }


                    </TouchableOpacity>
                </View>

            </View>
            :
            <View style={[styles.container, props.style]}>
                <TextInput
                    label={props.label}
                    value={props.value}
                    underlineHeight={1}
                    underlineColor={Colors.backgroundColor}
                    labelActiveColor={Colors.placeHolderColor}
                    underlineActiveColor={Colors.backgroundColor}
                    onChangeText={props.onChangeText}
                    keyboardType={props.keyboardType}
                    maxLength={props.maxLength}
                    secureTextEntry={props.secureTextEntry}
                    

                />
            </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,
        elevation: 1,
        paddingHorizontal: 30,

    },
    view5: {
        height: 50,
        flex: 1,
        backgroundColor: Colors.textinputcolor,
        paddingHorizontal: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 0.1,
        borderRadius: 20,
    },
})
export default MyInput;