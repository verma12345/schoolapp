

import React, { useCallback, useRef, useMemo } from 'react';
import { StyleSheet, View, Text, Button, TouchableOpacity, Image, PixelRatio } from 'react-native';
import BottomSheet, { BottomSheetFlatList } from '@gorhom/bottom-sheet';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setFeed, setFeedIsActive, setNotification, setNotificationIsActive } from '../redux_store/actions/indexActionsJaswant';

const NotificationFeedScreen = (props) => {
    // hooks


    const snapPoints = useMemo(() => ['25%', '50%', '90%'], []);

    // callbacks
    const handleSheetChange = useCallback(index => {
        console.log('handleSheetChange', index);
    }, []);




    const _onpressNotification = () => {
        props.setNotificationIsActive(true)
    }

    const _onpressFeed = () => {
        props.setNotificationIsActive(false)
    }

    const _renderFeed = (dataItem) => {
        return (
            <View style={{}} >
                {dataItem.index == 0 ?
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', paddingLeft: 15, borderTopLeftRadius: 50, borderTopRightRadius: 50 }}>
                        <TouchableOpacity onPress={() => _onpressNotification()}>
                            <Text style={[myStyle.textBold, { fontSize: 16, color: props.notificationIsActive ? Colors.textDarkColor : Colors.grayColor }]}>{"Notification"}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ paddingLeft: 20 }}
                            onPress={() => _onpressFeed()}
                        >
                            <Text style={[myStyle.textBold, { fontSize: 16, color: props.notificationIsActive ? Colors.grayColor : Colors.textDarkColor }]}>{"Feed"}</Text>
                        </TouchableOpacity>
                    </View>
                    : null
                }
                <View style={{ flexDirection: 'row', marginTop: 12, marginHorizontal: 20, }}>

                    <View>
                        <Image
                            style={{
                                height: PixelRatio.getPixelSizeForLayoutSize(15),
                                width: PixelRatio.getPixelSizeForLayoutSize(15),
                                borderRadius: 15
                            }}
                            source={dataItem.item.profile}
                        />
                    </View>
                    <View style={{ paddingLeft: 20 }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>{dataItem.item.name}</Text>
                        <View style={{ flexDirection: 'row', }}>
                            <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>{dataItem.item.subject}{','}</Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12, paddingLeft: 10, color: Colors.grayColor }]}>{dataItem.item.date}</Text>
                        </View>
                        <Text numberOfLines={1} style={[myStyle.textNormal, { marginTop: 8, width: '70%', fontSize: 12, color: Colors.textDarkColor }]}>{dataItem.item.message}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                            <Image
                                style={{ height: 20, width: 20 }}
                                source={dataItem.item.commenticon}
                            />
                            <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12, paddingLeft: 10 }]}>{dataItem.item.commentcount}</Text>
                        </View>
                    </View>
                </View>

                <View style={{ height: 1, marginTop: 20, flex: 1, marginLeft: 20, backgroundColor: 'lightgray' }} />

            </View>
        )
    }

    const _renderNotification = (dataItem) => {
        return (
            <View style={{ width: '100%', }} >
                {dataItem.index == 0 ?
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', paddingHorizontal: 20, borderTopLeftRadius: 50, borderTopRightRadius: 50 }}>
                        <TouchableOpacity onPress={() => _onpressNotification()}>
                            <Text style={[myStyle.textBold, { fontSize: 16, color: props.notificationIsActive ? Colors.textDarkColor : Colors.grayColor }]}>{"Notification"}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={{ paddingLeft: 20 }}
                            onPress={() => _onpressFeed()}>
                            <Text style={[myStyle.textBold, { fontSize: 16, color: props.notificationIsActive ? Colors.grayColor : Colors.textDarkColor }]}>{"Feed"}</Text>
                        </TouchableOpacity>
                    </View>
                    : null}
                <View style={myStyle.container} >
                    <View style={{ marginVertical: 10, paddingHorizontal: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={[myStyle.textBold, { fontSize: 13 }]}>{dataItem.item.title}</Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12, color: 'gray' }]}>{dataItem.item.time_ago}</Text>
                        </View>

                        {/* <Text style={[myStyle.textNormal, { color: 'gray', fontSize: 12 }]}>{dataItem.item.title}</Text> */}
                        <Text style={[myStyle.textBold, { color: 'gray', fontSize: 12 }]}>{dataItem.item.message}</Text>
                    </View>
                    <View style={{ height: 1, marginVertical: 10, flex: 1, marginLeft: 20, backgroundColor: 'lightgray' }} />
                </View>
            </View>
        )
    }
console.log(props.notificationIsActive);
    return (

        <BottomSheet
            snapPoints={snapPoints}
            onChange={handleSheetChange}
            style={[myStyle.myshadow, { backgroundColor: Colors.whiteText, elevation: 5, borderTopLeftRadius: 20, borderTopRightRadius: 20 }]}
        >
            {
                props.notificationIsActive ?
                    <BottomSheetFlatList
                        data={props.notification}
                        keyExtractor={(item, index) => 'key' + index}
                        renderItem={_renderNotification}
                        contentContainerStyle={styles.contentContainer}
                    /> 
                    :
                    <BottomSheetFlatList
                        data={props.feed}
                        keyExtractor={(item, index) => 'key' + index}
                        renderItem={_renderFeed}
                        contentContainerStyle={styles.contentContainer}
                    // backdropComponent={styles.contentContainer}
                    />

            }
        </BottomSheet>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingTop: 200,
    },
    contentContainer: {
        backgroundColor: 'white',
    },
    itemContainer: {
        padding: 6,
        margin: 6,
        backgroundColor: '#eee',
    },
});



const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jas = state.indexReducerJaswant

    return {
        notification: jas.notification,
        feed: jas.feed,
        feedIsActive: jas.feedIsActive,
        notificationIsActive: jas.notificationIsActive
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setNotification: (notification) => setNotification(notification),
        setFeed: (feed) => setFeed(feed),
        setNotificationIsActive: (notificationIsActive) => setNotificationIsActive(notificationIsActive),
        setFeedIsActive: (feedIsActive) => setFeedIsActive(feedIsActive)
    }, dispatch)
}



export default connect(mapStateToProps, mapDispatchToProps)(NotificationFeedScreen)
