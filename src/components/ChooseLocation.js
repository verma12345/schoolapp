import React, { useState } from 'react'
import { Dimensions, Image, SafeAreaView, ScrollView, StatusBar, Text, TouchableOpacity, View } from "react-native"
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Address } from '../components/Address'
import { CustomeButtton } from '../components/CustomeButtton'
// import { setDropLoc, setPickUpLoc } from '../readux/actions/indexActaion'
import { useNavigation } from '@react-navigation/native'
import colors from '../styles/colors'
import Colors from '../common/Colors'
import { myStyle } from '../common/myStyle'


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const ChooseLocation = (props) => {
    const navigation = useNavigation()
    const [state, setState] = useState({
        startingCords: {},
        destinationCords: {}
    })
    const { startingCords, destinationCords } = state

    const _onPressDone = () => {
        props.route.params.getCordinates({
            startingCords,
            destinationCords
        })
        navigation.goBack()
    }


    const fetchAddressCordinates = (data, details) => {
        setState({
            ...state, startingCords: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng
            }
        })

    }

    const fetchDestinationCordinates = (data, details) => {
        setState({
            ...state, destinationCords: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng
            }
        })
    }



    return (
        <SafeAreaView style={{ flex: 1,backgroundColor:Colors.themeColor  }}>
            <View style={{ flex: 1 }} >
                <View style={{
                    height: 50,
                    backgroundColor: colors.themeColor,
                    elevation: 5,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                }} >
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}
                        style={{ position: 'absolute',left:10 }} >
                        <Image
                            style={{
                                height: 25, width: 25,
                                resizeMode: 'contain', tintColor: Colors.grayColor
                            }}
                            source={require('../../assets/back.png')}
                        />
                    </TouchableOpacity>

                    <Text style={[myStyle.textBold]} >{"Choose Location"}</Text>
                </View>



                <ScrollView
                    keyboardShouldPersistTaps='always'
                    style={{ flext:1, marginTop: 50 }}
                // contentContainerStyle={{ flex: 1 }}
                >

                    <Address
                        placeholder='Enter pickup location'
                        fetchAddress={fetchAddressCordinates}
                        containerStyle={{marginHorizontal:10}}
                    />
                    <Address
                        placeholder='Enter destination location'
                        fetchAddress={fetchDestinationCordinates}
                        containerStyle={{marginHorizontal:10}}

                    />



                    <CustomeButtton
                        title="Done"
                        onPress={() => _onPressDone()}
                        style={{marginHorizontal:10}}

                    />

                </ScrollView>

            </View>
        </SafeAreaView>
    )
}


const mapStateToProps = (state) => {
    const common = state.indexReducer;

    return {
        // dropLoca: common.dropLoca,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setDropLoc: (login_id) => setDropLoc(login_id),
            // setPickUpLoc: (cor) => setPickUpLoc(cor),
        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ChooseLocation);