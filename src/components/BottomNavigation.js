import { TouchableOpacity, Text, View, StyleSheet, Image, PermissionsAndroid, } from 'react-native';
import React from 'react';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { debugLog } from '../common/Constants';

const BottomNavigation = (props) => {


    const _onLocation = () => {
        requestLocationPermission()

    }

    const requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
                {
                    title: "Loation Permission",
                    message: "Please allow access map ",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                debugLog("Map Access permission success!");
                props.navigation.navigate('MapTestingNew')
            } else {
                debugLog("Location permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };


    return (

        <View style={[styles.myshadow, {
            height: 55,
            backgroundColor: '#FFFFFF',
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20
        }]}>
            {/* <Pressable
                android_ripple={{ radius: 100, color: Colors.yellowColor }} */}

            < TouchableOpacity
                onPress={() => props.navigation.navigate('DashboardScreen')}
                style={[styles.touch,
                { marginTop: props.isDashboard ? 0 : 0 }]}
            >
                <Image
                    source={props.isDashboard ? require('../../assets/home2.png') : require('../../assets/home.png')}
                    style={[styles.imgStyle, { resizeMode: 'contain' }]}
                />

            </TouchableOpacity>


            <TouchableOpacity
                onPress={() => _onLocation()}
                style={[styles.touch,
                { marginTop: props.isActivity ? 1 : 0 }]}>
                <Image
                    source={props.isMap ? require('../../assets/location2.png') : require('../../assets/location.png')}
                    style={[styles.imgStyle,]}
                />

            </TouchableOpacity>



            <TouchableOpacity style={[styles.touch, { marginTop: props.isMore ? 1 : 0 }]}
                onPress={() => props.navigation.navigate('MoreScreen')}
            >
                <Image
                    source={props.isMore ? require('../../assets/category2.png') : require('../../assets/category.png')}
                    style={[styles.imgStyle,]}
                />

            </TouchableOpacity>


            <TouchableOpacity style={[styles.touch, { marginTop: props.isProfile ? 1 : 0 }]}
                onPress={() => props.navigation.navigate('ProfileScreen')}
            >
                <Image
                    source={props.isProfile ? require('../../assets/profile_map.png') : require('../../assets/profile1.png')}
                    style={[styles.imgStyle, { tintColor: Colors.blue, height: 30 }]}
                />


            </TouchableOpacity>
        </View>
    );
};
const styles = StyleSheet.create({

    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
    },
    imgStyle: {
        width: 25,
        height: 25,
        // resizeMode:'cover'
    },
    txtStyle: {
        fontSize: 11,
        fontWeight: '500',
        marginTop: 5,
        fontFamily: Font.robotoSourceSansProBold
    },
    touch: {
        height: 50,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
export default BottomNavigation;