import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

export const EventHeader = (props) => {
    return (
        <View style={{
            backgroundColor: Colors.whiteText,
            borderBottomLeftRadius: 18,
            borderBottomRightRadius: 18,
            elevation: 2,
            paddingVertical: 8

        }}>

            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                <TouchableOpacity onPress={props.onPressBack}               >
                    <Image
                        source={require('../../assets/back.png')}
                        style={{ height: 18, width: 18, resizeMode: 'contain', marginLeft: 20, marginTop: 10 }}
                    />
                </TouchableOpacity>

                <Text style={[myStyle.textBold, { color: Colors.textDarkColor, fontSize: 16, marginLeft: 10 }]}>{props.title}</Text>

                <Image
                    source={require('../../assets/student2.png')}
                    style={{ height: 28, width: 28, borderRadius: 12,marginRight:25 }}
                />
            </View>

            <TouchableOpacity>
                <Image

                />
            </TouchableOpacity>

            <View style={{
                flexDirection: 'row',
                width: '60%',
                alignSelf: 'center',
                backgroundColor: Colors.whiteText,
                borderWidth: 1,
                borderRadius: 12,
                borderColor: Colors.lightGray,
                marginTop: 18,
                marginBottom: 20
            }}>
                <TouchableOpacity
                    onPress={props.onPressUpcoming}
                    style={{ flex: 1, alignItems: 'center', paddingVertical: 6, borderRadius: 12, justifyContent: 'center', paddingHorizontal: 10, backgroundColor: props.is_upcoming ? Colors.blue : Colors.whiteText }}>
                    <Text style={[myStyle.textNormal], { color: props.is_upcoming ? "white" : "black", fontSize: 13 }}>{'Upcoming'} </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={props.onPressPast}
                    style={{ flex: 1, alignItems: 'center', paddingVertical: 6, borderRadius: 12, justifyContent: 'center', backgroundColor: props.is_past ? Colors.blue : Colors.whiteText, paddingHorizontal: 15, }}>
                    <Text style={[myStyle.textNormal], { color: props.is_past ? "white" : "black", fontSize: 13 }}>{'Past'} </Text>
                </TouchableOpacity>


            </View>
        </View>
    )
}
