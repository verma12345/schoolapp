import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { myStyle } from '../common/myStyle';


const Header = (props) => {
    return (
        <View style={[myStyle.myshadow,{
            height: 50,
            width: '100%',
            backgroundColor: Colors.backgroundColor,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: 'row',
            paddingHorizontal: 15
        }, props.style]}>
            <TouchableOpacity
                onPress={props.onPressBack}
                style={{ position: 'absolute', left: 0, padding: 15 }}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{ height: 17, width: 17, resizeMode: 'contain' }}
                />
            </TouchableOpacity>

            {
                props.title ?
                    <Text style={[myStyle.textBold, { fontSize: 14 },props.titleStyle]}>
                        {props.title}
                    </Text>
                    :
                    null
            }
            {props.children}

        </View>
    )
}


export default Header;
