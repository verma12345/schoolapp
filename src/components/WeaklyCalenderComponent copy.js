import React, { Component } from 'react';
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';
import { setArr, setDayName, setMonthName, setTemp, setWeakDays, setYear } from '../redux_store/actions/indexActionsRajpoot';
import indexReducerRajpoot from '../redux_store/reducers/indexReducerRajpoot';



class WeaklyCalenderComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }

        this.weakdays = 0
        this.indexcount = 0
        this.days = ''
        this.daycounter = 1
    }




    _renderItem = (dataItem) => {
        let newdataitem = dataItem.item

        this.weakdays = this.weakdays
        if (this.weakdays == 6) {
            this.weakdays = 0
        }
        else {
            this.weakdays = this.weakdays + 1
        }

        if (dataItem.index == 0) {
            this.weakdays = 0
        }

        this.days = this.props.weakdays[this.weakdays]

        // debugLog(this.weakdays)
        // debugLog(this.days)

        return (
            <TouchableOpacity style={{ height: 100, width: 50, marginTop: 20,marginBottom:10 }} onPress={()=>alert("comming soon ...")}>
                <Text style={[myStyle.textBold,{ textAlign: 'center', fontSize: 14,color:Colors.grayColor }]}>{this.days}</Text>
                <Text style={[myStyle.textBold,{ marginTop: 20, textAlign: 'center', fontSize: 14 }]}>{newdataitem[0].day}</Text>
            </TouchableOpacity>
        )


    }

    render() {
        return (
            <View style={styles.container} >
                <View style={{ alignItems: 'center', flexDirection: 'row', width: '100%',  marginTop: 30 }}>
                   <TouchableOpacity onPress={()=> this.props.navigation.goBack()}>
                   <Image
                        style={{ height: 15, width: 15 }}
                        source={require('../../assets/back.png')}
                    />
                   </TouchableOpacity>
                    <View style={{ width: '90%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <Image
                            style={{ height: 30, width: 30, borderRadius: 12 }}
                            source={require('../../assets/profile.png')}
                        />
                        <Text style={[myStyle.textBold, { fontSize: 16, paddingLeft: 20 }]}>{'TimeTable'}</Text>

                    </View>
                </View>
                <View style={{flexDirection:'row',marginTop:30}}>
                    <Text style={[myStyle.textBold,{fontSize:16}]}>{this.props.monthname}</Text>
                    <Text style={[myStyle.textBold,{fontSize:16,paddingLeft:5}]}>{this.props.year}</Text>
                </View>

                <FlatList
                    data={this.props.month > 11 ? this.props.arr[0].month : this.props.arr[this.props.month].month}
                    horizontal={true}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index + 'key'}

                />




            </View>
        )
    }



}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        backgroundColor: Colors.backgroundColor,
        paddingHorizontal: 20,
        borderRadius:20,
        elevation:2

    },

})

const mapStateToProps = (state) => {
    let rajpoot = state.indexReducerRajpoot;
    return {
        arr: rajpoot.arr,
        month: rajpoot.month,
        monthname: rajpoot.monthname,
        year: rajpoot.year,
        weakdays: rajpoot.weakdays,
        temp: rajpoot.temp


    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setArr: (arr) => setArr(arr),
        setMonth: (month) => setMonth(month),
        setMonthName: (monthname) => setMonthName(monthname),
        setYear: (year) => setYear(year),
        setWeakDays: (weakdays) => setWeakDays(weakdays),
        setDayName: (dayname) => setDayName(dayname),
        setTemp: (temp) => setTemp(temp)


    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(WeaklyCalenderComponent)