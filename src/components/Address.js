import React, { } from "react"
import { StyleSheet, View } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { GOOGLE_MAPS_APIKEY } from "../common/Constants";
import colors from "../styles/colors";
import Font from "../styles/Font";

export const Address = (props) => {

    const onPressAddress = (data, details) => {
        props.fetchAddress(data, details)
    }

    return (
        <View style={[{ flex: 1, marginVertical: 5 },props.containerStyle]} >
            <GooglePlacesAutocomplete
                placeholder={props.placeholder}
                fetchDetails={true}
                onPress={onPressAddress}
                query={{
                    key: GOOGLE_MAPS_APIKEY,
                    language: 'en',
                }}
                styles={{
                    textInputContainer: styles.textInputContainer,
                    textInput: styles.textInput,
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    textInputContainer: {
        backgroundColor: colors.themeColor,
        borderRadius: 10,
        elevation: 10
    },
    textInput: {
        color: colors.grayColor,
        height: 50,
        fontSize: 20,
        fontFamily: Font.Roboto
    }
})
