import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';


export const SignupHeader = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.view2}>
                <TouchableOpacity
                    style={styles.btn}
                    // onPress={props.navigation.goBack()}
                    onPress={() => props.navigation.goBack()}
                >
                    <Image
                        source={props.backpic}
                        style={{ height: 13, width: 13 }}
                    />
                </TouchableOpacity>
                <TouchableOpacity style={{  }}>
                    <Text style={[myStyle.textNormal,{ color:Colors.grayColor,fontSize:14,}]}>{'Skip'}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.view1}>
                <Text style={[myStyle.textBold,{fontSize: 28 }]}>{props.txtheading}</Text>
                <View style={{ width: '80%' }}>
                    <Text style={[myStyle.textNormal,{ color:Colors.grayColor, paddingTop: 20,fontSize:12}]}>{props.txt}</Text>

                </View>
            </View>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20,
        // paddingLeft:40
    },
    btn: {
        height: 35,
        width: 35,
        elevation:2,
        backgroundColor:Colors.backgroundColor,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    view1: {
        marginTop: 30
    },
    view2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        // paddingLeft: 40,
        alignItems: 'center'
    }
})