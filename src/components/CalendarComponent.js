import moment from "moment";
import React, { useState } from "react"
import { Text, View } from "react-native"
import CalendarPicker from 'react-native-calendar-picker';
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";

const CalendarComponent = (props) => {
    const [selectedStartDate, setSelectStartDate] = useState(null)
    const [selectedEndDate, setSelectedEndDate] = useState(null)
    const [check, setcheck] = useState(false)

    // const onDateChange = (date, type) => {
    //     if (type === 'END_DATE') {
    //         const formatedDate = moment(date).format('DD/MM/YYYY');
    //         setSelectedEndDate(formatedDate)
    //         setSelectStartDate(null)

    //     } else {
    //         const formatedDate = moment(date).format('DD/MM/YYYY');
    //         setSelectStartDate(formatedDate)
    //         setSelectedEndDate(null)
    //     }
    // }

    onDateChange = () => {

        var dt = new Date();
        // var day=dt.getDay();
        var month = dt.getMonth();
        var year = dt.getFullYear();
        daysInMonth = new Date(year, month, 0).getDate();
        for (var i = 0; i < daysInMonth; i++) {
            // console.log(i,)

            if (i == day.add(i, 'day').isSame(today, 'month')) {
                customDatesStyles.push({

                    date: day.clone(),
                    // Random colors
                    style: [myStyle.myshadow, { backgroundColor: 'yellow',borderRadius:8}],
                    textStyle: [myStyle.textBold, { color: "black" }], // sets the font color
                    containerStyle: [], // extra styling for day container
                    allowDisabled: true, // allow custom style to apply to disabled dates

                });
                console.log(day)
                break;
            }
        }
    }


    //     let today = moment();
    // let day = today.clone().startOf('month');
    // let customDatesStyles = [];
    // while(day.add(1, 'day').isSame(today, 'month')) {
    //   customDatesStyles.push({
    //     date: day.clone(),
    //     // Random colors
    //     style: {backgroundColor: '#'+('#00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6)},
    //     textBold: {color: 'black'}, // sets the font color
    //     containerStyle: [], // extra styling for day container
    //     allowDisabled: true, // allow custom style to apply to disabled dates
    //   });
    // }

    let today = moment();
    let day = today.clone().startOf('month');
    let customDatesStyles = [];
    // while (day.add(1, 'day').isSame(today, 'month')) {
    //     customDatesStyles.push({
    //         date: day.clone(),
    //         // Random colors
    //         style: [myStyle.myshadow, { backgroundColor: Colors.blueColor, }],
    //         textStyle: [myStyle.textBold, { color: "white" }], // sets the font color
    //         containerStyle: [], // extra styling for day container
    //         allowDisabled: true, // allow custom style to apply to disabled dates


    //     });
    // }

    return (
        <View>

            <CalendarPicker
                onDateChange={onDateChange}
                todayBackgroundColor={Colors.blue}
                selectedDayColor={Colors.blueColor}
                customDatesStyles={customDatesStyles}
                // containerStyle={{backgroundColor:"red"}}
                // selectedRangeStartStyle={{ elevation: 10, backgroundColor: 'red' }}
                // selectedRangeEndStyle={{ elevation: 10, backgroundColor: 'red' }}
                // allowRangeSelection={true}
                selectedDayStyle={[myStyle.myshadow, { backgroundColor: Colors.blue, borderRadius: 8 }]}
                selectedDayTextStyle={[myStyle.textBold, { color: Colors.whiteText, }]}
                previousTitle="<"
                nextTitle=">"
                nextTitleStyle={[myStyle.textBold, { fontSize: 28, color: 'gray' }]}
                previousTitleStyle={[myStyle.textBold, { fontSize: 28, color: 'gray' }]}
                headerWrapperStyle={{ justifyContent: 'flex-end', }}
                dayLabelsWrapper={{ borderColor: 'white', }}
                textStyle={{ fontWeight: 'bold', fontSize: 14, color: '#909090' }}
                dayShape={'square'}

            />
            <Text style={[myStyle.textBold, { fontSize: 20 }]} >{"Selected Date"}</Text>
            <Text style={[myStyle.textBold,]}>{selectedStartDate} </Text>
            <Text style={[myStyle.textBold,]}>{selectedEndDate} </Text>


            <Text style={[myStyle.textBold,]}>{day.toString()} </Text>

        </View>
    )
}

export const CalendarComponentMemo = React.memo(CalendarComponent);