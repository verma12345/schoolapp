import React, { useState } from 'react'
import { ActivityIndicator, Image, Text, TouchableOpacity, View } from 'react-native'
import Colors from '../common/Colors'
import { myStyle } from '../common/myStyle'

const StudentDetailsOption = (props) => {
    const [layout, setLayout] = useState({})
    return (

        <TouchableOpacity
            onLayout={(event) => { setLayout(event.nativeEvent.layout) }}
            onPress={props.onPress}
            style={[myStyle.myshadow, {
                paddingHorizontal: 8,
                paddingVertical: 6,
                marginBottom: 15,
                marginHorizontal: 8,
                borderRadius: 20,
                backgroundColor: '#FFFFFF'
            }, props.style]}
        >

            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10 }} >


                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', flex: 1 }}>
                    <View style={[{ padding: 7, borderRadius: 10 }, props.iconStyle]} >
                        <Image
                            style={[{ height: 22, width: 22, resizeMode: 'contain' },]}
                            source={props.leftIcon}
                        />
                    </View>
                    <View style={{ paddingHorizontal: 10 }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>{props.title}  {props.headerCount ? '(' : null} <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>{props.headerCount} </Text>{props.headerCount ? ')' : null}  </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor, marginTop: 5 }]}>{props.message} </Text>
                    </View>
                </View>
                <Image
                    style={{ height: 16, resizeMode: 'contain', width: 16, resizeMode: 'contain', marginLeft: 10 }}
                    source={props.rightIcon}
                />
            </View>

            {props.loader ?
                <View style={{
                    position: 'absolute', alignItems: "center", justifyContent: "center",
                    backgroundColor: '#0002', width: layout.width, height: layout.height,
                    borderRadius: 15,
                    // elevation: 1
                }} >
                    <ActivityIndicator color={Colors.blue} size='large' />
                </View>
                : null}


        </TouchableOpacity>
    )
}

export const StudentDetailsOptionMemo = React.memo(StudentDetailsOption)