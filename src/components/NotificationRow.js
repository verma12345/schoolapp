import React from "react";
import { Image, PixelRatio, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import css from "../styles/css";

const NotificationRow = (props) => {

    return (

        <Pressable
            android_ripple={{ color: Colors.lightblue, borderless: false, borderRadius: 100 }}
            style={[myStyle.myshadow, styles.main, props.container]}
            onPress={props.onPressNotification}


        >
            <View style={{ flexDirection: "row", justifyContent: 'center', alignItems: 'center' }} >
                <TouchableOpacity
                    style={{ backgroundColor: props.backgroundColor, padding: 7, borderRadius: 10 }}
                    onPress={props.onPressNotification}>
                    <Image
                        source={props.image}
                        style={styles.iconStyle}
                    />
                </TouchableOpacity>

                <Text style={[myStyle.textNormal, { marginLeft: 17, fontSize: 15 }]}>
                    {props.title}
                </Text>
            </View>

            <View style={{ flexDirection: "row", alignItems: 'center' }} >
                {
                    props.isToggle ?
                        <TouchableOpacity
                            style={styles.onOff}
                            onPress={props.onPressToggle}>
                            <View style={styles.dot} />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity
                            style={[styles.onOff, { backgroundColor: Colors.blue, justifyContent: 'flex-end' }]}
                            onPress={props.onPressToggle}>
                            <View style={styles.dot} />
                        </TouchableOpacity>
                }
            </View>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    onOff: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'flex-start',
        borderRadius: 20,
        backgroundColor: Colors.grayColor,
        padding: 2,
        width: 32
    },
    dot: {
        height: 11,
        width: 11,
        borderRadius: 30,
        backgroundColor: "white"
    },
    iconStyle: {
        height: 23,
        width: 23,
        resizeMode: 'contain'
    },
    main: {
        justifyContent: 'space-between',
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginTop: 12,
        marginHorizontal: 25,
        paddingVertical: 10,
        elevation: 1,
        borderRadius: 15,
    },
})
export default NotificationRow;