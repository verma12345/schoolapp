import React from "react";
import { ActivityIndicator, StyleSheet, Text, View } from "react-native";
import Colors from "../common/Colors";
import colors from "../styles/colors";
import Font from "../styles/Font";

export const Loader = (props) => {


    return (
        <View
            style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#0005',
                elevation: 10,
                position: 'absolute',
            }}>
            <View style={{ borderRadius: 10, backgroundColor: "white", padding: 10, borderRadius: 30, justifyContent: "center", alignItems: "center", }}>
                <ActivityIndicator
                    size={"large"}
                    color={Colors.blue}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    textInputContainer: {
        backgroundColor: colors.themeColor,
        borderRadius: 10,
        elevation: 10
    },
    textInput: {
        color: colors.grayColor,
        height: 50,
        fontSize: 20,
        fontFamily: Font.Roboto
    }
})
