import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';


const CheckBox = (props) => {
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 35 }}>
            <TouchableOpacity
                style={{ borderWidth: 1, borderColor: Colors.grayColor, borderRadius: 5, padding: 2 }}
                onPress={props.onPress}>
                <Image
                    style={{
                        height: 15,
                        width: 15,
                        resizeMode: 'contain',
                        tintColor: props.is_check ? Colors.greenColor : 'white'
                    }}
                    source={props.is_check ? require('../../assets/check3.png') : require('../../assets/checkbox.png')}
                />
            </TouchableOpacity>

            <TouchableOpacity onPress={props.onPressUrl}>
                <Text style={[myStyle.textNormal, { fontSize: 12, marginLeft: 10, color: Colors.grayColor }]}>
                    {`I accept terms and conditions.`}
                </Text>
            </TouchableOpacity>
        </View>
    )
}


export default CheckBox;
