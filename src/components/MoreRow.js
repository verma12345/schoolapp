import React from "react";
import { Image, PixelRatio, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";

const MoreRow = (props) => {

    return (
        <Pressable
        android_ripple={{ color: Colors.lightblue, borderless: false }}
            style={[myStyle.myshadow, styles.main, props.mainStyle]}
            onPress={props.onPress}>
            <View style={{ backgroundColor: props.backgroundColor, padding: 7, borderRadius: 10 }} >
                <Image
                    source={props.image}
                    style={[styles.iconStyle,props.iconStyle]}
                />
            </View>
            <Text style={[myStyle.textNormal, { marginLeft: 17,fontWeight:'400', fontSize: 15 }]}>
                {props.title}
            </Text>
        </Pressable >
    )
}

const styles = StyleSheet.create({
    main: {
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginTop: 12,
        marginHorizontal: 25,
        paddingVertical: 10,
        elevation: 1,
        borderRadius: 15,
    },
   
    iconStyle: {
        height: 23,
        width: 23,
        resizeMode: 'contain'
    },
})
export default MoreRow;