import React, { Component } from 'react'
import { Dimensions, Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';


const PopUp = (props) => {
    return (
        <View style={{
            height,
            width,
            position: 'absolute',
            justifyContent: 'center',
            backgroundColor: '#0009',
            elevation: 4
        }}>
            <View style={{
                flex: 1,
                backgroundColor: 'white',
                marginVertical: 260,
                borderRadius: 10,
                marginHorizontal: 20,
                justifyContent: 'center',
            }}>

                <TextInput
                    placeholder="Amount"
                    backgroundColor="white"
                    onChangeText={props.onChangeText}
                    value={props.value}
                    keyboardType="number-pad"
                    maxLength={7}
                    style={{ paddingHorizontal: 20, elevation: 6, width: '90%', borderRadius: 7, alignSelf: 'center' }}
                />

                <TouchableOpacity
                    onPress={props.onPress}
                    style={{ borderRadius: 6, alignItems: 'center', marginTop: 50, backgroundColor: Colors.blue, padding: 15, elevation: 6, width: '90%', alignSelf: 'center' }}>
                    <Text style={{ color: 'white' }}>
                        {'SAVE'}
                    </Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

const { width, height } = Dimensions.get('window')
export default PopUp;
