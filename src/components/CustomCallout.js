import React, { Component } from "react";
import { Image, Text, View } from "react-native";

class CustomCallout extends Component {

    render() {
        return (
            <View style={{
                flexDirection: 'row',
                justifyContent: 'center', 
                margin:10,
                width:200

            }} >
                <Text style={{marginLeft:10}} >
                    <Image
                        style={{ height: 20, width: 20 }}
                        source={{ uri: this.props.data.icon }}
                    />
                </Text>
                <Text numberOfLines={2} style={{width:"100%",margin:5}}>
                    {this.props.data.name==undefined?"Please select any location": this.props.data.name}
                </Text>
            </View>



        )
    }
}
export default CustomCallout;