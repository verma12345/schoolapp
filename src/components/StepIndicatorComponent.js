import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import { myStyle } from '../common/myStyle';

const indicatorStyles = {
    stepIndicatorSize: 20,
    currentStepIndicatorSize: 25,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 2,
    stepStrokeCurrentColor: '#3BB54A',
    stepStrokeWidth: 2,
    stepStrokeFinishedColor: '#3BB54A',
    stepStrokeUnFinishedColor: 'red',
    separatorFinishedColor: '#3BB54A',
    separatorUnFinishedColor: '#dedede',
    stepIndicatorFinishedColor: '#3BB54A',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 0,
    currentStepIndicatorLabelFontSize: 0,
    stepIndicatorLabelCurrentColor: 'transparent',
    stepIndicatorLabelFinishedColor: 'transparent',
    stepIndicatorLabelUnFinishedColor: 'transparent',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#999999',
    separatorStrokeHeight: 500,
};

const StepIndicatorComponent = (props) => {

    const renderStepIndicator = () => {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image
                    style={{ height: 10, width: 10 }}
                    source={require('../../assets/check3.png')}
                />

            </View>
        )
    }

    return (
        <View style={[{ flex: 1 }, props.style]}>

            <StepIndicator
                customStyles={indicatorStyles}
                currentPosition={props.currentPosition}
                labels={props.labels}
                stepCount={props.stepCount}
                direction={props.direction}
                renderLabel={props.renderLabel}
                renderStepIndicator={renderStepIndicator}
            />

        </View>
    )
}


export default StepIndicatorComponent;
