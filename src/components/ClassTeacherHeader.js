import React, { Component } from 'react';
import { Image, PixelRatio, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';



class ClassTeacherHeader extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ padding: 10 }}>

                    <View style={[myStyle.myshadow, { padding: 10, borderTopRightRadius: 20, borderTopLeftRadius: 20, backgroundColor: Colors.whiteText }]} >
                        <View style={{ flexDirection: 'row', padding: 10 }} >
                            <Image
                                style={{
                                    height: PixelRatio.getPixelSizeForLayoutSize(35),
                                    width: PixelRatio.getPixelSizeForLayoutSize(35)
                                }}
                                source={require('../../assets/profile.png')}
                            />

                                <View style={{ flexDirection: 'row', alignItems: 'center' }} >

                                    <View style={{ padding: 5 }} >
                                        <Text style={[myStyle.textBold, { fontSize: 16 }]} >
                                            {"Akshita"}
                                        </Text>
                                        <Text style={[myStyle.textBold, { color: Colors.dark, fontSize: 14 }]} >
                                            {"CMS Rajajipuram"}
                                        </Text>


                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>



                    <View style={{ height: 1, width: '100%', backgroundColor: "lightgray" }} />


                    <View style={[myStyle.myshadow, { flexDirection: 'row', justifyContent: 'space-around', backgroundColor: 'white', borderBottomLeftRadius: 20, borderBottomRightRadius: 20, }]} >
                        <TouchableOpacity
                            onPress={() => this.onPressTimeTable()}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }} >
                            <Text style={myStyle.textNormal}>
                                {'Time Table'}
                            </Text>
                        </TouchableOpacity>

                        <View style={{ height: 50, width: 1, backgroundColor: "gray" }} />

                        <TouchableOpacity
                            onPress={() => this.onPressReport()}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={myStyle.textNormal}>
                                {'Report'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>



        )
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20
    },
    btn: {
        height: 35,
        width: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})

export default ClassTeacherHeader;