import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';



export const LoginHeader = (props) => {
    return (
        <View style={styles.container}>
            <View>
                <TouchableOpacity
                    style={styles.btn}
                    onPress={() => props.navigation.goBack()}
                >
                    <Image
                        source={props.backpic}
                        style={{ height: 16, width: 16, resizeMode: 'center' }}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.view1}>
                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 28 }]}>{props.txtheading}</Text>

                <Text style={[myStyle.textNormal, { color: Colors.grayColor, paddingTop: 20, fontSize: 12 }]}>{props.txt}</Text>

            </View>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 35
    },
    btn: {
        padding:10,
        alignSelf:'flex-start',
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})