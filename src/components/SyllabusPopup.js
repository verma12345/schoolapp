import React, { Component } from 'react'
import { Dimensions, Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';


const SyllabusPopup = (props) => {
    return (
        <TouchableOpacity 
        onPress={props.onPressCancel}
        style={{
            height,
            width,
            position: 'absolute',
            justifyContent: 'center',
            backgroundColor: '#0003',
            elevation: 4
        }}>
            <View style={{
                backgroundColor: 'white',
                paddingVertical:20,
                borderRadius: 20,
                marginHorizontal: 20,
                justifyContent: 'center',
            }}>


                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: Colors.lightGray,
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        paddingHorizontal:15,
                        paddingVertical:9,
                        marginHorizontal:15,
                        marginVertical:7 
                    }}
                    onPress={props.onPressWatchvideo}
                    >
                    <View style={{ padding: 7, borderRadius: 10, backgroundColor: 'rgba(242, 153, 74, 0.15)' }} >
                        <Image
                            style={[{ height: 22, width: 22, resizeMode: 'contain' },]}
                            source={require('../../assets/video.png')}
                        />
                    </View>
                    <Text style={[myStyle.textBold, { fontSize: 14, marginLeft: 14 }]}>{"Watch Video"}  <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>{props.headerCount} </Text>{props.headerCount ? ')' : null}  </Text>


                </TouchableOpacity>

                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: Colors.lightGray,
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        paddingHorizontal:15,
                        paddingVertical:9,
                        marginTop:8,
                        marginHorizontal:15,
                        marginVertical:7  
                    }}
                    onPress={props.onPressQuiz}
                    >
                    <View style={{ padding: 7, borderRadius: 10, backgroundColor: 'rgba(47, 128, 237, 0.2)' }} >
                        <Image
                            style={[{ height: 22, width: 22, resizeMode: 'contain' },]}
                            source={require('../../assets/password.png')}
                        />
                    </View>
                    <Text style={[myStyle.textBold, { fontSize: 14, marginLeft: 14 }]}>{"Quiz"}  <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>{props.headerCount} </Text>{props.headerCount ? ')' : null}  </Text>


                </TouchableOpacity>


                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        borderRadius: 20,
                        borderWidth: 1,
                        borderColor: Colors.lightGray,
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        paddingHorizontal:15,
                        paddingVertical:9,
                        marginTop:8,
                        marginHorizontal:15,
                        marginVertical:7  
                    }}
                    onPress={props.onPressHelp}
                    >
                    <View style={{ padding: 7, borderRadius: 10, backgroundColor:  '#FCE6E6'  }} >
                        <Image
                            style={[{ height: 22, width: 22, resizeMode: 'contain' },]}
                            source={require('../../assets/help.png')}
                        />
                    </View>
                    <Text style={[myStyle.textBold, { fontSize: 14, marginLeft: 14 }]}>{"Expert Help"}  <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>{props.headerCount} </Text>{props.headerCount ? ')' : null}  </Text>


                </TouchableOpacity>


            </View>

        </TouchableOpacity >
    )
}

const { width, height } = Dimensions.get('window')
export default SyllabusPopup;
