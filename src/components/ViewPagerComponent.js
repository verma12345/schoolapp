import React, { useState, useRef, Component } from 'react';
import { StyleSheet, View, Platform, Text, TouchableOpacity, Image, ImageBackground, Dimensions } from 'react-native';
import Colors from '../common/Colors';
import Font from '../common/Font';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PagerView from 'react-native-pager-view';
import { myStyle } from '../common/myStyle';

class ViewPagerComponent extends Component {


    render() {
        let row = this.props.pager_list.map((item, index) => {
            return (
                <View
                    key={index}
                    style={[{
                        flex: 1,
                        backgroundColor: "#5FA6E9"
                    }]}
                >
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-end', backgroundColor: "#5FA6E9" }} >
                        <Image
                            source={item.icon}
                            style={{ height: 200, width: 200, marginBottom: index == 0 ? 5 : -30 }}
                        />
                    </View>

                    <ImageBackground
                        imageStyle={{ resizeMode: 'stretch', }}
                        style={{ flex: 1,justifyContent:'center' }}
                        source={require('../../assets/ellips.png')}
                    >



                        <View style={{ padding: 20, marginTop: 50 }} >
                            <Text style={[myStyle.textBold, { fontSize: 30, color: '#000000',marginBottom:20 }]} >
                                {item.heading}
                            </Text>

                            <Text style={[myStyle.textNormal, { fontSize: 16, color: '#909090', marginBottom:40}]} >
                                {item.message}
                            </Text>
                        </View>

                    </ImageBackground>

                </View>
            )
        })


        return (

            < View style={{ flex: 1 }} >

                <PagerView
                    onPageSelected={this.props.onPageSelected}
                    style={styles.viewPager}
                    initialPage={0}>

                    {row}

                </PagerView>

            </View >
        );
    }


};

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    viewPager: {
        flex: 1
    },

    txtStyle: {
        fontSize: 16,
        fontFamily: Font.robotoSourceSansPro,
        fontWeight: '300',
        margin: 5,
        color: Colors.textDullColor,
        width: "100%",
    },

});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jaswant = state.indexReducerJaswant;

    return {
        // pager_list: jaswant.pager_list,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setUserId: (user_id) => setUserId(user_id),

            // setWorkoutOfTheWeekData: (res) => setWorkoutOfTheWeekData(res),
            // hitWorkoutOfTheWeek: (param) => hitWorkoutOfTheWeek(param),



        },
        dispatch,
    );
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewPagerComponent);
