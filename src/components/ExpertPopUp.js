import React, { Component } from 'react'
import { Dimensions, Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';


const ExpertPopUp = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={{
                height,
                width,
                position: 'absolute',
                justifyContent: 'center',
                backgroundColor: '#0003',
                elevation: 4
            }}>
            <View style={{
                backgroundColor: 'white',
                paddingVertical: 20,
                borderRadius: 20,
                marginHorizontal: 20,
                justifyContent: 'center',
            }}>

                <TouchableOpacity
                    style={{
                        borderRadius: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingHorizontal: 15,
                        paddingVertical: 9,
                        marginTop: 8,
                        marginHorizontal: 15,
                        marginVertical: 7
                    }}
                    onPress={props.onPressExpertHelp}
                >
                    <View style={{ padding: 7, borderRadius: 10, backgroundColor: '#FCE6E6' }} >
                        <Image
                            style={[{ height: 28, width: 28, resizeMode: 'contain' },]}
                            source={require('../../assets/help.png')}
                        />
                    </View>
                    <Text style={[myStyle.textNormal, { fontSize: 14, marginLeft: 14, color: Colors.grayColor, marginTop: 14 }]}>{"Expert Help"}  <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{props.headerCount} </Text>{props.headerCount ? ')' : null}  </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 14, marginLeft: 14 }]}>{"Coming Soon..."}  <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>{props.headerCount} </Text>{props.headerCount ? ')' : null}  </Text>

                </TouchableOpacity>



            </View>

        </TouchableOpacity >
    )
}

const { width, height } = Dimensions.get('window')
export default ExpertPopUp;
