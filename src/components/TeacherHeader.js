import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';



export const TeacherHeader = (props) => {
    return (



        <View style={{
            height: 65,
            width: '100%',
            backgroundColor: Colors.whiteText,
            justifyContent: "center",
            alignItems: "center",
            flexDirection: 'row',
            paddingHorizontal: 15,
            borderBottomLeftRadius:20,
            borderBottomRightRadius:20,
            elevation:2,
           
        }}>
            <TouchableOpacity
                onPress={props.onPressBack}
                style={{
                    position: 'absolute',
                    left: 20,
                    padding:8,
                }}
            >
                <Image
                    source={require('../../assets/back.png')}
                    style={{ height: 16, width: 16, resizeMode: 'contain' }}
                />
            </TouchableOpacity>

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                    source={props.teacher_pic}
                    style={{ height: 28, width: 28,borderRadius:12 }}
                />
                <Text style={[myStyle.textBold, { color: Colors.textDarkColor, fontSize: 14, marginLeft: 10 }]}>{props.txt}</Text>


            </View>


        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20
    },
    btn: {
        height: 35,
        width: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})