

import React, { useCallback, useRef, useMemo } from 'react';
import { StyleSheet, View, Text, Button, TouchableOpacity, Image } from 'react-native';
import BottomSheet, { BottomSheetFlatList } from '@gorhom/bottom-sheet';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';
import { connect } from 'react-redux';
import { setFeed, setFeedIsActive, setNotification, setNotificationIsActive } from '../redux_store/actions/indexActionsRajpoot';
import { bindActionCreators } from 'redux';

const MapBottomFeed = (props) => {
    // hooks


    const snapPoints = useMemo(() => ['20%', '50%', '90%'], []);

    // callbacks
    const handleSheetChange = useCallback(index => {
        console.log('handleSheetChange', index);
    }, []);




    const _onpressNotification = () => {
        props.setNotificationIsActive(!props.notificationIsActive)
    }

    const _renderNotification = (dataItem) => {
        return (
            <View>
                {dataItem.index == 0 ?
                    <View style={{ flexDirection: 'row', paddingLeft: 15, backgroundColor: Colors.backgroundColor, borderTopLeftRadius: 50, borderTopRightRadius: 50 }}>
                        <TouchableOpacity onPress={() => _onpressNotification()}>
                            <Text style={{ color: props.notificationIsActive ? 'gray' : 'black' }}>{"Notification"}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ paddingLeft: 20 }}
                            onPress={() => _onpressFeed()}
                        >

                        </TouchableOpacity>
                    </View>
                    : null
                }
                <View style={{ flexDirection: 'row', marginTop: 12 }}>

                    <View>
                        <Image
                            style={{ height: 50, width: 50, borderRadius: 50 }}
                            source={dataItem.item.profile}
                        />
                    </View>
                    <View style={{ paddingLeft: 20 }}>
                        <Text style={[myStyle.textBold, { fontSize: 16 }]}>{'Choose your favourite location'}</Text>
                        <Text style={[myStyle.textBold, { marginTop: 10, width: '60%', fontSize: 14 }]}>{dataItem.item.descritopn}</Text>

                    </View>
                </View>
            </View>
        )
    }



    return (

        <BottomSheet
            snapPoints={snapPoints}
            onChange={handleSheetChange}
        >
            <BottomSheetFlatList
                data={props.notification}
                keyExtractor={(item, index) => 'key' + index}
                renderItem={_renderNotification}
                contentContainerStyle={styles.contentContainer}
            // backdropComponent={styles.contentContainer}
            />

        </BottomSheet>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingTop: 200,
    },
    contentContainer: {
        backgroundColor: 'white',
    },
    itemContainer: {
        padding: 6,
        margin: 6,
        backgroundColor: '#eee',
    },
});



const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let rajpoot = state.indexReducerRajpoot

    return {
        notification: rajpoot.notification,
        feed: rajpoot.feed,
        feedIsActive: rajpoot.feedIsActive,
        notificationIsActive: rajpoot.notificationIsActive
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setNotification: (notification) => setNotification(notification),
        setFeed: (feed) => setFeed(feed),
        setNotificationIsActive: (notificationIsActive) => setNotificationIsActive(notificationIsActive),
        setFeedIsActive: (feedIsActive) => setFeedIsActive(feedIsActive)
    }, dispatch)
}



export default connect(mapStateToProps, mapDispatchToProps)(MapBottomFeed)
