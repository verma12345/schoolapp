import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';



export const AttendancePercetage = (props) => {
    return (

        <View style={{
            flexDirection: 'row',
            borderBottomRightRadius: 20,
            borderBottomLeftRadius: 20,
            justifyContent: 'space-around',
            alignItems: 'center',
            backgroundColor: Colors.whiteText,
            borderRadius:15,
            elevation:4,
            marginHorizontal:20,
            paddingVertical:20,
            marginTop:10
        }}>
            <View style={{alignItems:'center'}} >
                <Text style={[myStyle.textNormal, { fontSize: 14,color:Colors.grayColor }]}>{"Monthly present"}</Text>
                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 16 }]}>{'A'}</Text>
            </View>

            <View style={{alignItems:'center'}} >
                <Text style={[myStyle.textNormal, { fontSize: 14,color:Colors.grayColor }]}>{"Percent"}</Text>
                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 16 }]}>{'92%'}</Text>
            </View>


        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20
    },
    btn: {
        height: 35,
        width: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})