import React, { Component } from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';


const DashboardHeader = (props) => {
    return (
        <View style={{
            height: 50,
            backgroundColor: Colors.backgroundColor,
            justifyContent: "space-between",
            alignItems: "center",
            flexDirection: 'row',
            paddingHorizontal: 15
        }}>
            <Text style={[myStyle.textBold, { fontSize: 18 }]}>
                {'School'}<Text style={[myStyle.textNormal,{fontSize: 16}]} >{' App'}</Text>
            </Text>

            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >
                <TouchableOpacity
                    style={{ marginRight: 20 }}
                >
                    <Image
                        source={require('../../assets/notification.png')}
                        style={{ height: 24, width: 24, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>

                <TouchableOpacity>
                    <Image
                        source={require('../../assets/plus.png')}
                        style={{ height: 24, width: 24, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>
            </View>


        </View>
    )
}


export default DashboardHeader;
