import React, { Component } from 'react'
import { View } from 'react-native';
import Colors from '../common/Colors';


class GalleryPagerDot extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let active_dot = this.props.list.map((item, index) => {
            return (
                <View
                    key={index}
                    style={{
                        elevation: 3,
                        zIndex: 1,
                        height: 10,
                        marginHorizontal: 3,
                        width:10,
                        backgroundColor: this.props.position == index ? Colors.blue : Colors.grayColor,
                        borderRadius: 10,
                    }} />
                // </View>
            );
        });

        return (
            <View style={{ height: 10, margin: 10, }}>
                <View style={{
                    position: 'absolute',
                    flexDirection: 'row',
                    // bottom: 0,
                    alignSelf: "center",
                }}>
                    {active_dot}
                </View>

            </View>
        )
    }
}


export default GalleryPagerDot;
