import React from 'react'
import { Image, View } from "react-native"
import Colors from '../common/Colors'

export const CustomeMarker = (props) => {
    return (

        props.index == 0 ?
            <View style={{
               padding:5,
                borderRadius: 50,
                backgroundColor: '#0004',
                elevation: 15,
                justifyContent: 'center',
                alignItems: 'center'
            }} >
                <View style={{
                    height: 15, width: 15, borderRadius: 100,
                    elevation: 10,
                    backgroundColor: Colors.blue,
                }} >

                </View>
            </View>
            :
            <Image
                source={require('../../assets/bus.png')}
                style={{ height: 40, width: 40, resizeMode: 'contain' }}
            />
            // <View>
            //     <View style={{ width: 10, height: 20, borderWidth: 1, backgroundColor: '#E78C34' }} />
            // </View>
    )
}