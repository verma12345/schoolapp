import React from 'react'
import { ColorPropType, Image, Text, TouchableOpacity, View } from "react-native"
import Font from '../common/Font'

export const PopupLocationInfo = (props) => {
    return (
        <View style={{
            position: 'absolute',
            padding: 15,
            backgroundColor: 'white',
            elevation: 10,
            bottom: 0,
            width:'100%',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20
        }} >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                    style={{ height: 35, width: 35 }}
                    source={{ uri: props.data.icon }}
                />
                <Text style={{ fontSize: 20, fontWeight: 'bold', marginLeft: 8, fontFamily: Font.Roboto }}>
                    {props.data.name}
                </Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 8, alignItems: 'center' }}>
                <Text style={{ fontSize: 14, fontWeight: '500' }}>
                    {'Rating:' + props.data.rating}
                </Text>
                <Text style={{ fontSize: 14, marginLeft: 10, fontWeight: '500' }}>
                    {'Reviews:' + props.data.user_ratings_total}
                </Text>
            </View>
            <Text style={{ fontSize: 14, fontWeight: '500' }}>
                {props.data.formatted_address}
            </Text>

            {/* <Text style={{fontSize:14,fontWeight:'500'}}>
                {props.data.opening_hours.periods[0].open.day}
            </Text>  */}
            <Text style={{ fontSize: 14, fontWeight: '500' }}>
                {'lat:' + props.data.geometry.location.lat}
            </Text>
            
            <Text style={{ fontSize: 14, fontWeight: '500' }}>
                {'long:' + props.data.geometry.location.lng}
            </Text>
        </View>
    )
}

