import React, { Component } from 'react';
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';



class WeaklyCalenderComponent extends Component {
    constructor(props) {
        super(props)
      

    }

    
    onPressBack = () => {
        this.props.navigation.goBack()
    }
    _renderItem = (dataItem) => {
        return (
            <View style={{}}>
                <TouchableOpacity
                    onPress={() => this.props.setIndex(dataItem.index)}
                    style={{
                        backgroundColor: dataItem.index == this.props.index ? Colors.blue : Colors.whiteText,
                        paddingHorizontal: 12,
                        height: 25,
                        justifyContent: 'center',
                        borderRadius: 10,
                        // paddingVertical: 10,
                        // borderTopLeftRadius: dataItem.index == 0 ? 12 : 0,
                        // borderTopRightRadius: dataItem.index == this.props.days_list.length - 1 ? 12 : 0,
                        // borderBottomLeftRadius: dataItem.index == 0 ? 12 : 0,
                        // borderBottomRightRadius: dataItem.index == this.props.days_list.length - 1 ? 12 : 0,
                    }}>

                    <Text style={[myStyle.textBold, { fontSize: 12, color: dataItem.index == this.props.index ? Colors.whiteText : Colors.textColor, }]}>{dataItem.item.day}</Text>

                </TouchableOpacity>
            </View>

        )
    }


    render() {
        return (
            <View style={styles.container} >


                {/********************************* timetable header *********************************/}
                <View style={[myStyle.myshadow, {
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                    marginBottom: 1,
                    borderBottomRightRadius: 30,
                    borderBottomLeftRadius: 30
                }]}>

                    <View style={{
                        height: 45,
                        width: '100%',
                        backgroundColor: Colors.whiteText,
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: 'row',
                        paddingHorizontal: 15,
                        marginTop: 7

                    }}>
                        <TouchableOpacity
                            onPress={this.onPressBack}
                            style={{ position: 'absolute', left: 20 }}
                        >
                            <Image
                                source={require('../../assets/back.png')}
                                style={{ height: 14, width: 14, tintColor: Colors.textColor, resizeMode: 'contain' }}
                            />
                        </TouchableOpacity>

                        <View style={{
                            justifyContent: "space-between",
                            alignItems: "center",
                            flexDirection: 'row',
                        }} >
                            <Image
                                source={require('../../assets/teacher4.png')}
                                style={{ height: 27, width: 27, marginRight: 10, borderRadius: 12 }}
                            />
                            <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.textDarkColor }]}>{"Timetable"}</Text>
                        </View>
                    </View>


                    <FlatList
                        style={{
                            borderWidth: 1,
                            borderRadius: 12,
                            borderColor: 'lightgray',
                            marginTop: 15,
                            marginBottom: 20
                        }}

                        data={this.props.days_list}
                        renderItem={this._renderItem}
                        keyExtractor={(item, index) => index + 'key'}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                    />
                </View>

            </View>
        )
    }



}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.backgroundColor,
    },

})

const mapStateToProps = (state) => {
    let akshita = state.indexReducerAkshita;

    return {
        days_list: akshita.days_list


    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setDaysList: (days_list) => setDaysList(days_list)



    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(WeaklyCalenderComponent)