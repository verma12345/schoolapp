import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import Font from '../common/Font'
import { myStyle } from '../common/myStyle'
export const CustomeButtton = (props) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.btnStyle, props.style]} >

            <Text style={[myStyle.textBold, props.titleStyle]} >{props.title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    btnStyle: {
        borderRadius: 10,
        width: '100%',
        height: 55,
        justifyContent: "center",
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.lightblue,
        elevation: 10,
        // marginVertical: 40
    },
  
})