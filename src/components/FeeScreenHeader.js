import React, { Component } from 'react'
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { myStyle } from '../common/myStyle';


const FeeScreenHeader = (props) => {
    return (
        <View style={{
            borderBottomRightRadius: 20,
            borderBottomLeftRadius: 20,
            backgroundColor: 'white',
            elevation: 3,
            paddingVertical: 10,
            paddingBottom: 20
        }} >
            <View style={styles.header}>

                <TouchableOpacity onPress={props.onBack}
                    style={{}} >
                    <Image
                        source={require('../../assets/back.png')}
                        style={{ height: 18, width: 18, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                    <Image
                        source={require('../../assets/profile.png')}
                        style={{ height: 28, width: 28, resizeMode: 'contain',borderRadius:50, paddingRight: 10, marginRight: 10 }}
                    />
                    <Text style={[myStyle.textBold,{fontSize:14}]}>{'School Fee'} </Text>
                </View>

                <TouchableOpacity onPress={props.onFeeStructure}
                    style={{}} >
                    <Image
                        source={require('../../assets/invoice.png')}
                        style={{ height: 20, width: 20, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20, marginTop: 30}} >
                <Image
                    source={props.student_profile}
                    style={{ height: 65, width: 65, resizeMode: 'contain', marginHorizontal: 10, borderRadius: 10 }}
                />
                <View style={{marginLeft:7}}>
                    <Text style={[myStyle.textBold, { fontSize: 15, color: '#28293D' }]}>{props.student_name} </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12, marginTop: 3, color: '#909090' }]}>{props.school_name} </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12, marginTop: 3, color: '#28293D' }]}>{props.school_address} </Text>
                </View>

            </View>
        </View>
    )
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },
    header: {
        height: 40,
        paddingHorizontal: 20,
        backgroundColor: Colors.backgroundColor,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: "center",

    },

})


export default FeeScreenHeader;
