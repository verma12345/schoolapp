import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import Colors from '../common/Colors'
import { myStyle } from '../common/myStyle'
import colors from "../styles/colors"
import Font from '../styles/Font'
export const CustomeButtton = (props) => {
    return (
        props.icon_left ?
            <TouchableOpacity style={[styles.view3, props.style]}
                onPress={props.onPress}
                disabled={props.disabled}
                >
                <Image style={{ height: 23, width: 23 }}
                    source={props.icon_left}
                />
                <Text style={[myStyle.textNormal, { paddingLeft: 20, fontSize: 12 }, props.txtStyle]}>{props.title}</Text>
                {props.children}
            </TouchableOpacity>

            :

            <TouchableOpacity
                onPress={props.onPress}
                disabled={props.disabled}
                style={[styles.btnStyle, props.style]} >
                <Text style={[styles.txtStyle, props.titleStyle]} >{props.title}</Text>
                {props.children}
            </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    btnStyle: {
        borderRadius: 18,
        height: 48,
        justifyContent: "center",
        flexDirection:'row',
        alignItems: 'center',
        backgroundColor: Colors.blue,
        elevation: 3,
    },
    txtStyle: {
        fontSize: 14,
        color: 'white',
        fontFamily: Font.Roboto
    },
    view3: {
        height: 48,
        backgroundColor: Colors.backgroundColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.1,
        borderRadius: 20,
    },

})