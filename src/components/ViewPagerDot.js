import React, { Component } from 'react'
import { View } from 'react-native';
import Colors from '../common/Colors';


class ViewPagerDot extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let active_dot = this.props.list.map((item, index) => {
            return (
              
                <View
                    key={index}
                    style={{
                        elevation: 3,
                        zIndex: 1,
                        height: 10,
                        marginHorizontal: 3,
                        width: this.props.position == index ? 30 : 10,
                        backgroundColor: this.props.position == index ? Colors.skyBlue : Colors.blue,
                        borderRadius: 10,
                    }} />
                // </View>
            );
        });

        return (
            <View style={{ height: 10, margin: 20, }}>
                <View style={{
                    position: 'absolute',
                    flexDirection: 'row',
                    // bottom: 0,
                    alignSelf: "center",
                }}>
                    {active_dot}
                </View>

            </View>
        )
    }
}


export default ViewPagerDot;
