import React from 'react'
import { Image, Text, Pressable, View, StyleSheet } from 'react-native'
import colors from '../styles/colors';
import css from '../styles/css';

const Header1 = (props) => {
    return (

        <View style={[styles.headerStyle, css.myshadow, props.style]} >

            <View style={[styles.headerStyle, props.style]} >
                {props.isBack != false ?
                    <Pressable
                        android_ripple={{ color: colors.blueColor, borderless: false, }}
                        onPress={props.onBack}
                        style={{ paddingLeft: 20, height: 40, justifyContent: 'center' }}
                    >
                        <Image
                            source={require('../../assets/back.png')}
                            style={[
                                {
                                    tintColor: colors.whiteColor,
                                    height: 15,
                                    width: 30,
                                }]}
                        />

                    </Pressable>
                    : null
                }
                <Text style={[css.textStyle, { paddingLeft: 20, alignSelf: 'center', color: colors.whiteColor, fontSize: 20 }]} >{props.title} </Text>

            </View >
            {props.isMenu == true ?
                <Pressable
                    android_ripple={{ color: 'red', borderless: false, }}
                    onPress={props.onMenu}
                    style={{ paddingLeft: 20, height: 40, justifyContent: 'center' }}
                >
                    <Image
                        source={require('../../assets/menuvertical.png')}
                        style={[
                            {
                                tintColor: colors.whiteColor,
                                height: 30,
                                width: 30,
                            }]}
                    />

                </Pressable>
                : null
            }
        </View>

    )
}
const styles = StyleSheet.create({
    headerStyle: {
        height: 50,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: 'row',
        backgroundColor: colors.blueColor
    },
})
export const HeaderMemo = React.memo(Header1);