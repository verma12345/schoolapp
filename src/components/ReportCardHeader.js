import React, { useState } from 'react';
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';



export const ReportCardHeader = (props) => {
    const [event, setEvent] = useState({})
    const onLayout = (event) => {
        setEvent(event.nativeEvent)
    }
    const [activeIndex, setActiveIndex] = useState(-1)

    const renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    setActiveIndex(dataItem.index)
                    props.onSubject(dataItem.item.subject_name)
                }}
                style={{
                    backgroundColor: dataItem.index == activeIndex ? Colors.blue : Colors.whiteText,
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    marginHorizontal: 5,
                    borderRadius: 20,
                    marginTop: 20,
                    marginBottom: 5,
                    borderWidth: 1,
                    borderColor: Colors.lightGray
                }} >

                <Text style={[myStyle.textNormal, {
                    color: dataItem.index == activeIndex ? Colors.whiteText : Colors.grayColor,
                    fontSize: 11,

                }]}>{dataItem.item.subject_name}</Text>

            </TouchableOpacity>
        )
    }


    const renderHeaderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => {
                    setActiveIndex(-1)
                    props.onAll('All')
                }}
                style={{
                    backgroundColor: activeIndex == -1 ? Colors.blue : Colors.whiteText,
                    paddingHorizontal: 10,
                    paddingVertical: 5,
                    marginHorizontal: 5,
                    borderRadius: 20,
                    marginTop: 20,
                    marginBottom: 5,
                    borderWidth: 1,
                    borderColor: Colors.lightGray
                }} >

                <Text style={[myStyle.textNormal, {
                    color: activeIndex == -1 ? Colors.whiteText : Colors.grayColor,
                    fontSize: 11,

                }]}>{'All'}</Text>

            </TouchableOpacity>
        )
    }


    return (

        <View style={[myStyle.myshadow, {
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            alignItems: 'center',
            backgroundColor: Colors.whiteText,
            paddingVertical: 10,
            paddingBottom: 15
        }]} >

            <View style={[{
                // height: 80,
                width: '100%',
                justifyContent: "center",
                alignItems: "center",
                flexDirection: 'row',
                paddingHorizontal: 15,
            }, props.style]}>
                <TouchableOpacity
                    onPress={props.onPressBack}
                    style={{ position: 'absolute', left: 10, padding: 15, }}
                >
                    <Image
                        source={require('../../assets/back.png')}
                        style={{ height: 16, width: 16, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>

                <View style={{
                    justifyContent: "space-between",
                    alignItems: "center",
                    flexDirection: 'row',
                }} >
                    <Image
                        source={props.icon1}
                        style={{ height: 28, width: 28, marginRight: 10, borderRadius: 11 }}
                    />
                    <Text style={[myStyle.textBold, { color: Colors.textDarkColor, fontSize: 14, }]}>{props.txt}</Text>
                </View>

                {props.isMenu != true ?
                    null
                    : <TouchableOpacity
                        onLayout={onLayout}
                        onPress={() => props.onPressMenu(event)}
                        style={{ position: 'absolute', right: 10, padding: 10 }} >
                        <Image
                            source={props.icon2}
                            style={{ height: 25, width: 25 }}
                        />
                    </TouchableOpacity>
                }

            </View>

            <Text style={[myStyle.textBold, { marginTop: 16, color: Colors.textDarkColor, fontSize: 12, }]}>{props.year}</Text>
            {props.exam_name != null ?
                <Text style={[myStyle.textBold, { marginTop: 10, color: Colors.textDarkColor, fontSize: 22, }]}>{props.exam_name}</Text>
                : null}

            {props.exam_name == null ? <FlatList
                data={props.data}
                renderItem={renderItem}
                ListHeaderComponent={renderHeaderItem}
                keyExtractor={(item, index) => 'key' + index}
                horizontal
                showsHorizontalScrollIndicator={false}
            /> : null}
        </View>


    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20
    },
    btn: {
        height: 35,
        width: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})