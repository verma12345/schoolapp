import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';



export const ReportRow = (props) => {
    return (
        <View style={[myStyle.myshadow2, {
            flexDirection: 'row',
            justifyContent: "space-around",
            marginHorizontal: 25,
            marginVertical: 10,
            paddingHorizontal: 20,
            paddingVertical: 22,
            borderRadius: 20,
            alignItems: 'center',
            backgroundColor: Colors.whiteText,

        }, props.containerStyle]}>
            <View style={{ alignItems: 'center' }} >
                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>{"Grade"}</Text>
                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 14 }]}>{props.grade}</Text>
            </View>

            <View style={{ alignItems: 'center' }} >
                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>{"Percent"}</Text>
                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 14 }]}>{props.percentage}</Text>
            </View>

            <View style={{ alignItems: 'center' }} >
                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>{"Rank"}</Text>
                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 14 }]}>{props.rank}</Text>
            </View>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // paddingLeft: 50,
        marginTop: 20
    },
    btn: {
        height: 35,
        width: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 2,
        backgroundColor: Colors.backgroundColor
    },
    view1: {
        marginTop: 30
    }
})