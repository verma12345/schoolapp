import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../common/Colors';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';


export const VarificationHeader = (props) => {
    return (
        <View style={styles.container}>
            <Text style={[myStyle.textBold,{color:Colors.textColor,fontSize:30}]}>{props.txtHeading}</Text>
            <Text style={[myStyle.textNormal,{color:Colors.grayColor,textAlign:'center',paddingTop:10,fontSize:16}]}>{props.txt}</Text>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
   
})