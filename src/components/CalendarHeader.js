import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

export const CalendarHeader = (props) => {
    return (
        <View style={{
            backgroundColor: Colors.whiteText,
            borderBottomLeftRadius: 18,
            borderBottomRightRadius: 18,
            elevation: 2,
            paddingVertical:5

        }}>
            <TouchableOpacity onPress={props.onPressBack}               >
                <Image
                    source={require('../../assets/back.png')}
                    style={{ height: 18, width: 18, resizeMode: 'contain', marginLeft: 20, marginTop: 10 }}
                />
            </TouchableOpacity>

            <View style={{
                flexDirection: 'row',
                width: '60%',
                alignSelf: 'center',
                backgroundColor: Colors.whiteText,
                borderWidth: 1,
                borderRadius: 12,
                borderColor: Colors.lightGray,
                marginTop:18,
                marginBottom:20
            }}>
                <TouchableOpacity
                    onPress={props.onPressAttendance}
                    style={{ flex: 1, alignItems: 'center', paddingVertical: 6, borderRadius: 12, justifyContent: 'center', paddingHorizontal: 10, backgroundColor: props.is_attendance ? Colors.blue : Colors.whiteText }}>
                    <Text style={[myStyle.textNormal], { color: props.is_attendance ? "white" : "black", fontSize: 13 }}>{'Attendance'} </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={props.onPressHoliday}
                    style={{ flex: 1, alignItems: 'center', paddingVertical: 6, borderRadius: 12, justifyContent: 'center', backgroundColor: props.is_holiday ? Colors.blue : Colors.whiteText, paddingHorizontal: 15, }}>
                    <Text style={[myStyle.textNormal], { color: props.is_holiday ? "white" : "black", fontSize: 13 }}>{'Holidays'} </Text>
                </TouchableOpacity>


            </View>
        </View>
    )
}
