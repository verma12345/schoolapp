import React from 'react'
import { Text, TouchableOpacity, View,ImageBackground,Dimensions } from 'react-native';
import Colors from '../../common/Colors';
import { myStyle } from '../../common/myStyle';

const VideoPlayerRow = (props)=>{
    return(
        <View style={{
            // marginHorizontal:14
        }}>
              
              <TouchableOpacity
                // onPress={() => this.setIndex(dataItem.index)}
                onPress={()=>props.onPress(props.data_row)}
                style={{
                    height: 250,
                    width: '100%',
                   marginTop:15,
                    justifyContent: 'center',
                    borderRadius: 20
                }}>
                <ImageBackground
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                    imageStyle={{ resizeMode: "cover", borderRadius: 10 }}
                    source={props.data_row.thumbnail}
                >

                </ImageBackground>

                <Text style={[myStyle.textBold], { fontSize: 16,marginTop:10,marginHorizontal:12,marginBottom:12  }}>{props.data_row.video_name} </Text>
            </TouchableOpacity>
        </View>
    )
}

export default VideoPlayerRow;