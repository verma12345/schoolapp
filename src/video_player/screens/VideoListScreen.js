import React, { Component } from "react";
import { SafeAreaView, View, Text, Image, TouchableOpacity, ScrollView, FlatList, ImageBackground, Dimensions } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../../common/Colors";
import { myStyle } from "../../common/myStyle";
import { setVideoPlayerList, setVideoUrl } from "../../redux_store/actions/indexActionsVideo";

class VideoListScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0
        }

    }


    setIndex = (txt) => {
        console.log(txt);
        this.setState({ index: txt })
    }
    onPressBack = () => {
        this.props.navigation.goBack()
    }

    onPressVideo = (dataItem) => {
        console.log(dataItem.item);
        // return
        this.props.setVideoUrl(dataItem.item)
        this.props.setVideoPlayerList(this.props.chapterVideoList[this.state.index].videos)
        this.props.navigation.navigate('VideoPlayerScreen')
    }

    // continue watch videos list
    renderItemContinueWatch = (dataItem) => {
        // console.log(dataItem.item);
        return (
            <TouchableOpacity
                onPress={() => this.setIndex(dataItem.index)}
                style={{
                    height: 110,
                    width: Dimensions.get('window').width / 2.3,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 20
                }}>
                <ImageBackground
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                    imageStyle={{ resizeMode: "cover", borderRadius: 10 }}
                    source={dataItem.item.thumbnail}
                >

                    <Text style={[myStyle.textBold], { fontSize: 16, color: Colors.whiteText }}>{dataItem.item.title} </Text>
                    <Text style={[myStyle.textBold], { fontSize: 16, color: Colors.whiteText }}>{dataItem.item.description} </Text>

                </ImageBackground>
            </TouchableOpacity>
        )

    }


    renderItem = (dataItem) => {
        // console.log(dataItem.item);
        return (
            <TouchableOpacity
                onPress={() => this.setIndex(dataItem.index)}
                style={{
                    height: 90,
                    width: Dimensions.get('window').width / 2.5,
                    marginRight: 15,
                    backgroundColor: dataItem.item.bgColor,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 20
                }}>
                {/* <ImageBackground
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                    imageStyle={{ resizeMode: "cover", borderRadius: 10 }}
                    source={dataItem.item.thumbnail}
                > */}

                <Text style={[myStyle.textBold], { fontSize: 14, color: Colors.whiteText }}>{dataItem.item.title} </Text>
                <Text style={[myStyle.textBold], { fontSize: 14, color: Colors.whiteText }}>{dataItem.item.description} </Text>

                {/* </ImageBackground> */}
            </TouchableOpacity>
        )

    }

    // video list
    renderItemVideos = (dataItem) => {
        return (
            <View>
                <Text style={[myStyle.textBold], { fontSize: 14, marginLeft: 15, marginBottom: 15, marginTop: 8 }}>{dataItem.item.video_name} </Text>
                <TouchableOpacity
                    onPress={() => this.onPressVideo(dataItem)}
                    style={{
                        height: 200,
                        width: Dimensions.get('window').width / 1.02,
                        // margin: 15,
                        alignSelf: 'center',
                        borderRadius: 20
                    }}>
                    <ImageBackground
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        imageStyle={{ resizeMode: "cover", borderRadius: 20 }}
                        source={dataItem.item.thumbnail}
                    >
                        <Image
                            style={{ height: 35, width: 35, resizeMode: 'contain' }}
                            source={require('../../../assets/play.png')}
                        />
                    </ImageBackground>

                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={myStyle.container}>
                <View style={[myStyle.container]}>

                    {/* header */}
                    <View style={{
                        borderBottomRightRadius: 20,
                        borderBottomLeftRadius: 20,
                        backgroundColor: 'white',
                        elevation: 3,
                        paddingVertical: 12,
                        paddingBottom: 20
                    }}>

                        <TouchableOpacity
                            onPress={() => this.onPressBack()}
                            style={{ position: 'absolute', left: 0, padding: 15, }}
                        >
                            <Image
                                source={require('../../../assets/back.png')}
                                style={{ height: 17, width: 17, resizeMode: 'contain' }}
                            />
                        </TouchableOpacity>

                        <View style={{ marginTop: 42, marginLeft: 18 }}>
                            <Text style={[myStyle.textBold, { fontSize: 28 }]}>{'Chapter 1'} </Text>
                            <Text style={[myStyle.textBold, { fontSize: 28 }]}>{'Watch and learn'} </Text>
                        </View>
                    </View>


                    <ScrollView key={'1'}>
                        <View style={{ marginHorizontal: 14, marginTop: 16 }}>
                            <Text style={[myStyle.textBold], { fontSize: 14, marginBottom: 10 }}>{'Continue Watch'} </Text>
                            <FlatList
                                contentContainerStyle={{ paddingBottom: 15 }}
                                data={this.props.continueWatchVideoList}
                                renderItem={this.renderItemContinueWatch}
                                horizontal
                                keyExtractor={(item, index) => 'key' + index}
                            />
                        </View>




                        <View style={{ marginHorizontal: 14, marginTop: 16 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                                <Text style={[myStyle.textBold], { fontSize: 14 }}>{'Chapters'} </Text>
                                <Text
                                    // onPress={() => this.props.navigation.navigate('Testing')}
                                    style={[myStyle.textBold], { fontSize: 16, color: Colors.blue }}>
                                    {'View all'}
                                </Text>
                            </View>

                            <FlatList
                                contentContainerStyle={{ paddingBottom: 20 }}
                                data={this.props.chapterVideoList}
                                renderItem={this.renderItem}
                                horizontal
                                keyExtractor={(item, index) => 'key' + index}

                            />

                        </View>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: 30 }}
                            data={this.props.chapterVideoList[this.state.index].videos}
                            renderItem={this.renderItemVideos}
                            keyExtractor={(item, index) => 'key' + index}

                        />




                    </ScrollView>
                </View>
            </SafeAreaView >
        )
    }
}



const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const videos = state.indexReducerVideo;


    return {
        chapterVideoList: videos.chapterVideoList,
        continueWatchVideoList: videos.continueWatchVideoList
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(

        {
            setVideoPlayerList: (list) => setVideoPlayerList(list),

            setVideoUrl: (url) => setVideoUrl(url)
        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(VideoListScreen);
