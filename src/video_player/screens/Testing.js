import React, { Component } from "react";
import { SafeAreaView, View, Text, Image, TouchableOpacity, ScrollView, FlatList, ImageBackground, Dimensions, TextInput } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { debugLog } from "../../common/Constants";
import { myStyle } from "../../common/myStyle";
import { selectAll, setCheckUnCheck, setSelectedList } from "../../redux_store/actions/indexActionsVideo";


class Testing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0
        }
        this.isSelectAll = false
    }


    setIndex = (txt) => {
        console.log(txt);
        this.setState({ index: txt })
    }
    onPressBack = () => {
        this.props.navigation.goBack()
    }

    onPressVideo = () => {
        // this.props.navigation.navigate('VideoPlayerScreen')
    }
    onPressCheck = (index) => {
        this.onPressOK()
        this.props.setCheckUnCheck(index)

    }

    renderItem = (dataItem) => {
        console.log(dataItem.item.is_checked);
        return (
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={[myStyle.textBold], { fontSize: 14, marginLeft: 15, marginBottom: 15, marginTop: 8 }}>{dataItem.item.type} </Text>

                <TouchableOpacity onPress={() => this.onPressCheck(dataItem.index)}>
                    <Image
                        style={{ height: 30, width: 30 }}
                        source={dataItem.item.is_checked ? require('../../../assets/checked.png') : require('../../../assets/uncheck.png')}
                    />
                </TouchableOpacity>
            </View>
        )

    }


    selectAll = () => {
        this.isSelectAll = !this.isSelectAll
        debugLog(this.isSelectAll)
        this.props.selectAll(this.isSelectAll)
    }

    onPressOK = () => {
        let newData = []
        this.props.isCheckUnCheck.map((item, index) => {
            if (item.is_checked == true) {
                newData.push({
                    is_checked: item.is_checked,
                    type: item.type
                })
            }
        })
        // debugLog(newData)
        this.props.setSelectedList(newData)
        debugLog('testing')
        debugLog(this.props.selectedList)
    }

    render() {
        let row = this.props.selectedList.map((item, index) => {
            return (
                <View style={{
                    backgroundColor: 'white', marginVertical: 4, marginRight: 15, elevation: 2,
                    paddingHorizontal: 15, margin: 2, justifyContent: 'center', alignItems: 'center', borderRadius: 30
                }}>
                    <Text>
                        {item.type}
                    </Text>
                </View>
            )
        })
        return (
            <SafeAreaView style={myStyle.container}>
                <View style={[myStyle.container]}>

                    {/* header */}
                    <View style={{
                        borderBottomRightRadius: 20,
                        borderBottomLeftRadius: 20,
                        backgroundColor: 'white',
                        elevation: 3,
                        paddingVertical: 12,
                        paddingBottom: 20
                    }}>

                        <TouchableOpacity
                            onPress={() => this.onPressBack()}
                            style={{ position: 'absolute', left: 0, padding: 15, }}
                        >
                            <Image
                                source={require('../../../assets/back.png')}
                                style={{ height: 17, width: 17, resizeMode: 'contain' }}
                            />
                        </TouchableOpacity>

                        <View style={{ marginTop: 42, marginLeft: 18 }}>
                            <Text style={[myStyle.textBold, { fontSize: 28 }]}>{'Chapter 1'} </Text>
                            <Text style={[myStyle.textBold, { fontSize: 28 }]}>{'Watch and learn'} </Text>
                        </View>
                    </View>

                    <View style={{ height: 50, backgroundColor: 'green', borderRadius: 100, marginHorizontal: 7 }}>

                        <ScrollView showsHorizontalScrollIndicator={false} horizontal>
                            {row}
                            <TextInput
                                placeholder="Search"
                                style={{
                                    width: 300,
                                    backgroundColor: 'gray',
                                    borderRadius: 30,
                                    paddingHorizontal: 15,
                                    marginVertical: 4,
                                    marginRight: 10
                                }}
                            />
                        </ScrollView>


                    </View>



                    <View style={{ margin: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{ padding: 10, backgroundColor: 'lightgreen' }}
                            onPress={() => this.selectAll()}>
                            <Text>{'Select All'} </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ padding: 10, backgroundColor: 'lightgreen' }}
                            onPress={() => this.onPressOK()}>
                            <Text>{'OK'} </Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 30 }}
                        data={this.props.isCheckUnCheck}
                        renderItem={this.renderItem}
                    />

                </View>
            </SafeAreaView >
        )
    }
}



const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const videos = state.indexReducerVideo;


    return {
        isCheckUnCheck: videos.isCheckUnCheck,
        selectedList: videos.selectedList

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(

        {
            setCheckUnCheck: (is_checked) => setCheckUnCheck(is_checked),
            selectAll: (isTrue) => selectAll(isTrue),
            setSelectedList: (list) => setSelectedList(list)
        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Testing);
