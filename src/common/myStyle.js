import { StyleSheet } from "react-native";
import Colors from "./Colors";
import Font from "./Font";

export const myStyle = StyleSheet.create({
    textBold: {
        fontFamily: Font.Roboto,
        color: Colors.textDarkColor,
        fontWeight: '700',
        fontSize: 16
    },
    textNormal: {
        fontFamily: Font.SourceSansPro,
        color: Colors.textDarkColor,
        fontWeight: '500',
        fontSize: 16,
        color:'#28293D'
    },
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    myshadow: {
        shadowOffset: { width: 0, height: 3 },
        shadowColor: '#51C9FE',
        shadowOpacity: 0.6,
        elevation: 6,
        zIndex: 2
    },
    myshadow2: {
        shadowOffset: { width: 0, height: 3 },
        shadowColor: '#51C9FE',
        shadowOpacity: 0.6,
        elevation: 3,
        zIndex: 2
    },
    footerMain: {
        height: 50,
        width: "100%",
        flexDirection: 'row',
        backgroundColor: Colors.whiteColor,
    },
    footerOption: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },


    bottomCart: {
        width: '100%',
        paddingTop: 10,
        paddingBottom: 30,
        paddingHorizontal: 30,
        borderTopRightRadius: 24,
        borderTopStartRadius: 24,
        backgroundColor: "white",
        elevation: 10,
        zIndex: 1
    },

    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
        elevation: 10,
        padding: 5,
        zIndex: 1,
        backgroundColor: Colors.themeColor
    },
})