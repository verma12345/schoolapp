const blue = '#000D83';
const darkBlue = '#050049';
const skyBlue = '#51C9FE';
const darkSky = '#1361F9'

const dark = '#28293D';
const grayColor = '#909090';
const lightGray = '#E9E9E9';
const lightblue = '#D8EFFF';

const whiteText = '#FFFFFF';
const statusBarColor = '#1050E6';
const placeHolderColor = '#C4C4C4';
const textColor = '#28293D';
const textDarkColor = "#28293D"
const progressColor = '#9E9E9E';
const greenColor = '#27AE60';
const backgroundColor = '#F8FAFF';
const lightGreen = '#ADCE93'
const calendarHoliday = '#C0ECB0'
const calendarPresent = '#000D83'
const calendarLeaveAbsent = '#E9E9E9'
const calendarCurrentDate = '#FFAEAE'
const textinputcolor='#F1F4FB';
const googletextcolor='#f57f17';
const signuptextcolor='#00b0ff';
const btncolor='#E0E6F3';
const orangeColor ="#f39f55"
const lightorange="#FDF0E4"
const lightRed = "#FCE6E6"
const lightgreen ="#DFF3E7"

export default {
  blue,
  darkBlue,
  skyBlue,
  dark,
  darkSky,
  lightGray,
  tabIconDefault: '#384659',
  whiteText,
  progressColor,
  statusBarColor,
  red: "red",
  grayColor,
  placeHolderColor,
  textColor,
  textDarkColor,
  greenColor,
  backgroundColor,
  lightblue,
  lightGreen,
  calendarHoliday,
  calendarPresent,
  calendarLeaveAbsent,
  calendarCurrentDate,
  textinputcolor,
  googletextcolor,
  signuptextcolor,
  btncolor,
  orangeColor,
  lightorange,
  lightRed,
  lightgreen
};