import { PixelRatio, StyleSheet } from "react-native";
import colors from "./colors";
import Font from "./Font";

const css = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.themeColor,
        // justifyContent: 'center',
        // alignItems: "center"
    },
    textStyle: {
        fontSize: 16,
        fontFamily: Font.Roboto,
        fontWeight: '500',
        color: colors.themeItemColor
    },
    footerMain: {
        height: 50,
        width: "100%",
        flexDirection: 'row',
        backgroundColor: colors.whiteColor,
    },
    footerOption: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },


    bottomCart: {
        width: '100%',
        // paddingTop: 10,
        paddingBottom: 30,
        paddingHorizontal: 30,
        borderTopRightRadius: 24,
        borderTopStartRadius: 24,
        backgroundColor: "white",
        elevation: 10,
        zIndex: 1
    },

    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10,
        elevation: 10,
        padding: 5,
        zIndex: 1,
        backgroundColor: colors.themeColor
    },

    myshadow: {
        shadowColor: "#1050e6",
        shadowOpacity: 0.15,
        shadowOffset: {
            width: 0,
            height: 0,
        },
        shadowRadius: 8.30,
        elevation: 13,
        zIndex: 5
    },
    radious:{
        backgroundColor:colors.whiteColor,
        borderRadius:100
    }
})

export default css