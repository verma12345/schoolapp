const whiteColor = 'white'
const themeColor = 'white'
const themeItemColor = 'gray'
const grayColor = 'gray'
const blueColor = '#6200EE'
export default {
    whiteColor,
    themeColor,
    grayColor,
    themeItemColor,
    blueColor
}