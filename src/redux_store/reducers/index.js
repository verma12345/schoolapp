import { combineReducers } from 'redux';
import indexReducer from './indexReducer';
import indexReducerAkshita from './indexReducerAkshita';
import indexReducerJaswant from './indexReducerJaswant';
import indexReducerVideo from './indexReducerVideo';

export default combineReducers({
    indexReducer: indexReducer,
    indexReducerAkshita:indexReducerAkshita,
    indexReducerJaswant:indexReducerJaswant,
    indexReducerVideo:indexReducerVideo
})

