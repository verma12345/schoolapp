import { EMAIL_OTP, EVENT_LIST, GALLERY_LIST, GALLERY_PAGER, RANDOM_NUM_OTP, SET_ADD_STUDENT_LIST, SET_CLASS_TEACHER_DETAILS, SET_EVENT_DETAILS, SET_FEE_LIST, SET_FEE_RECEIPT_LIST, SET_FEE_STRUCTURE_LIST, SET_GALLERY_IMAGE_DETAILS, SET_INVOICE_LIST, SET_QUIZ_RESULT_LIST, SET_RECENT_TEST_LIST, SET_SELECTED_GALLERY_LIST, SET_STUDENTS_LIST, SET_TEACHER_LIST, SET_TIMETABLE_LIST, SMS_OTP } from "../types/typesAkshita";
import { SET_ABOUT_QUIZ, SET_QUIZ_SESSION } from "../types/typesJaswant";


const initialState = {
  quiz_session: '',
  about_quiz: {},
  recentTestList: [
    {
      test_name: 'English',
      time: '9:30 pm'
    },
    {
      test_name: 'English',
      time: '9:30 pm'
    },
    {
      test_name: 'English',
      time: '9:30 pm'
    }
  ],

  quizResultList:
  {
    current_rank: "1",
    attempt: "28/30",
    skip: 2,
    wrong: 12,
    percentage: '91%',
    remark: 'Keep it up!'
  },

  // signup verification var
  email_otp: '',
  sms_otp: '',
  random_num: '',

  //timetale var
  days_list: [],

  // teacher var
  teacher_list: [],
  classTeacherDetails: {},

  //add student
  addStudentList: null,
  students_list: [],
  searchStudentList: [],

  //payment
  payment_record: [
    { fee_type: `Cultural Activities`, fee: '₹ 200', },
    { fee_type: 'Development Fee', fee: '₹ 60' },
    { fee_type: 'Electricity Generator', fee: '₹ 600' },
    { fee_type: 'Games Fee', fee: '₹ 100' },
    { fee_type: 'Lab Fee', fee: '₹ 20' },
    { fee_type: 'Science Fee', fee: '₹ 250' },
    { fee_type: 'Term Fee', fee: '₹ 1200' },
    { fee_type: 'Tution Fee', fee: '₹ 1400' },

  ],

  gallery_list: [

    {
      category: "College",
      images: [
        { image: require('../../../assets/college1.png'), title: "College", image_heading: 'Image Heading', image_description: 'Image description' },
        { image: require('../../../assets/college2.png'), title: "College", image_heading: 'Image Heading', image_description: 'Image description' },
        { image: require('../../../assets/college1.png'), title: "College", image_heading: 'Image Heading', image_description: 'Image description' },
      ]
    },
    {
      category: "Classes",
      images: [
        // { image: require('../../../assets/classes1.png') },
        // { image: require('../../../assets/classes.png') },
        { image: require('../../../assets/classes3.png'), title: "Classes", image_heading: 'Image Heading', image_description: 'Image description' },
        { image: require('../../../assets/classes3.png'), title: "Classes", image_heading: 'Image Heading', image_description: 'Image description' },
        { image: require('../../../assets/classes3.png'), title: "Classes", image_heading: 'Image Heading', image_description: 'Image description' },
      ]
    },
    {
      category: "Staffs",
      images: [
        { image: require('../../../assets/staffs.png'), title: "Staffs", image_heading: 'Image Heading', image_description: 'Image description' },
        { image: require('../../../assets/staff2.png'), title: "Staffs", image_heading: 'Image Heading', image_description: 'Image description' },
        { image: require('../../../assets/staffs.png'), title: "Staffs", image_heading: 'Image Heading', image_description: 'Image description' },
      ]
    }
  ],

  gallery_pager: [
    { image: require('../../../assets/gallery1.png') },
    { image: require('../../../assets/gallery1.png') },
    { image: require('../../../assets/gallery1.png') },
  ],
  selectedGalleryList: [],
  galleryImageDetails: null,

  // events
  eventList: [
    {
      upcoming: [
        {
          event_month: "AUG",
          event_date: "20",
          event_name: "Football Competition",
          event_timing: "08:30 AM - 09:00 PM",
          event_address: "Indira Nagar, Lucknow",
        },
        {
          event_month: "AUG",
          event_date: "25",
          event_name: "Seminar",
          event_timing: "10:00 AM to 01:00 PM",
          event_address: "Indira Nagar, Lucknow",
        }
      ],
    },
    {
      past: [
        {
          event_month: "JULY",
          event_date: "30",
          event_name: "Football Competition",
          event_timing: "08:30 AM - 09:00 PM",
          event_address: "Indira Nagar, Lucknow",
        },
        {
          event_month: "JUNE",
          event_date: "15",
          event_name: "Seminar",
          event_timing: "10:00 AM to 01:00 PM",
          event_address: "Indira Nagar, Lucknow",
        }
      ]
    }
  ],
  eventDetails: null,


  // FEE SCREEN
  feeDetailList: [],
  invoiceList: [],
  feeReceiptDetailsList:[],
  feeStructureList:[]


};

export default function (state = initialState, action) {
  switch (action.type) {

    // signup verification cases

    case EMAIL_OTP:
      return {
        ...state,
        email_otp: action.payload,
      };

    case SMS_OTP:
      return {
        ...state,
        sms_otp: action.payload,
      };

    case RANDOM_NUM_OTP:
      return {
        ...state,
        random_num: action.payload,
      };

    //timetable  cases
    case SET_TIMETABLE_LIST:
      return {
        ...state,
        days_list: action.payload
      }

    // teacher cases
    case SET_TEACHER_LIST:
      return {
        ...state,
        teacher_list: action.payload
      }

    case SET_CLASS_TEACHER_DETAILS:
      return {
        ...state,
        classTeacherDetails: action.payload,
      };

    //add student cases
    case SET_ADD_STUDENT_LIST:
      return {
        addStudentList: action.payload
      }

    case SET_STUDENTS_LIST:
      return {
        ...state,
        students_list: action.payload,
      };

    // gallery
    case GALLERY_LIST:
      return {
        ...state,
        gallery_list: action.payload
      }

    case GALLERY_PAGER:
      return {
        ...state,
        gallery_pager: action.payload
      }

    case SET_SELECTED_GALLERY_LIST:
      return {
        ...state,
        selectedGalleryList: action.payload
      }

    case SET_GALLERY_IMAGE_DETAILS:
      return {
        ...state,
        galleryImageDetails: action.payload
      }

    //events
    case EVENT_LIST:
      return {
        ...state,
        eventList: action.payload
      }
    case SET_EVENT_DETAILS:
      return {
        ...state,
        eventDetails: action.payload
      }

    // FEE SCREEN VAR
    case SET_FEE_LIST:
      return {
        ...state,
        feeDetailList: action.payload
      }

    case SET_INVOICE_LIST:
      return {
        ...state,
        invoiceList: action.payload
      }

      case SET_FEE_RECEIPT_LIST:
        return {
          ...state,
          feeReceiptDetailsList: action.payload
        }

        case SET_FEE_STRUCTURE_LIST:
          return {
            ...state,
            feeStructureList: action.payload
          }

    //quiz
    case SET_QUIZ_SESSION:
      return {
        ...state,
        quiz_session: action.payload
      }

    case SET_ABOUT_QUIZ:
      return {
        ...state,
        about_quiz: action.payload
      }

    case SET_RECENT_TEST_LIST:
      return {
        ...state,
        recentTestList: action.payload
      }

    case SET_QUIZ_RESULT_LIST:
      return {
        ...state,
        quizResultList: action.payload
      }
    default:
      return state;
  }
}
