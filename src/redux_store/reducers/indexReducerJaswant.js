import { SET_ALOT_EXAM, SET_ATTEMPT_QUIZ, SET_ATTENDANCE_LIST, SET_CLASS_TEST_DATA, SET_COUPAN_CODE, SET_ELEARNING_LIST, SET_ELEARNING_SYLLABUS_LIST, SET_FEED, SET_FEEDISACTIVE, SET_GET_QUESTION, SET_HOLIDAY_LIST, SET_IS_CHECKED, SET_IS_INTERNET_CONNECTED, SET_IS_LOADING_PREVIOUS, SET_IS_LOADING_REVIEW, SET_IS_LOADING_SAVE_AND_NEXT, SET_IS_LOADING_SAVE_AND_SUBMIT, SET_IS_LOADING_SKIP, SET_IS_PREVIOUS, SET_IS_SAVE_AND_NEXT, SET_IS_SUBMIT, SET_IS_SUBMIT_LOADING, SET_MOBILE, SET_NOTIFICATION, SET_NOTIFICATIONISACTIVE, SET_PASS, SET_POSITION, SET_PROGRESS, SET_PROGRESS_INTERVAL, SET_QUESTION_LIST, SET_QUIZ_ATTEMPT_INDEX, SET_QUIZ_DETAILS, SET_QUIZ_INFO, SET_QUIZ_SESSION, SET_REPORT_CARD_LIST, SET_REPORT_CARD_MARKS_LIST, SET_SAVE_ANSWER_ID, SET_SAVE_QUIZ_ID, SET_SELETCT_OPTION, SET_SESSION_YEAR, SET_SHOW_PASS, SET_SKIP_QUIZ, SET_TIME, SET_TOTAL_SECONDS, SET_TOTAL_TEACHER_COUNT, SET_TOTAL_TIME, SET_WEEK_DAY } from "../types/typesJaswant";


const initialState = {
  quiz_session: '',
  exam_info: {},
  alot_exam: {},
  coupan_code: '',
  isCheckedQuiz: false,
  quiz_list: [],
  quiz_attempt_index: 0,
  skiped_quiz: 0,
  attempt_quiz: 0,
  quiz_details: [],
  get_question: {},
  isInternet: false,

  save_a_id: "",
  save_q_id: 7789,

  isClickedOption: false,

  // by akshita
  isSubmit: false,
  isLoadingReview: false,
  isLoadingPrevious: false,
  isLoadingSaveAndNext: false,
  isLoadingSkip: false,
  isLoadingSaveAndSubmit: false,
  submitLoading: false,
  isPrevious: false,
  isSaveAndNext: false,
  progressIterval: 0,
  progress: 0,
  total_time: {
    "h": 0,
    "m": 0,
    "s": 0
  },
  time: {
    "h": 0,
    "m": 0,
    "s": 0,
  },
  total_seconds: 0,

  //view pager
  position: 0,
  // login
  mobile: '',
  password: '',
  showpass: true,
  // dashboard
  totalTeacherCounts: 0,
  sessionYear: '',
  empty_week_day: [],

  // student details screen
  notification: [],

  feed: [
    {
      icon: require('../../../assets/tick.png'),
      subject: 'Physics',
      message: 'Read and finish energy chapter unter 22 July.',
      heading: 'Work was rated',
      content: 'English + Examination + Traditional poems, nursery rhymes',
      feedcount: '1',
      date: 'fri 11:34'
    },
    {
      icon: require('../../../assets/tick.png'),
      subject: 'Physics',
      message: 'Read and finish energy chapter unter 22 July.',
      heading: 'Work was rated',
      content: 'English + Examination + Traditional poems, nursery rhymes',
      feedcount: '1',
      date: 'fri 11:34'
    },
    {
      icon: require('../../../assets/tick.png'),
      subject: 'Physics',
      message: 'Read and finish energy chapter unter 22 July.',
      heading: 'Work was rated',
      content: 'English + Examination + Traditional poems, nursery rhymes',
      feedcount: '1',
      date: 'fri 11:34'
    },
    {
      icon: require('../../../assets/tick.png'),
      subject: 'Physics',
      message: 'Read and finish energy chapter unter 22 July.',
      heading: 'Work was rated',
      content: 'English + Examination + Traditional poems, nursery rhymes',
      feedcount: '1',
      date: 'fri 11:34'
    },
    {
      icon: require('../../../assets/tick.png'),
      subject: 'Physics',
      message: 'Read and finish energy chapter unter 22 July.',
      heading: 'Work was rated',
      content: 'English + Examination + Traditional poems, nursery rhymes',
      feedcount: '1',
      date: 'fri 11:34'
    },
  ],
  notificationIsActive: true,
  feedIsActive: false,

  // report card list
  reportCardList: [],

  class_test_data: [],
  reportCardMarksList: [],

  // calander
  holiday_list: [
    {
      month: 1,
      days: [{ "day": 15, "name": "Dashera 1" }]
    },
    {
      month: 2,
      days: [{ "day": 4, "name": "Diwali-1" }, { "day": 5, "name": "Diwali-2" }],
    },
    {
      month: 3,
      days: [{ "day": 15, "name": "Dashera 2" }]
    },
    {
      month: 4,
      days: [{ "day": 4, "name": "Diwali-55" }, { "day": 5, "name": "Diwali-2" }],
    },
    {
      month: 5,
      days: [{ "day": 15, "name": "Dashera 3" }]
    },
    {
      month: 6,
      days: [{ "day": 4, "name": "Diwali-3" }, { "day": 5, "name": "Diwali-2" }],
    },
    {
      month: 7,
      days: [{ "day": 15, "name": "Dashera 4" }]
    },
    {
      month: 8,
      days: [{ "day": 4, "name": "Diwali-4" }, { "day": 5, "name": "Diwali-2" }],
    },
    {
      month: 9,
      days: [{ "day": 15, "name": "Dashera 5" }]
    },
    {
      month: 10,
      days: [{ "day": 4, "name": "Diwali-5" }, { "day": 5, "name": "Diwali-2" }],
    },
    {
      month: 11,
      days: [{ "day": 15, "name": "Dashera 6" }]
    },
    {
      month: 12,
      days: [{ "day": 4, "name": "Diwali-6" }, { "day": 5, "name": "Diwali-2" }, { "day": 6, "name": "Diwali-3" }, { "day": 16, "name": "Diwali-13" }],
    },
  ],
  attendanceList: [],

  eLearningList: [
    {
      subject_name: "English",
      chapters: "15 Chapters",
      syllabus: [
        {
          chapter_name: 'Chapter 1 Name',
          chapter_desc: 'eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi',
          chapter_video: [{

          }],
          chapter_quiz: [{
            quiz_desc: "desc of quiz",
            quiz_instruction: `eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
            Praesentium voluptatum eleniti atque corrupti
            quos dolores et quas molestias excepturi
            eos et accusamus et iusto odio dignissimos
            Praesentium voluptatum eleniti atque corrupti
            quos dolores et quas molestias excepturi`
          }],

          help: "Get Expert Help"

        },
        {
          chapter_name: 'Chapter 6 Name',
          chapter_desc: 'Detail about this chapter'
        },
        {
          chapter_name: 'Chapter 7 Name',
          chapter_desc: 'Detail about this chapter'
        },

      ]
    },
    {
      subject_name: "Hindi",
      chapters: "12 Chapters",
      syllabus: [
        {
          chapter_name: 'Chapter 1 Name',
          chapter_desc: 'Detail about this chapter'
        },
        {
          chapter_name: 'Chapter 2 Name',
          chapter_desc: 'Detail about this chapter'
        },
        {
          chapter_name: 'Chapter 3 Name',
          chapter_desc: 'Detail about this chapter'
        },
      ]
    },
  ],

  eLearningSyllabusList: []
};

export default function (state = initialState, action) {
  switch (action.type) {

    // E-learning
    case SET_ELEARNING_LIST:
      return {
        ...state,
        eLearningList: action.payload
      }

    case SET_ELEARNING_SYLLABUS_LIST:
      return {
        ...state,
        eLearningSyllabusList: action.payload
      }

    // calander
    case SET_HOLIDAY_LIST:
      return {
        ...state,
        holiday_list: action.payload
      }

    case SET_ATTENDANCE_LIST:
      return {
        ...state,
        attendanceList: action.payload
      }

    // report card list
    case SET_REPORT_CARD_LIST:
      return {
        reportCardList: action.payload
      }

    case SET_REPORT_CARD_MARKS_LIST:
      return {
        reportCardMarksList: action.payload
      }

    case SET_CLASS_TEST_DATA:
      return {
        class_test_data: action.payload
      }


    // student details screen

    case SET_WEEK_DAY:
      return {
        ...state,
        empty_week_day: action.payload,
      };


    case SET_NOTIFICATION:
      return {
        ...state,
        notification: action.payload
      }

    case SET_FEED:
      return {
        ...state,
        feed: action.payload
      }

    case SET_FEEDISACTIVE:
      return {
        ...state,
        feedIsActive: action.payload
      }

    case SET_NOTIFICATIONISACTIVE:
      return {
        ...state,
        notificationIsActive: action.payload
      }

    // dashboard

    case SET_TOTAL_TEACHER_COUNT:
      return {
        ...state,
        totalTeacherCounts: action.payload
      }

    case SET_SESSION_YEAR:
      return {
        sessionYear: action.payload
      }





    // +++++++++++++++++++++
    case SET_POSITION:
      return {
        ...state,
        position: action.payload,
      };

    case SET_MOBILE:
      return {
        ...state,
        mobile: action.payload
      }

    case SET_PASS:
      return {
        ...state,
        password: action.payload
      }

    case SET_SHOW_PASS:
      return {
        ...state,
        showpass: action.payload
      }


    // quiz
    case SET_IS_INTERNET_CONNECTED:
      return {
        ...state,
        isInternet: action.payload
      }


    case SET_SAVE_QUIZ_ID:
      return {
        ...state,
        save_q_id: action.payload
      }

    case SET_TOTAL_SECONDS:
      return {
        ...state,
        total_seconds: action.payload
      }


    case SET_TOTAL_TIME:
      return {
        ...state,
        total_time: action.payload
      }


    case SET_TIME:
      return {
        ...state,
        time: action.payload
      }


    case SET_PROGRESS_INTERVAL:
      return {
        ...state,
        progressIterval: action.payload
      }


    case SET_PROGRESS:
      return {
        ...state,
        progress: action.payload
      }

    // by akshita
    case SET_IS_SAVE_AND_NEXT:
      return {
        ...state,
        isSaveAndNext: action.payload
      }


    case SET_IS_SUBMIT:
      return {
        ...state,
        isSubmit: action.payload
      }

    case SET_IS_LOADING_REVIEW:
      return {
        ...state,
        isLoadingReview: action.payload
      }

    case SET_IS_LOADING_PREVIOUS:
      return {
        ...state,
        isLoadingPrevious: action.payload
      }

    case SET_IS_LOADING_SAVE_AND_NEXT:
      return {
        ...state,
        isLoadingSaveAndNext: action.payload
      }

    case SET_IS_LOADING_SKIP:
      return {
        ...state,
        isLoadingSkip: action.payload
      }

    case SET_IS_LOADING_SAVE_AND_SUBMIT:
      return {
        ...state,
        isLoadingSaveAndSubmit: action.payload
      }

    case SET_IS_SUBMIT_LOADING:
      return {
        ...state,
        submitLoading: action.payload
      }

    case SET_IS_PREVIOUS:
      return {
        ...state,
        isLoadingReview: action.payload
      }
    // ***********************88
    case SET_SAVE_ANSWER_ID:
      return {
        ...state,
        save_a_id: action.payload
      }

    case SET_SELETCT_OPTION:
      return {
        ...state,
        isClickedOption: action.payload
      }


    case SET_QUIZ_SESSION:
      return {
        ...state,
        quiz_session: action.payload
      }

    case SET_QUIZ_INFO:
      return {
        ...state,
        exam_info: action.payload
      }

    case SET_ALOT_EXAM:
      return {
        ...state,
        alot_exam: action.payload
      }

    case SET_COUPAN_CODE:
      return {
        ...state,
        coupan_code: action.payload
      }

    case SET_IS_CHECKED:
      return {
        ...state,
        isCheckedQuiz: action.payload
      }

    case SET_QUESTION_LIST:
      return {
        ...state,
        quiz_list: action.payload
      }

    case SET_QUIZ_ATTEMPT_INDEX:
      return {
        ...state,
        quiz_attempt_index: action.payload
      }

    case SET_SKIP_QUIZ:
      return {
        ...state,
        skiped_quiz: action.payload
      }

    case SET_ATTEMPT_QUIZ:
      return {
        ...state,
        attempt_quiz: action.payload
      }

    case SET_QUIZ_DETAILS:
      return {
        ...state,
        quiz_details: action.payload
      }

    case SET_GET_QUESTION:
      return {
        ...state,
        get_question: action.payload
      }



    default:
      return state;
  }
}
