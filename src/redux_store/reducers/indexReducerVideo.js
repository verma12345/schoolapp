import { Dimensions } from "react-native";
import { SET_LOADING, SET_REPORT_CARD_MARKS_LIST } from "../types/types";
import { SELECTED_LIST, SET_CHECK_UNCHECK, SET_VIDEO_PLAYER_LIST, SET_VIDEO_URL } from "../types/typeVideo";

const { width, height } = Dimensions.get('window');


const initialState = {
  is_loading: false,

  continueWatchVideoList: [
    {
      title: "title1",
      description: "Chapter Name here",
      thumbnail: require('../../../assets/staffs.png'),
    },
    {
      title: "title2",
      description: "Chapter Name here",
      thumbnail: require('../../../assets/classes3.png'),

    },
    {
      title: "title2",
      description: "Chapter Name here",
      thumbnail: require('../../../assets/staff2.png'),

    }
  ],

  chapterVideoList: [
    {
      title: "title1",
      description: "Chapter Name here",
      bgColor: '#4ECEAF',
      thumbnail: require('../../../assets/classes.png'),
      videos: [
        {
          thumbnail: require('../../../assets/staffs.png'),
          url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
          video_name: `Chapter's Topic name 1`
        },
        {
          thumbnail: require('../../../assets/video_thumbnail.png'),
          url: "http://techslides.com/demos/sample-videos/small.mp4",
          video_name: `Chapter's Topic name 2`
        }
      ]
    },
    {
      title: "title2",
      description: "Chapter Name here",
      bgColor: '#848DEA',
      thumbnail: require('../../../assets/staff2.png'),
      videos: [
        {
          thumbnail: require('../../../assets/event1.png'),
          url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
          video_name: `Chapter's Topic name 1`
        },
        {
          thumbnail: require('../../../assets/classes3.png'),
          url: "http://techslides.com/demos/sample-videos/small.mp4",
          video_name: `Chapter's Topic name 2`
        }
      ]
    },
    {
      title: "title3",
      description: "Chapter Name here",
      bgColor: 'orange',
      thumbnail: require('../../../assets/staffs.png'),
      videos: [
        {
          thumbnail: require('../../../assets/video_thumbnail.png'),
          url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
          video_name: `Chapter's Topic name 1`
        },
        {
          thumbnail: require('../../../assets/college2.png'),
          url: "http://techslides.com/demos/sample-videos/small.mp4",
          video_name: `Chapter's Topic name 2`
        }
      ]
    }
  ],

  videoPlayerList:[],
  videoUrl:null,





  // FOR TESTING ONLY 
  isCheckUnCheck: [
    {
      type: "Simple",
      is_checked: true
    },
    {
      type: "Moderate",
      is_checked: false
    },
    {
      type: "Hard",
      is_checked: false
    },
  ],
  isSelectAll: false,
  selectedList:[],
  reportCardMarksList: []

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_REPORT_CARD_MARKS_LIST:
      return {
        reportCardMarksList: action.payload
      }

    // ---------------
    case SET_LOADING:
      return {
        ...state,
        is_loading: action.payload,
      };
     
      case SET_VIDEO_PLAYER_LIST:
        return {
          ...state,
          videoPlayerList: action.payload,
        };
       
        case SET_VIDEO_URL:
          return {
            ...state,
            videoUrl: action.payload,
          };
         






      // FOR TESTING ONLY
      case SET_CHECK_UNCHECK:
        return {
          ...state,
          isCheckUnCheck: action.payload,
        };

        case SELECTED_LIST:
          return {
            ...state,
            selectedList: action.payload,
          };
    default:
      return state;
  }
}
