import { SET_LOADING, SET_LOGIN_EMAIL_MOBILE, SET_LOGIN_PASS, SET_LOGIN_SHOW_PASS, SET_SIGNUP_CONF_PASS, SET_SIGNUP_EMAIL, SET_SIGNUP_MOBILE, SET_SIGNUP_NAME, SET_SIGNUP_PASS, SET_SIGNUP_QUALIFICATION, SET_SIGNUP_SHOW_CONF_PASS, SET_SIGNUP_SHOW_PASS, SET_USER_ID } from "../types/types";
import { SEARCH_STUDENT, SET_SELECTED_GALLERY_LIST } from "../types/typesAkshita";
import { SET_STUDENTS_LIST, SET_STUDENT_DETAILS } from "../types/typesJaswant";


const initialState = {
  isLoading: false,

  user_id: '111',

  // login var
  login_email_mobile: '',
  login_pass: '',

  // signup var
  signup_name: '',
  signup_email: '',
  signup_mobile: '',
  signup_qualification: '',
  signup_pass: '',
  signup_conf_pass: '',

  isLoginShowPass: false,
  isSignupShowPass: false,
  isSignupConfPass: false,
  searchStudentList: [],
  students_details: [],
  students_list: [],

  //GALLERY
  selectedGalleryList: [],

};

export default function (state = initialState, action) {
  switch (action.type) {

    case SET_STUDENT_DETAILS:
      return {
        ...state,
        students_details: action.payload

      }

    case SET_STUDENTS_LIST:
      return {
        ...state,
        students_list: action.payload,
      };

    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload
      }


    case SET_USER_ID:
      return {
        ...state,
        user_id: action.payload
      }

    // login cases

    case SET_LOGIN_EMAIL_MOBILE:
      return {
        ...state,
        login_email_mobile: action.payload
      }

    case SET_LOGIN_PASS:
      return {
        ...state,
        login_pass: action.payload
      }

    // signup cases

    case SET_SIGNUP_NAME:
      return {
        ...state,
        signup_name: action.payload
      }

    case SET_SIGNUP_EMAIL:
      return {
        ...state,
        signup_email: action.payload
      }

    case SET_SIGNUP_MOBILE:
      return {
        ...state,
        signup_mobile: action.payload
      }

    case SET_SIGNUP_QUALIFICATION:
      return {
        ...state,
        signup_qualification: action.payload
      }

    case SET_SIGNUP_PASS:
      return {
        ...state,
        signup_pass: action.payload
      }

    case SET_SIGNUP_CONF_PASS:
      return {
        ...state,
        signup_conf_pass: action.payload
      }



    case SET_LOGIN_SHOW_PASS:
      return {
        ...state,
        isLoginShowPass: action.payload
      }

    case SET_SIGNUP_SHOW_PASS:
      return {
        ...state,
        isSignupShowPass: action.payload
      }

    case SET_SIGNUP_SHOW_CONF_PASS:
      return {
        ...state,
        isSignupConfPass: action.payload
      }

    // ADD STUDENT LIST FILTER
    case SEARCH_STUDENT:
      return {
        ...state,
        searchStudentList: action.payload,
      };

    // GALLERY 
    case SET_SELECTED_GALLERY_LIST:
      return {
        ...state,
        selectedGalleryList: action.payload
      }

    default:
      return state;
  }
}
