import { debugLog } from "../../common/Constants";
import store from "../store";
import {
  SET_ALOT_EXAM, SET_ATTEMPT_QUIZ, SET_ATTENDANCE_LIST, SET_CLASS_TEST_DATA, SET_COUPAN_CODE, SET_ELEARNING_LIST, SET_ELEARNING_SYLLABUS_LIST, SET_FEED, SET_FEEDISACTIVE, SET_GET_QUESTION, SET_HOLIDAY_LIST, SET_IS_CHECKED, SET_IS_INTERNET_CONNECTED,
  SET_IS_LOADING_PREVIOUS, SET_IS_LOADING_REVIEW, SET_IS_LOADING_SAVE_AND_NEXT, SET_IS_LOADING_SAVE_AND_SUBMIT,
  SET_IS_LOADING_SKIP, SET_IS_PREVIOUS, SET_IS_SAVE_AND_NEXT, SET_IS_SUBMIT, SET_IS_SUBMIT_LOADING, SET_LOGIN_INFO,
  SET_MOBILE, SET_NOTIFICATION, SET_NOTIFICATIONISACTIVE, SET_PASS, SET_POSITION, SET_PROGRESS, SET_PROGRESS_INTERVAL, SET_QUESTION_LIST, SET_QUIZ_ATTEMPT_INDEX,
  SET_QUIZ_DETAILS, SET_QUIZ_INFO, SET_QUIZ_SESSION, SET_REPORT_CARD_LIST, SET_REPORT_CARD_MARKS_LIST, SET_SAVE_ANSWER_ID, SET_SAVE_QUIZ_ID, SET_SELETCT_OPTION,
  SET_SESSION_YEAR, SET_SHOW_PASS, SET_SKIP_QUIZ, SET_STUDENTS_LIST, SET_STUDENT_DETAILS, SET_TIME, SET_TOTAL_SECONDS,
  SET_TOTAL_TEACHER_COUNT, SET_TOTAL_TIME, SET_WEEK_DAY
} from "../types/typesJaswant";

// e-learning
export const setELearningList = (list) => (dispatch) => {
  debugLog(list)
  dispatch({
    type: SET_ELEARNING_LIST,
    payload: list
  })
}
export const setELearningSyllabusList = (list) => (dispatch) => {
  dispatch({
    type: SET_ELEARNING_SYLLABUS_LIST,
    payload: list
  })
}

// Calander
export const setHolidayList = (holiday_list) => (dispatch) => {
  dispatch({
    type: SET_HOLIDAY_LIST,
    payload: holiday_list
  })
}

export const setAtendanceList = (attendance_list) => (dispatch) => {
  dispatch({
    type: SET_ATTENDANCE_LIST,
    payload: attendance_list
  })
}
// exam list screen

export const setReportCardList = (list) => (dispatch) => {

  dispatch({
    type: SET_REPORT_CARD_LIST,
    payload: list
  })
}

export const setReportCardMarksList = (list) => (dispatch) => {

  dispatch({
    type: SET_REPORT_CARD_MARKS_LIST,
    payload: list
  })
}



export const setClassTestData = (list) => (dispatch) => {

  dispatch({
    type: SET_CLASS_TEST_DATA,
    payload: list
  })
}



// student details screen

export const setEmptyWeek = () => (dispatch) => {

  let day = new Date()
  let newData = []
  for (let index = 1; index <= day.getDay(); index++) {
    // debugLog(index + " : " + day.getDay())
    newData.push({
      empty: ''
    })
  }
  debugLog('emppty')
  debugLog(newData)

  dispatch({
    type: SET_WEEK_DAY,
    payload: newData,
  });
};


export const setNotification = (notification) => (dispatch) => {
  dispatch({
    type: SET_NOTIFICATION,
    payload: notification
  })
}

export const setFeed = (feed) => (dispatch) => {
  dispatch({
    type: SET_FEED,
    payload: feed
  })
}


export const setFeedIsActive = (feedIsActive) => (dispatch) => {
  dispatch({
    type: SET_FEEDISACTIVE,
    payload: feedIsActive
  })
}

export const setNotificationIsActive = (notificationIsActive) => (dispatch) => {
  dispatch({
    type: SET_NOTIFICATIONISACTIVE,
    payload: notificationIsActive
  })
}

// dashboard
export const setTotalTeacherCount = (counts) => (dispatch) => {
  dispatch({
    type: SET_TOTAL_TEACHER_COUNT,
    payload: counts
  })
}

export const setSessionYear = (year) => (dispatch) => {

  dispatch({
    type: SET_SESSION_YEAR,
    payload: year
  })
}


export const setStudentDetailsList = (list) => (dispatch) => {

  dispatch({
    type: SET_STUDENTS_LIST,
    payload: list,
  });
};

export const setStudentDetails = (students_details) => (dispatch) => {
  dispatch({
    type: SET_STUDENT_DETAILS,
    payload: students_details
  })
}

// +++++++++++++++

export const setPosition = (position) => (dispatch) => {
  dispatch({
    type: SET_POSITION,
    payload: position,
  });
};

// login

export const setMobile = (mobile) => (dispatch) => {
  dispatch({
    type: SET_MOBILE,
    payload: mobile
  })
}

export const setShowPass = (showpass) => (dispatch) => {
  dispatch({
    type: SET_SHOW_PASS,
    payload: showpass

  })
}

export const setPass = (password) => (dispatch) => {
  dispatch({
    type: SET_PASS,
    payload: password

  })
}


export const setLoginInfo = (login_info) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_INFO,
    payload: login_info
  })
}



// quiz

export const setQuizSession = (session) => (dispatch) => {
  dispatch({
    type: SET_QUIZ_SESSION,
    payload: session
  })
}

export const setAlotExam = (session) => (dispatch) => {
  dispatch({
    type: SET_ALOT_EXAM,
    payload: session
  })
}

export const setExamInfo = (info) => (dispatch) => {
  dispatch({
    type: SET_QUIZ_INFO,
    payload: info
  })
}

export const setCoupanCode = (coupan) => (dispatch) => {
  dispatch({
    type: SET_COUPAN_CODE,
    payload: coupan
  })
}

export const setIsChecked = (coupan) => (dispatch) => {
  dispatch({
    type: SET_IS_CHECKED,
    payload: coupan
  })
}

export const setQuestionsList = (list) => (dispatch) => {
  dispatch({
    type: SET_QUESTION_LIST,
    payload: list
  })
}

export const setQuestionsIndex = (list) => (dispatch) => {
  dispatch({
    type: SET_QUIZ_ATTEMPT_INDEX,
    payload: list
  })
}


export const setSkipedQuiz = (skiped) => (dispatch) => {
  dispatch({
    type: SET_SKIP_QUIZ,
    payload: skiped
  })
}

export const setAttemptQuiz = (attempt) => (dispatch) => {
  dispatch({
    type: SET_ATTEMPT_QUIZ,
    payload: attempt
  })
}


export const setQuizDetails = (details) => (dispatch) => {

  // options

  let newData = []

  details.options.map((item, index) => {
    if (item.a_id == details.prev_a_id) {
      newData.push({
        a_id: item.a_id,
        a_type: item.a_type,
        answer: item.answer,
        selected_option: true
      })
    } else {
      newData.push({
        a_id: item.a_id,
        a_type: item.a_type,
        answer: item.answer,
        selected_option: false
      })
    }
  })

  dispatch({
    type: SET_QUIZ_DETAILS,
    payload: newData
  })
}

export const setGetQuestion = (details) => (dispatch) => {

  dispatch({
    type: SET_GET_QUESTION,
    payload: details
  })
}

export const setIsSaveAndNext = (details) => (dispatch) => {

  dispatch({
    type: SET_IS_SAVE_AND_NEXT,
    payload: details
  })
}

export const setUpdateQuizOptionList = (index1, previous_id) => (dispatch) => {
  let state = store.getState().indexReducerJaswant;
  let data = state.quiz_details;
  let newData = []

  data.map((item, index) => {
    if (index1 == index) {
      newData.push({
        a_id: item.a_id,
        a_type: item.a_type,
        answer: item.answer,
        selected_option: item.selected_option ? false : true
      })
    } else {
      newData.push({
        a_id: item.a_id,
        a_type: item.a_type,
        answer: item.answer,
        selected_option: false
      })
    }
  })

  dispatch({
    type: SET_QUIZ_DETAILS,
    payload: newData,
  });
};


export const setSelectedAnswer = (save_a_id) => (dispatch) => {
  dispatch({
    type: SET_SAVE_ANSWER_ID,
    payload: save_a_id
  })
}

export const setSelectedQusId = (save_q_id) => (dispatch) => {
  dispatch({
    type: SET_SAVE_QUIZ_ID,
    payload: save_q_id
  })
}

export const setIsClickedOption = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_SELETCT_OPTION,
    payload: isTrue
  })
}

// by akshita

export const setIsSubmit = (isSubmit) => (dispatch) => {
  dispatch({
    type: SET_IS_SUBMIT,
    payload: isSubmit
  })
}

export const setIsLoadingReview = (isLoadingReview) => (dispatch) => {
  dispatch({
    type: SET_IS_LOADING_REVIEW,
    payload: isLoadingReview
  })
}

export const setIsLoadingPrevious = (isLoadingPrevious) => (dispatch) => {
  dispatch({
    type: SET_IS_LOADING_PREVIOUS,
    payload: isLoadingPrevious
  })
}

export const setIsLoadingSaveAndNext = (isLoadingSaveAndNext) => (dispatch) => {
  dispatch({
    type: SET_IS_LOADING_SAVE_AND_NEXT,
    payload: isLoadingSaveAndNext
  })
}

export const setIsLoadingSkip = (isLoadingSkip) => (dispatch) => {
  dispatch({
    type: SET_IS_LOADING_SKIP,
    payload: isLoadingSkip
  })
}

export const setIsLoadingSaveAndSubmit = (isLoadingSaveAndSubmit) => (dispatch) => {
  dispatch({
    type: SET_IS_LOADING_SAVE_AND_SUBMIT,
    payload: isLoadingSaveAndSubmit
  })
}

export const setSubmitLoading = (submitLoading) => (dispatch) => {
  dispatch({
    type: SET_IS_SUBMIT_LOADING,
    payload: submitLoading
  })
}

export const setIsPrevious = (isPrevious) => (dispatch) => {
  dispatch({
    type: SET_IS_PREVIOUS,
    payload: isPrevious
  })
}


export const setProgressInterval = (interval) => (dispatch) => {
  dispatch({
    type: SET_PROGRESS_INTERVAL,
    payload: interval
  })
}

export const setProgress = (progress) => (dispatch) => {
  dispatch({
    type: SET_PROGRESS,
    payload: progress
  })
}


export const setTime = (total_time) => (dispatch) => {
  dispatch({
    type: SET_TIME,
    payload: total_time
  })
}

export const setTotalTime = (time) => (dispatch) => {
  dispatch({
    type: SET_TOTAL_TIME,
    payload: time
  })
}

export const setTotalSeconds = (time) => (dispatch) => {
  dispatch({
    type: SET_TOTAL_SECONDS,
    payload: time
  })
}

export const setUpdateQuizList = (index1, skip_attempt) => (dispatch) => {

  let state = store.getState().indexReducerJaswant;
  let data = state.quiz_list;
  let newData = []

  data.map((item, index) => {
    if (index1 == index) {
      newData.push({
        q_id: item.q_id,
        status: skip_attempt
      })
    } else {
      newData.push({
        q_id: item.q_id,
        status: item.status
      })
    }
  })

  console.log('DATA___', newData);
  dispatch({
    type: SET_QUESTION_LIST,
    payload: newData
  })
}


export const setIsInternet = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_IS_INTERNET_CONNECTED,
    payload: isTrue
  })
}