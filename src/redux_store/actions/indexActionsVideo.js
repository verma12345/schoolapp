import store from "../store";
import { SET_LOADING, SET_REPORT_CARD_MARKS_LIST } from "../types/types";
import { SELECTED_LIST, SET_CHECK_UNCHECK, SET_VIDEO_PLAYER_LIST, SET_VIDEO_URL } from "../types/typeVideo";

//common
export const setLoading = (isTrue) => (dispatch) => {
  dispatch({
    type: SET_LOADING,
    payload: isTrue,
  });
};



export const setVideoPlayerList = (list) => (dispatch) => {
  
  dispatch({
    type: SET_VIDEO_PLAYER_LIST,
    payload: list,
  });
};


export const setVideoUrl= (list) => (dispatch) => {
  
  dispatch({
    type: SET_VIDEO_URL,
    payload: list,
  });
};



















// FOR TESTING ONLY
export const selectAll = (isTrue) => (dispatch) => {
  let newData = []
  let state = store.getState().indexReducerVideo;
  let data = state.isCheckUnCheck;
  data.map((item, index) => {
      newData.push({
        type:item.type,
        is_checked: isTrue
      })
  })
  console.log(newData)
  // return
 
  dispatch({
    type: SET_CHECK_UNCHECK,
    payload: newData,
  });
};


export const setCheckUnCheck = (index1) => (dispatch) => {
  let newData = []
  let state = store.getState().indexReducerVideo;
  let data = state.isCheckUnCheck;
  data.map((item, index) => {
    if (index1 == index) {
      newData.push({
        type:item.type,
        is_checked: !item.is_checked
      })
    } else {
      newData.push({
        type:item.type,
        is_checked: item.is_checked
      })
    }
  })
  console.log(newData)
  // return
 
  dispatch({
    type: SET_CHECK_UNCHECK,
    payload: newData,
  });
};

export const setSelectedList = (list) => (dispatch) => {
  
  dispatch({
    type: SELECTED_LIST,
    payload: list,
  });
};


export const setReportCardMarksList = (list) => (dispatch) => {

  dispatch({
    type: SET_REPORT_CARD_MARKS_LIST,
    payload: list
  })
}
