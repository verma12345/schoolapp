import Constants, { debugLog } from "../../common/Constants";
import { SET_LOADING } from "../types/types";

// JASWANT
// notification
export const hitNotification = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_notification`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


// calander api
export const hitGetAttendanceApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_attendance`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


export const hitGetDashboardApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_dashboard`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=

export const hitLoginApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)
    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/user_login`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    // debugLog(data)
    let response = await data.json();

    return response;
  };
};

export const hitSignUpApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)
    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/email_signup`, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


export const hitGetTimetableApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_timetable`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};

export const hitTeacherListApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_teacher_list`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


export const hitGetStudentListApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_student_list`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};

export const hitAddStudentApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/add_student`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};

export const hitFeeStatusApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_fee_status`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};

export const hitInvoiceApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_invoice`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


export const hitGetFeeReceiptApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_fee_reciept`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


export const hitGetFeeStructureApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_fee_structure`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};








// quiz
export const hitGetQuestionListApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });


    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      session_id: param
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    let data = await fetch(`${Constants.API_BASE_URL}get_question_list`, requestOptions);

    dispatch({
      type: SET_LOADING,
      payload: false,
    });

    let response = await data.json();

    return response;
  };
};





export const hitSaveAnswerApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(param),
      redirect: 'follow'
    };

    // debugLog(param)
    let data = await fetch(`${Constants.API_BASE_URL}save_answer`, requestOptions);

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};

export const hitExamInfoApi = (param) => {
  return async (dispatch) => {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(param),
      redirect: 'follow'
    };

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/exam_info`, requestOptions);
    let response = await data.json();

    return response;
  };
};



export const hitAlotExamApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(param),
      redirect: 'follow'
    };

    let data = await fetch(`${Constants.API_BASE_URL}allot_exam`, requestOptions);

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};



export const hitGetQuestionApi = (param) => {
  return async (dispatch) => {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify(param),
      redirect: 'follow'
    };

    let data = await fetch(`${Constants.API_BASE_URL}get_question`, requestOptions);

    let response = await data.json();

    return response;
  };
};


export const hitReportCardListApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_exam_list`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};


export const hitReportCardMarksApi = (param) => {
  return async (dispatch) => {

    dispatch({
      type: SET_LOADING,
      payload: true,
    });
    // debugLog(param)

    let data = await fetch(`${Constants.API_BASE_URL_PHP}user_controller/get_report_card`, {
      method: 'post',
      body: JSON.stringify(param),
    });

    dispatch({
      type: SET_LOADING,
      payload: false,
    });
    let response = await data.json();

    return response;
  };
};