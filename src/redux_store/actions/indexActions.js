import { SET_LOGIN_EMAIL_MOBILE, SET_LOGIN_PASS, SET_LOGIN_SHOW_PASS, SET_SIGNUP_CONF_PASS, SET_SIGNUP_EMAIL, SET_SIGNUP_MOBILE, SET_SIGNUP_NAME, SET_SIGNUP_PASS, SET_SIGNUP_QUALIFICATION, SET_SIGNUP_SHOW_CONF_PASS, SET_SIGNUP_SHOW_PASS, SET_USER_ID } from "../types/types";

export const setUserId = (user_id) => (dispatch) => {
  dispatch({
    type: SET_USER_ID,
    payload: user_id
  })
}

// login action

export const setLoginEmailMobile = (login_email_mobile) => (dispatch) => {
  dispatch({
    type: SET_LOGIN_EMAIL_MOBILE,
    payload: login_email_mobile
  })
}

export const setLoginPass = (login_pass) => (dispatch) => {
  dispatch({
    type:SET_LOGIN_PASS,
    payload: login_pass
  })
}

// signup action

export const setSignupName = (sign_name) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_NAME ,
    payload: sign_name
  })
}

export const setSignupEmail = (signup_email) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_EMAIL ,
    payload: signup_email
  })
}

export const setSignupMobile = (sign_mobile) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_MOBILE ,
    payload: sign_mobile
  })
}

export const setSignupQualification = (sign_qualification) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_QUALIFICATION ,
    payload: sign_qualification
  })
}

export const setSignupPass = (sign_pass) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_PASS ,
    payload: sign_pass
  })
}

export const setSignupConfPass = (sign_conf_pass) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_CONF_PASS ,
    payload: sign_conf_pass
  })
}

export const setLoginShowPass = (show_pass) => (dispatch) => {
  dispatch({
    type:SET_LOGIN_SHOW_PASS ,
    payload: show_pass
  })
}

export const setSignupShowPass = (show_pass) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_SHOW_PASS ,
    payload: show_pass
  })
}

export const setSignupShowConfPass = (show_conf_pass) => (dispatch) => {
  dispatch({
    type:SET_SIGNUP_SHOW_CONF_PASS ,
    payload: show_conf_pass
  })
}
