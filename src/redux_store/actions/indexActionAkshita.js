import Constants, { debugLog } from "../../common/Constants";
import store from "../store";
import { EMAIL_OTP, EVENT_LIST, GALLERY_LIST, GALLERY_PAGER, RANDOM_NUM_OTP, SEARCH_STUDENT, SET_ADD_STUDENT_LIST, SET_CLASS_TEACHER_DETAILS, SET_EVENT_DETAILS, SET_FEE_LIST, SET_FEE_RECEIPT_LIST, SET_FEE_STRUCTURE_LIST, SET_GALLERY_IMAGE_DETAILS, SET_INVOICE_LIST, SET_QUIZ_RESULT_LIST, SET_RECENT_TEST_LIST, SET_SELECTED_GALLERY_LIST, SET_STUDENTS_LIST, SET_TEACHER_LIST, SET_TIMETABLE_LIST, SMS_OTP } from "../types/typesAkshita";


export const setRecentTestList = (attempt) => (dispatch) => {
  dispatch({
    type: SET_RECENT_TEST_LIST,
    payload: attempt
  })
}

export const setQuizResultList = (result) => (dispatch) => {
  dispatch({
    type: SET_QUIZ_RESULT_LIST,
    payload: result
  })
}



//signup verification
export const setEmailOtp = (otp) => (dispatch) => {
  dispatch({
    type: EMAIL_OTP,
    payload: otp,
  })
}

export const setSmsOtp = (otp) => (dispatch) => {
  dispatch({
    type: SMS_OTP,
    payload: otp,
  })
}

export const setRandomNumOtp = (random_num) => (dispatch) => {
  dispatch({
    type: RANDOM_NUM_OTP,
    payload: random_num
  })
}

//timetable action
export const setTimeTableList = (timetable_list) => (dispatch) => {
  dispatch({
    type: SET_TIMETABLE_LIST,
    payload: timetable_list
  })
}

//teacher action
export const setTeacherDetailsList = (list) => (dispatch) => {
  dispatch({
    type: SET_TEACHER_LIST,
    payload: list
  })
}

export const setClassTeacherDetails = (teachers_details) => (dispatch) => {
  dispatch({
    type: SET_CLASS_TEACHER_DETAILS,
    payload: teachers_details
  })
}


// add student
export const setAddStudentList = (list) => (dispatch) => {
  dispatch({
    type: SET_ADD_STUDENT_LIST,
    payload: list
  })
  dispatch({
    type: SEARCH_STUDENT,
    payload: list,
  });
}


export const setAddUpdateStudentList = (list) => (dispatch) => {
  dispatch({
    type: SET_STUDENTS_LIST,
    payload: list,
  });
};


export const setUpdateAddStudentList = (index1) => (dispatch) => {

  let newData = []
  let state = store.getState().indexReducerAkshita;
  let data = state.addStudentList;

  data.map((item, index) => {
    if (index1 == index) {
      newData.push({
        class: item.class,
        profile_pic: item.profile_pic,
        section: item.section,
        student_id: item.student_id,
        student_name: item.student_name,
        is_selected: item.is_selected ? false : true
      })
    } else {
      newData.push({
        class: item.class,
        profile_pic: item.profile_pic,
        section: item.section,
        student_id: item.student_id,
        student_name: item.student_name,
        is_selected: item.is_selected == undefined ? false : item.is_selected
      })
    }
  })

  dispatch({
    type: SET_ADD_STUDENT_LIST,
    payload: newData
  })
}

export const searchStudent = (text) => (dispatch) => {
  let state = store.getState().indexReducer;
  let txt = text.toUpperCase();
  let holder_list = state.searchStudentList;

  let newData = holder_list.filter(function (item) {
    let student_name = item.student_name.toUpperCase()
    return student_name.includes(txt);
  });

  if (txt == "") {
    dispatch({
      type: SET_ADD_STUDENT_LIST,
      payload: state.searchStudentList,
    });
  } else {
    dispatch({
      type: SET_ADD_STUDENT_LIST,
      payload: newData,
    });
  }
};


// gallery action
export const setGalleryList = (gallery_list) => (dispatch) => {
  dispatch({
    type: GALLERY_LIST,
    payload: gallery_list
  })
}
export const setGalleryPager = (gallery_pager) => (dispatch) => {
  dispatch({
    type: GALLERY_PAGER,
    payload: gallery_pager
  })
}
export const setSelectedGalleryList = (list) => (dispatch) => {
  debugLog(list)
  dispatch({
    type: SET_SELECTED_GALLERY_LIST,
    payload: list
  })
}
export const setGalleryImageDetails = (details) => (dispatch) => {
  dispatch({
    type: SET_GALLERY_IMAGE_DETAILS,
    payload: details
  })
}

// events
export const setUpcomingEventList = (upcomingEvent) => (dispatch) => {
  dispatch({
    type: EVENT_LIST,
    payload: upcomingEvent
  })
}

export const setEventDetails = (event_deatils) => (dispatch) => {
  dispatch({
    type: SET_EVENT_DETAILS,
    payload: event_deatils
  })
}

// Fee
export const setFeeDetailList = (list) => (dispatch) => {
  dispatch({
    type: SET_FEE_LIST,
    payload: list
  })
}

export const setInvoiceDetails = (list) => (dispatch) => {
  dispatch({
    type: SET_INVOICE_LIST,
    payload: list
  })
}

export const setFeeReceiptDetailList = (list) => (dispatch) => {
  dispatch({
    type: SET_FEE_RECEIPT_LIST,
    payload: list
  })
}

export const setFeeStructureList = (list) => (dispatch) => {
  dispatch({
    type: SET_FEE_STRUCTURE_LIST,
    payload: list
  })
}