import React, { Component } from "react";
import { Image, ScrollView, StyleSheet, Text, View, SafeAreaView, TouchableOpacity } from "react-native";
import TextInput from 'react-native-material-textinput'
import Font from "../common/Font";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import { setSignupConfPass, setSignupPass, setSignupShowConfPass, setSignupShowPass } from "../redux_store/actions/indexActions";
import { LoginHeader } from "../components/LoginHeader";

class ChangePasswordScreen extends Component {
    constructor(props) {
        super(props)
    }



    _showpass = () => {
        this.props.setShowPass(!this.props.showpass)

    }

    _showcnpass = () => {
        this.props.setShowCnPass(!this.props.showcnpass)

    }
    
    _validate = () => {
        if (this.props.signup_pass == '' || this.props.signup_conf_pass == '') {
            alert('Enter the Password')
            return
        }
        if (this.props.signup_pass.length > 5 && this.props.signup_pass == this.props.signup_conf_pass) {
            this.props.navigation.navigate('LoginScreen')
            return
        } else {
            alert('Please enter same password')
            return
        }
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <ScrollView style={styles.container}>
                        <View style={{ paddingHorizontal: 40, marginBottom: 50 }}>
                            <LoginHeader
                                navigation={this.props.navigation}
                                backpic={require('../../assets/back.png')}
                                txtheading={'Change\nPassword'}
                                txt={"Sign in with your data that you have\n entered during your registration"}
                            />
                            <View style={[styles.view2]}>

                                <TextInput
                                    width={150}
                                    label='New Password'
                                    value={this.props.password}
                                    underlineHeight={1}
                                    underlineColor={Colors.backgroundColor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(password) => {
                                        this.props.setSignupPass(password)
                                    }}
                                    showpass={this.props.isSignupShowPass}
                                    secureTextEntry={this.props.isSignupShowPass}
                                    maxLength={15}
                                />
                                <TouchableOpacity onPress={() => this.props.setSignupShowPass(!this.props.isSignupShowPass)}
                                    style={{ height: 56, width: 56, justifyContent: 'center', alignItems: 'flex-end' }}

                                >
                                    {/* {
                                        this.props.signup_pass.length > 5 && this.props.signup_pass == this.props.signup_conf_pass ?

                                            < Image
                                                style={{ height: 24, width: 24, }}
                                                source={require('../../assets/tick.png')}
                                            /> */}

                                    {/* : */}
                                    <Image
                                        style={{ height: 20, width: 20, tintColor: 'gray' }}
                                        source={this.props.isSignupShowPass ? require('../../assets/show.png') : require('../../assets/passhide.png')}
                                    />
                                    {/* } */}

                                </TouchableOpacity>
                            </View>

                            <View style={styles.view3}>

                                <TextInput
                                    width={150}
                                    label='Re-enter New Password'
                                    underlineHeight={1}
                                    underlineColor={Colors.textinputcolor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(cnpassword) => {
                                        this.props.setSignupConfPass(cnpassword)
                                    }}
                                    showpass={this.props.isSignupConfPass}
                                    secureTextEntry={this.props.isSignupConfPass}
                                    maxLength={15}
                                />

                                <TouchableOpacity onPress={() => this.props.setSignupShowConfPass(!this.props.isSignupConfPass)}
                                    style={{ height: 56, width: 56, justifyContent: 'center', alignItems: 'flex-end' }}
                                >
                                    <Image
                                        style={{ height: 20, width: 20, tintColor: 'gray' }}
                                        source={this.props.isSignupConfPass ? require('../../assets/show.png') : require('../../assets/passhide.png')}
                                    />

                                </TouchableOpacity>
                            </View>
                            {
                                this.props.showpasstick !== '' ?
                                    <TouchableOpacity style={styles.btn1}
                                        onPress={() => this._validate()}
                                    >
                                        <Text style={[myStyle.textBold, { color: Colors.backgroundColor, fontSize: 14 }]}>Login</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={styles.btn}
                                        onPress={() => this._validate()}
                                    >
                                        <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 14 }]}>Login</Text>
                                    </TouchableOpacity>


                            }


                        </View>


                    </ScrollView>
                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    btn: {
        height: 56,
        width: '100%',
        backgroundColor: Colors.btncolor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 30
    },
    btn1: {
        height: 56,
        width: '100%',
        backgroundColor: Colors.blue,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 30
    },
    // 
    view2: {
        height: 56,
        width: '100%',
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,

        marginTop: 60,
        flexDirection: 'row',
        paddingHorizontal: 40,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    view3: {
        height: 60,
        width: '100%',
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,

        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 40,

    },
})


const mapStateToProps = (state) => {
    let common = state.indexReducer;
    return {
        signup_pass: common.signup_pass,
        signup_conf_pass: common.signup_conf_pass,

        isSignupShowPass: common.isSignupShowPass,
        isSignupConfPass: common.isSignupConfPass
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setSignupPass: (signup_pass) => setSignupPass(signup_pass),
        setSignupShowPass: (show_pass) => setSignupShowPass(show_pass),

        setSignupConfPass: (signup_conf_pass) => setSignupConfPass(signup_conf_pass),
        setSignupShowConfPass: (show_conf_pass) => setSignupShowConfPass(show_conf_pass),

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordScreen)