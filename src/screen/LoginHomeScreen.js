import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Colors from "../common/Colors";
// import { Colors } from "react-native/Libraries/NewAppScreen";
import Font from "../common/Font";
import { myStyle } from "../common/myStyle";


export default class LoginHomeScreen extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container} >
                    <ScrollView
                        style={{ width: "100%" }}>
                        <ImageBackground style={{ width: "100%", alignItems: 'center' }}
                            source={require('../../assets/background.png')}
                        >
                            <Image
                                style={{ resizeMode: 'contain', height: Dimensions.get('window').height / 2, width: Dimensions.get('window').width / 1.2 }}
                                source={require('../../assets/Saly.png')}
                            />

                        </ImageBackground>
                        <View style={styles.view2}>
                            <View style={styles.view3}>
                                <Text style={[myStyle.textBold, { fontSize: 28, fontFamily: Font.Roboto }]}>{'School'}</Text>
                                <Text style={{ fontSize: 28, paddingLeft: 10, fontFamily: Font.Roboto }}>{'App'}</Text>

                            </View>

                            <View style={styles.view4}>
                                <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 14 }]}>
                                    {" Lorem ipsum dolor sit amet, consectet piscing\n elit. Nam felis lectus,efficitur at erat vitae,\n tempor mattis quam."}
                                </Text>
                            </View>

                            <View style={{ marginTop: 40 }}>
                                <TouchableOpacity style={styles.btnlogin}

                                    onPress={() => this.props.navigation.navigate('LoginScreen')}
                                >
                                    <Text style={[myStyle.textBold, { color: Colors.whiteText }]}>{"Login"}</Text>
                                </TouchableOpacity>

                                
                                <TouchableOpacity style={styles.btnsignup}
                                    onPress={() => this.props.navigation.navigate('SignupScreen')}
                                    // onPress={() => this.props.navigation.navigate('ELearningSyllabus')}

                                >
                                    <Text style={[myStyle.textBold, { fontWeight: 'bold', color: Colors.blue }]}>{'Signup'}</Text>
                                </TouchableOpacity>
                            </View>


                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#9fc7ec'

    },
    view1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'#1582EA',
    },
    view2: {
        flex: 1.5,
        backgroundColor: Colors.backgroundColor,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        paddingHorizontal: 40,
        paddingBottom: 100

    },
    view3: {
        flexDirection: 'row',
        marginTop: 40,

    },
    view4: {
        width: '100%',
        marginTop: 25
    },
    btnlogin: {
        backgroundColor: Colors.blue,


        width: '100%',
        height: 50,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        // marginTop: 50
    },
    btnsignup: {
        width: '100%',
        height: 50,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        borderWidth: 1,
        borderColor: Colors.blue,

    }
})