import React, { Component } from "react";
import { Dimensions, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import Header from "../components/Header";
const { width, height } = Dimensions.get('window')


class ShowImageScreen extends Component {
  constructor(props) {
    super(props);

  }


  onPressBack = () => {
    this.props.navigation.goBack()
  }

  render() {
    // debugLog(this.props.galleryImageDetails.image_description)
    return (
      <SafeAreaView style={myStyle.container}>
        <View style={myStyle.container}>

          <Header
            onPressBack={() => this.onPressBack()}
            // title={this.props.set_image.title}
            title={this.props.selectedGalleryList.category}

            style={{
              elevation: 2,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              backgroundColor: Colors.whiteText,
              paddingVertical: 22,
              paddingHorizontal: 15,
            }}
          />
          {/* 
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

            <ImageZoom cropWidth={width}
              cropHeight={height}
              imageWidth={width}
              imageHeight={height}>
              <Image style={{ width: width, height: height, resizeMode: 'contain' }}
                source={this.props.galleryImageDetails.image}
              />
            </ImageZoom>

          </View> */}

          <View style={{ flex: 1, marginTop: 80 }}>
            <TouchableOpacity>
              <Image style={{ width: width, height: width + 35 }}
                source={this.props.galleryImageDetails.image}
              />
            </TouchableOpacity>

            <View style={{ marginLeft: 20, marginTop: 5 }}>
              <TouchableOpacity style={{ marginLeft: 2 }}>
                <Image
                  style={{ height: 20, width: 20, tintColor: Colors.textColor }}
                  source={require('../../assets/heart.png')}
                />
              </TouchableOpacity>
              <Text style={[myStyle.textBold, { fontSize: 15, marginTop: 10, marginLeft: 2, }]}>{this.props.galleryImageDetails.image_heading} </Text>
              <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}> {this.props.galleryImageDetails.image_description} </Text>
            </View>
          </View>


        </View>
      </SafeAreaView>

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor
  },
});


const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const akshita = state.indexReducerAkshita;


  return {
    galleryImageDetails: akshita.galleryImageDetails,
    selectedGalleryList: common.selectedGalleryList
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(

    {
      // setGalleryImage: (set_image) => setGalleryImage(set_image)
    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ShowImageScreen);
