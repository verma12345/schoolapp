import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import Header from "../components/Header";
import { setGalleryImageDetails, setGalleryList, setSelectedGalleryList } from "../redux_store/actions/indexActionAkshita";

class SelectedGalleryList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      category_name: ''
    }
  }


// header go back navigation
  onPressBack = () => {
    this.props.navigation.goBack()
  }
  

  // getting full image on navigation
  onClickImage = (data) => {
    this.props.setGalleryImageDetails(data.item)
    this.props.navigation.navigate('ShowImageScreen')
  }


  _renderItem = (data) => {

    // debugLog(data.item.images)
    return (
      <View style={{ flex: 1 }}>
        <Text>
          {data.item.category}
        </Text>
        <TouchableOpacity onPress={() => this.onClickImage(data)}>
          <ImageBackground
            style={{ resizeMode: 'contain', height: 300, width: '100%' }}
            // style={{ resizeMode:'contain',flex:1 }}
            source={data.item.image}
          >

            <TouchableOpacity style={{
              position: 'absolute',
              right: 40,
              top: 20
            }}>
              <Image
                style={{ height: 25, width: 25, }}
                source={require('../../assets/heart.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity style={{
              position: 'absolute',
              right: 40,
              bottom: 40
            }}>
              <Image
                style={{ height: 25, width: 25, }}
                source={require('../../assets/download.png')}
              />
            </TouchableOpacity>

          </ImageBackground>
        </TouchableOpacity>
      </View>
    )
  }

  render() {

    return (

      <SafeAreaView style={myStyle.container}>
        <View style={myStyle.container}>

          <Header
            onPressBack={() => this.onPressBack()}
            title={this.props.selectedGalleryList.category}
            style={{
              elevation: 2,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              backgroundColor: Colors.whiteText,
              paddingVertical: 32,
              paddingHorizontal: 15,
            }}
          />

          <FlatList
            data={this.props.selectedGalleryList.images}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => 'key' + index}
            showsVerticalScrollIndicator={false}
          />

        </View>
      </SafeAreaView>

    )
  }
}
const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor
  },
});


const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const akshita = state.indexReducerAkshita;


  return {
    gallery_list: akshita.gallery_list,
    selectedGalleryList: common.selectedGalleryList
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setGalleryList: (gallery_list) => setGalleryList(gallery_list),
      setSelectedGalleryList: (list) => setSelectedGalleryList(list),

      setGalleryImageDetails: (set_image) => setGalleryImageDetails(set_image)

    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(SelectedGalleryList);
