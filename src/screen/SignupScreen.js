import { StackActions } from '@react-navigation/routers';
import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Image, SafeAreaView, StatusBar, Platform } from 'react-native';
import TextInput from 'react-native-material-textinput'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { myStyle } from '../common/myStyle';
import { setPrefs } from '../common/Prefs';
import { SignupHeader } from '../components/SignupHeader';
import { setEmailOtp, setSmsOtp } from '../redux_store/actions/indexActionAkshita';
import { hitSignUpApi } from '../redux_store/actions/indexActionApi';
import { setSignupConfPass, setSignupEmail, setSignupMobile, setSignupName, setSignupPass, setSignupShowConfPass, setSignupShowPass, setUserId } from '../redux_store/actions/indexActions';

class SignupScreen extends Component {
    constructor(props) {
        super(props)
    }

    // sign up api

    onClickSignUp = () => {
        if (this.props.signup_name == '' || this.props.signup_email == '' || this.props.signup_pass == '') {
            alert('Please enter name , email and password');
            return;
        }

        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const pass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{5,})/;

        if (this.props.signup_email === '') {
            alert('Enter Email')
        }

        if (reg.test(this.props.signup_email) === false) {
            alert('Please Type a valid Email')
            return
        }

        if (this.props.signup_pass == '') {
            alert('Enter the Password')
            return
        }

        if (this.props.signup_pass.length < 6) {
            alert('Password must be greater than 6 digit ')
            return
        }

        if (this.props.signup_conf_pass == '') {
            alert('Enter Confirm Password')
            return
        }

        if (this.props.signup_pass !== this.props.signup_conf_pass) {
            alert('Confirm Password does not matched ')
            return
        }
        if (this.props.signup_mobile.length < 5) {
            alert('Please enter valid mobile ');
            return;
        }
        //  this._setUserInfo(_response)
        //  this._onPressGetOtp()

        // local storage ,used in email verification
        // setPrefs('email', this.props.signup_email)

        //  setPrefs('user_signedup', '1')
        //  setPrefs('name', this.props.name)

        //local storage used in mobile verification
        // setPrefs('mobile', this.props.signup_mobile)

        //  setPrefs('password', this.props.password)

        let param = {
            school: {
                name: this.props.signup_name,
                email: this.props.signup_email,
                password: this.props.signup_pass,
                phone: this.props.signup_mobile,
                device_os: Platform.OS == "android" ? 'Android' : 'iOS',
                device_token: "7A0E4-517dsD-9D908-1EB3C9B8E749",
                email_otp: this.props.email_otp,
                sms_otp: this.props.sms_otp
            }
        };

        this.props.hitSignUpApi(param).then((response) => {
            debugLog(param)
            let _response = response.school;
            if (_response.status == 1) {
                debugLog('signup details')
                setPrefs('user_signup', '1')
                setPrefs('user_login', '1')
                setPrefs('mobile', this.props.signup_mobile)
                setPrefs('email', this.props.signup_email)

                debugLog(_response)
                // this._setUserInfo(_response)
                this.props.setUserId(_response.user_detail.student_id)
                this.props.navigation.dispatch(
                    StackActions.replace('MobileVerficationScreen'),
                )
            } else {
                alert(_response.msg);
            }
        });
    };

    // generate random no for sms and email otp
    componentDidMount() {
        var email_otp = Math.floor(10000 + Math.random() * 9000);
        this.props.setEmailOtp(email_otp)

        var sms_otp = Math.floor(10000 + Math.random() * 9000);
        this.props.setSmsOtp(sms_otp)

        setPrefs('email_otp', JSON.stringify(email_otp))
        setPrefs('sms_otp', JSON.stringify(sms_otp))

    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: Colors.backgroundColor, }}>
                <StatusBar
                    hidden={false} />
                <View>
                    <ScrollView style={{ paddingHorizontal: 40, marginBottom: 50 }}>
                        <SignupHeader
                            navigation={this.props.navigation}
                            backpic={require('../../assets/back.png')}
                            txtheading={'Create Account'}
                            txt={'Sign in with your data that you have\n entered during your registration'}
                        />

                        <View>
                            <View style={styles.view2}>
                                <TextInput

                                    label='Full Name'
                                    underlineHeight={1}
                                    value={this.props.signup_name}
                                    underlineColor={Colors.backgroundColor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(txtName) => {
                                        this.props.setSignupName(txtName)
                                        // this.props.setShow(txtName)
                                    }}
                                />
                            </View>
                            <View style={styles.view5}>
                                <TextInput
                                    width={200}
                                    label='Email Address'
                                    value={this.props.signup_email}
                                    underlineHeight={1}
                                    underlineColor={Colors.backgroundColor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(email) => {
                                        this.props.setSignupEmail(email)
                                    }}
                                    keyboardType='email-address'
                                />
                            </View>

                            <View style={styles.view5}>
                                <TextInput
                                    width={200}
                                    label='Mobile'
                                    maxLength={10}
                                    value={this.props.signup_mobile}
                                    underlineHeight={1}
                                    underlineColor={Colors.backgroundColor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(mobile) => {
                                        this.props.setSignupMobile(mobile)
                                    }}
                                    keyboardType='email-address'
                                />
                            </View>
                            <View style={styles.view5}>

                                <TextInput
                                    width={150}
                                    label='Password'
                                    value={this.props.signup_pass}
                                    underlineHeight={1}
                                    underlineColor={Colors.backgroundColor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(password) => {
                                        this.props.setSignupPass(password)
                                    }}
                                    secureTextEntry={this.props.isSignupShowPass}
                                    maxLength={15}
                                />
                                <TouchableOpacity onPress={() => this.props.setSignupShowPass(!this.props.isSignupShowPass)}
                                    style={{ height: 56, width: 56, justifyContent: 'center', alignItems: 'flex-end' }}>

                                    {this.props.signup_pass.length > 5 && this.props.signup_pass == this.props.signup_conf_pass ?
                                        < Image
                                            style={{ height: 24, width: 24, }}
                                            source={require('../../assets/tick.png')}
                                        />
                                        :
                                        <Image
                                            style={{ height: 20, width: 20, tintColor: 'gray' }}
                                            source={this.props.isSignupShowPass ? require('../../assets/show.png') : require('../../assets/passhide.png')}
                                        />
                                    }
                                </TouchableOpacity>

                            </View>

                            <View style={styles.view5}>
                                <TextInput
                                    width={150}
                                    label='Confirm Password'
                                    value={this.props.signup_conf_pass}
                                    underlineHeight={1}
                                    underlineColor={Colors.backgroundColor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    onChangeText={(cnpassword) => {
                                        this.props.setSignupConfPass(cnpassword)
                                    }}
                                    showpass={this.props.isSignupConfPass}
                                    secureTextEntry={this.props.isSignupConfPass}
                                    maxLength={15}
                                />

                                <TouchableOpacity onPress={() => this.props.setSignupShowConfPass(!this.props.isSignupConfPass)}
                                    style={{ height: 56, width: 56, justifyContent: 'center', alignItems: 'flex-end' }}>
                                    <Image
                                        style={{ height: 20, width: 20, tintColor: 'gray' }}
                                        source={this.props.isSignupConfPass ? require('../../assets/show.png') : require('../../assets/passhide.png')}
                                    />

                                </TouchableOpacity>
                            </View>

                            {this.props.show !== '' ?
                                <View style={{ marginTop: 30 }}>
                                    <TouchableOpacity style={styles.btn}
                                        onPress={() => { this.onClickSignUp() }}>
                                        <Text style={[myStyle.textBold, { color: Colors.whiteText, fontSize: 14 }]}>{'Signup'}</Text>
                                    </TouchableOpacity>
                                </View>
                                :
                                <View style={{ marginTop: 40 }}>
                                    <TouchableOpacity style={styles.btn1}>
                                        <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 14 }]}>{'Sign up'}</Text>
                                    </TouchableOpacity>
                                </View>
                            }

                        </View>

                        <TouchableOpacity style={styles.view3}>
                            <Image style={{ height: 20, width: 20 }}
                                source={require('../../assets/google1.png')}
                            />
                            <Text style={[myStyle.textNormal, { color: Colors.googletextcolor, paddingLeft: 20, fontSize: 12 }]}>{'Continue with Google'}</Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 50 }}>
                            <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]}>{"Don't have an account?"}</Text>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('LoginScreen')}>
                                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 14 }]}>{' Login'}</Text>
                            </TouchableOpacity>
                        </View>



                    </ScrollView>
                </View>
            </SafeAreaView>


        )
    }
}

const styles = StyleSheet.create({
    view2: {
        height: 56,
        width: '100%',
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,
        marginTop: 60,
        paddingLeft: 40,
        elevation: 1
    },
    view5: {
        height: 56,
        width: '100%',
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,
        marginTop: 15,
        paddingHorizontal: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        elevation: 1

    },
    btn: {
        height: 56,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,

        borderRadius: 20,
    },
    btn1: {
        height: 56,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.btncolor,
        borderRadius: 20,
    },
    view3: {
        height: 56,
        width: '100%',
        backgroundColor: Colors.backgroundColor,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.1,
        marginTop: 25,
        borderRadius: 30
    },
})

const mapStateToProps = (state) => {
    let akshita = state.indexReducerAkshita;
    let jaswant = state.indexReducerJaswant;
    let common = state.indexReducer;

    return {
        user_id: common.user_id,
        signup_name: common.signup_name,
        signup_email: common.signup_email,
        signup_mobile: common.signup_mobile,
        signup_pass: common.signup_pass,
        signup_conf_pass: common.signup_conf_pass,
        isSignupShowPass: common.isSignupShowPass,
        isSignupConfPass: common.isSignupConfPass,
        email_otp: akshita.email_otp,
        sms_otp: akshita.sms_otp,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

        setUserId: (user_id) => setUserId(user_id),
        setSignupName: (name) => setSignupName(name),
        setSignupEmail: (email) => setSignupEmail(email),
        setSignupMobile: (mobile) => setSignupMobile(mobile),
        setSignupPass: (password) => setSignupPass(password),
        setSignupConfPass: (cnpassword) => setSignupConfPass(cnpassword),
        setSignupShowPass: (showpass) => setSignupShowPass(showpass),
        setSignupShowConfPass: (showcnpass) => setSignupShowConfPass(showcnpass),

        hitSignUpApi: (param) => hitSignUpApi(param),
        setEmailOtp: (param) => setEmailOtp(param),
        setSmsOtp: (param) => setSmsOtp(param),

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen)