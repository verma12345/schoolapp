import React, { Component } from "react";
import { FlatList, Image, PixelRatio, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { myStyle } from "../common/myStyle";
import Header from "../components/Header";
import Colors from "../common/Colors";
import { setELearningList, setELearningSyllabusList } from "../redux_store/actions/indexActionsJaswant";
import ELearningList_Row from "../row_component/ELearningList_Row";

class ELearningScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subject_teacher_pic: null,
            subject_teacher_name: null,
            subject_name: null
        }
    }




    onPressBack = () => {
        this.props.navigation.goBack()
    }

    onPressElearningSubject = (dataRow) => {
        // console.log(dataRow);
        // return
        this.props.setELearningSyllabusList(dataRow)
        this.props.navigation.navigate('ELearningSyllabus')
    }

    _renderItem = (dataItem) => {
        return (
            <ELearningList_Row
                dataRow={dataItem.item}
                onPress={this.onPressElearningSubject}

            />
        )
    }


    render() {
        return (
            <SafeAreaView style={myStyle.container}>
                <View style={myStyle.container} >
                    <Header onPressBack={this.onPressBack}
                        title="E-Learning"
                        style={{ height: 70, backgroundColor: Colors.whiteText, borderBottomRightRadius: 20, borderBottomLeftRadius: 20 }}
                    />

                    <View style={{ flex: 1, marginTop: 8, paddingBottom: 15 }}>
                        <FlatList
                            data={this.props.eLearningList}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => 'key' + index}
                        />
                    </View>
                </View>

            </SafeAreaView >

        )
    }
}


const styles = StyleSheet.create({



});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jas = state.indexReducerJaswant;


    return {
        eLearningList: jas.eLearningList,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setELearningList: (list) => setELearningList(list),
            setELearningSyllabusList: (list) => setELearningSyllabusList(list)
        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ELearningScreen);