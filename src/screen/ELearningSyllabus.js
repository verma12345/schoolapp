import React, { Component } from "react";
import { FlatList, SafeAreaView, StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import ExpertPopUp from "../components/ExpertPopUp";
import Header from "../components/Header";
import SyllabusPopup from "../components/SyllabusPopup";
import ELearningSyllabusList_Row from "../row_component/ELearningSyllabusList_Row";

class ELearningSyllabus extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isPopUp: false,
            isComingSoon: false

        }
    }

    setIsPopUp = () => {
        this.setState({ isPopUp: !this.state.isPopUp })
    }

    setIsComingSoon = () => {
        this.setState({
            isComingSoon: !this.state.isComingSoon,
            isPopUp: !this.state.isPopUp
        })
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }
    _renderItem = (dataItem) => {
        return (
            <ELearningSyllabusList_Row
                dataRow={dataItem.item}
                onPress={() => this.setIsPopUp()}

            />
        )
    }

    onPressQuiz = (dataRow) => {
        // this.props.setELearningQuizDesc(dataRow)
        this.props.navigation.navigate('AboutQuizScreen')
    }

    onPressWatchVideo = () => {
        // this.props.setELearningQuizDesc(dataRow)
        this.props.navigation.navigate('VideoListScreen')
    }


    render() {

        return (

            <SafeAreaView style={myStyle.container} >


                <View style={myStyle.container}>

                    <Header onPressBack={this.onPressBack}
                        title={this.props.eLearningSyllabusList.subject_name + ' Syllabus'}
                        style={{ height: 70, backgroundColor: Colors.whiteText, borderBottomRightRadius: 20, borderBottomLeftRadius: 20 }}
                    />


                    <View style={{ flex: 1, marginTop: 8, }}>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: 50 }}
                            data={this.props.eLearningSyllabusList.syllabus}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => 'key' + index}

                        />
                    </View>


                    {this.state.isPopUp ?
                        <SyllabusPopup
                            onPressCancel={() => this.setIsPopUp(this.state.isPopUp)}
                            onPressWatchvideo={this.onPressWatchVideo}
                            onPressQuiz={this.onPressQuiz}
                            onPressHelp={() => this.setIsComingSoon()}

                        />
                        : null
                    }


                    {this.state.isComingSoon ?
                        <ExpertPopUp
                            onPress={() => this.setIsComingSoon(this.state.isComingSoon)}
                        />
                        : null
                    }
                </View>






            </SafeAreaView>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    // i
});

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    const jaswant = state.indexReducerJaswant;

    return {
        eLearningSyllabusList: jaswant.eLearningSyllabusList
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

        // setELearningQuizDesc: (list) => setELearningQuizDesc(list)
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ELearningSyllabus)
