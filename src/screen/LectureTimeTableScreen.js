import React, { Component } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, SafeAreaView, FlatList, ScrollView, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import Font from "../common/Font";
import { myStyle } from '../common/myStyle';
import WeaklyCalenderComponent from '../components/WeaklyCalenderComponent';
import { setTimeTableList } from '../redux_store/actions/indexActionAkshita';
import { hitGetTimetableApi } from '../redux_store/actions/indexActionApi';
import TimeTableRow from '../row_component/TimeTableRow';



class LectureTimeTableScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { weekIndex: 0 }
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }

    componentDidMount() {
        let params = {
            "school": {
                "class": 1
            }
        }

        this.props.hitGetTimetableApi(params).then(res => {
            debugLog('timetable', res.school)

            // if (res.school.status == 1) {
                this.props.setTimeTableList(res.school)
            // }
        })
    }

    // to display timetable list
    _renderItem = (dataItem) => {
        return (
            <TimeTableRow
                data_row={dataItem.item}
            />
        )
    }

    // for timetable heading
    renderHeader = (data) => {
        return (
            <View style={{ paddingLeft: 20, marginVertical: 12, marginLeft: 5 }}>
                <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.textColor }]}>{'Friday, 10 May 2021'}</Text>
            </View>
        )
    }

    // to get the day index
    onSetIndex = (index) => {
        this.setState({ weekIndex: index })
    }

    render() {
        // debugLog(this.props.days_list[0].timetable)
        return (
            <SafeAreaView style={styles.container} >

                <View style={styles.container} >

                    <WeaklyCalenderComponent
                        navigation={this.props.navigation}
                        setIndex={this.onSetIndex}
                        index={this.state.weekIndex}
                    />

                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={this.props.days_list.length > 0 ? this.props.days_list[this.state.weekIndex].timetable : this.props.days_list}
                        renderItem={this._renderItem}
                        ListHeaderComponent={this.renderHeader}
                        showsVerticalScrollIndicator={false}
                    />

                </View>

                
                {this.props.is_loading ? (
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#0008',
                            elevation: 10,
                            position: 'absolute',
                        }}>
                        <View style={{ borderRadius: 10, backgroundColor: "white", height: 200, width: 300, justifyContent: "center", alignItems: "center", }}>
                            <ActivityIndicator
                                size={"large"}
                                color={Colors.blue}
                            />
                            <Text style={{ fontSize: 14 }} >{"Please wait for a while..."} </Text>
                        </View>
                    </View>
                ) : null}

            </SafeAreaView>
        )
    }



}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.backgroundColor,
        flex: 1
    },

})


const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let akshita = state.indexReducerAkshita;

    return {
        days_list: akshita.days_list,
        is_loading : common.is_loading

    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setTimeTableList: (timetable_list) => setTimeTableList(timetable_list),
        hitGetTimetableApi: (params) => hitGetTimetableApi(params)
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LectureTimeTableScreen)