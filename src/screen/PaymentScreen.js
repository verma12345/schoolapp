import React, { Component } from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View, TextInput, Alert, SafeAreaView, ScrollView, ActivityIndicator, FlatList } from "react-native";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { myStyle } from "../common/myStyle";
import { debugLog } from "../common/Constants";
import Colors from "../common/Colors";
import RazorpayCheckout from 'react-native-razorpay';
import Header from "../components/Header";

class PaymentScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            otp: '',
        }
    }

// to display fee table content
    renderItem = (dataItem) => {
        return (
            <View style={{
                flexDirection: 'row', paddingLeft: 20, flex: 1, justifyContent: 'center',
                backgroundColor: Colors.whiteText
            }}>

                <View style={{ borderColor: 'lightgray', flex: 4, paddingTop: 8, justifyContent: 'center', }}>
                    <Text style={[myStyle.textNormal, { fontSize: 13, }]}>
                        {dataItem.item.fee_type}
                    </Text>
                </View>
                <View style={{ borderColor: 'lightgray', flex: 1, justifyContent: 'center', }}>
                    <Text style={[myStyle.textNormal, { fontWeight: '700', fontSize: 13 }]}>
                        {dataItem.item.fee}
                    </Text>
                </View>
            </View>
        )
    }


    // to dislay fee table heading
    listHeaderComponent = () => {
        return (
            <View style={{
                flexDirection: 'row', flex: 1, justifyContent: 'center',
                paddingLeft: 20,
                paddingBottom: 5,
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20,
                backgroundColor: Colors.whiteText
            }}>
                <View style={{ flex: 4, justifyContent: 'center', backgroundColor: Colors.whiteText }}>
                    <Text style={[myStyle.textBold, { fontSize: 14, }]}>
                        {'Particulars'}
                    </Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: Colors.whiteText, }}>
                    <Text style={[myStyle.textBold, { fontSize: 14, }]}>
                        {'Amount'}
                    </Text>
                </View>
            </View>
        )
    }

// to display fee table bottom component
    listFooterComponent = () => {
        return (
            <View
                style={{
                    flex: 1,
                    marginTop: 25
                }}>

                <View style={styles.rowOption}>

                    <View style={{ flex: 4, justifyContent: 'center' }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Pending Fee'} </Text>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Text style={[myStyle.textNormal, { fontSize: 14, fontWeight: '700' }]}>   {'₹ 3,500'} </Text>
                    </View>
                </View>

                <View style={styles.rowOption}>

                    <View style={{ flex: 4, justifyContent: 'center' }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Sub Total'} </Text>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Text style={[myStyle.textNormal, { fontSize: 14, fontWeight: '700' }]}>   {'₹ 3,500'} </Text>
                    </View>
                </View>


                <View style={styles.rowOption}>
                    <View style={{ flex: 4, justifyContent: 'center' }}>
                        <Text style={[myStyle.textBold, { fontSize: 14, fontWeight: '700' }]}>   {'Discount'} </Text>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Text style={[myStyle.textNormal, { fontSize: 14, color: '#6BB893', fontWeight: '700' }]}>   {'10%'} </Text>
                    </View>
                </View>

                <View style={[styles.rowOption, { borderBottomRightRadius: 20, borderBottomLeftRadius: 20 }]}>
                    <View style={{ flex: 4, justifyContent: 'center' }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Grand Total'} </Text>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <Text style={[myStyle.textNormal, { fontSize: 16, fontWeight: '700' }]}>   {'₹ 5,508'} </Text>
                    </View>
                </View>
            </View>

        )
    }

    // on pay btn
    onPressPay = () => {
        var options = {
            description: 'Chandigarh University',
            image: 'https://nextgenvidya.com/wp-content/uploads/2020/07/Chandigarh-University-Logo-2.png',
            currency: 'INR',
            key: 'rzp_test_X78KOG1QGwtRaI', // Your api key
            amount: '5000',
            name: 'Total Fee',
            prefill: {
                email: 'jaswant.raj45@gmail.com',
                contact: '8052004200',
                name: 'School App'
            },
            theme: { color: Colors.blue }
        }
        RazorpayCheckout.open(options).then((data) => {
            // handle success
            debugLog(`Success: ${data}`);
        }).catch((error) => {
            // handle failure
            alert(`Error: ${error.code} | ${error.description}`);
        });
    }

    onBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        debugLog(this.props.payment_record)
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.container}>
                    {/* header */}
                    {/* <View style={{ backgroundColor: Colors.whiteText, elevation: 1, paddingBottom: 10, borderBottomRightRadius: 30, borderBottomLeftRadius: 30 }}>
                        <Header
                            title="Due Fee - 2021"
                            onPressBack={() => this.onBack()}
                            style={{ backgroundColor: Colors.whiteText, paddingVertical:3 }}
                        />

                    </View> */}

                    <View style={{
                        borderBottomRightRadius: 20,
                        borderBottomLeftRadius: 20,
                        backgroundColor: 'white',
                        elevation: 3,
                        paddingVertical: 12,
                        paddingBottom: 20,
                        paddintTop: 12
                    }}>
                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <TouchableOpacity
                                onPress={() => this.onBack()}
                                style={{ position: 'absolute', left: 0, padding: 15 }}
                            >
                                <Image
                                    source={require('../../assets/back.png')}
                                    style={{ height: 17, width: 17, resizeMode: 'contain' }}
                                />
                            </TouchableOpacity>
                            <Text style={[myStyle.textBold, { fontSize: 14 }]}>{"Due Fee - 2021"} </Text>
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20, marginTop: 30 }} >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={require('../../assets/profile.png')}
                                    style={{ height: 54, width: 54, resizeMode: 'contain', marginHorizontal: 10, borderRadius: 20 }}
                                />
                                <View style={{ marginLeft: 7 }}>
                                    <Text style={[myStyle.textBold, { fontSize: 15, color: '#28293D' }]}>{'Akshita Yadav'} </Text>
                                    <Text style={[myStyle.textNormal, { fontSize: 12, marginTop: 3, color: '#909090' }]}>{'Student ID : AK458245'} </Text>
                                    <Text style={[myStyle.textNormal, { fontSize: 14, marginTop: 3, color: '#28293D' }]}>{'May 21 - July 21'} </Text>
                                </View>
                            </View>

                            <View style={{ paddingHorizontal: 10, paddingVertical: 4, borderRadius: 10, borderWidth: 1, borderColor: Colors.lightGray }}>
                                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor, textAlign: 'center' }]}>{'Due Date'} </Text>
                                <Text style={[myStyle.textNormal, { fontSize: 16 }]}>{'28 July 21'} </Text>
                            </View>



                        </View>
                    </View>

                    <View style={{
                        marginHorizontal: 25,
                        marginTop: 12,
                        paddingTop: 11,
                        paddingBottom: 5,
                        backgroundColor: Colors.whiteText,
                        elevation: 3,
                        borderRadius: 20
                    }}>
                        <FlatList
                            data={this.props.payment_record}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => 'key' + index}
                            ListHeaderComponent={this.listHeaderComponent}
                            ListFooterComponent={this.listFooterComponent}
                        />
                    </View>

                    <View style={{ paddingHorizontal: 30, }}>
                        <TouchableOpacity style={styles.btn}
                            onPress={() => {
                                // this.onPressPay()
                            }}>
                            <Text style={[myStyle.textBold, { color: Colors.whiteText, fontSize: 14 }]}>{"Pay Now"}</Text>
                        </TouchableOpacity>

                    </View>

                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // padding: 40,
        backgroundColor: Colors.backgroundColor
    },

    btn: {
        height: 50,
        width: '95%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,
        borderRadius: 20,
        marginTop: 25,
        alignSelf: 'center'
    },
    txtinput: {
        height: 40,
        height: 65,
        width: 65,
        backgroundColor: Colors.backgroundColor,
        borderRadius: 10,
        paddingLeft: 25,
        fontSize: 30,
        // borderWidth: 0.2
        elevation: 5
    },
    rowOption: {
        flexDirection: 'row',
        flex: 1,
        paddingVertical: 10,
        justifyContent: 'space-between',
        backgroundColor: Colors.whiteText,
        borderTopWidth: 1,
        paddingLeft: 10,
        paddingRight: 20,
        borderColor: Colors.lightGray
    }

})


const mapStateToProps = (state) => {
    const rajpoot = state.indexReducerRajpoot;
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    const jaswant = state.indexReducerJaswant;

    return {

        payment_record: akshita.payment_record
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({


    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen)