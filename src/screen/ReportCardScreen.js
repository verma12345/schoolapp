import React, { Component } from "react"
import { Image, StyleSheet, Text, View, SafeAreaView, ScrollView, TouchableOpacity, FlatList, Pressable } from "react-native";
import Colors from "../common/Colors";
import { ReportRow } from "../components/ReportRow";
import { myStyle } from "../common/myStyle";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ReportCardHeader } from "../components/ReportCardHeader";


class ReportCardScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isPopUpMenu: false,
            x: 0,
            y: 0,
            quartterlyExamList: [
                { exam: 'Quarterly 1' },
                { exam: 'Quarterly 2' },
                { exam: 'Quarterly 3' },
                { exam: 'Quarterly 4' },
            ]
        }
    }


// go back navigation
    onPressBack = () => {
        this.props.navigation.goBack()
    }

    // to get the report card details (date, subject, total , grade)
    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                style={[myStyle.myshadow2, {
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    // alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                    marginHorizontal: 25,
                    paddingLeft: 15,
                    borderTopWidth: 1,
                    borderTopColor: Colors.lightGray,
                    borderBottomRightRadius: dataItem.index == this.props.reportCardMarksList.length - 1 ? 20 : 0,
                    borderBottomLeftRadius: dataItem.index == this.props.reportCardMarksList.length - 1 ? 20 : 0,

                }]}
            >

                <View style={{
                    flex: 1,
                    borderBottomLeftRadius: dataItem.index == this.props.reportCardMarksList.length - 1 ? 20 : 0,
                    justifyContent: 'center', paddingVertical: 8.5,
                }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.textColor, }]}>
                        {dataItem.item.exam_date1}
                    </Text>
                </View>

                <View style={{ flex: 1.5, justifyContent: 'center', paddingVertical: 8.5, }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.blue, }]}>
                        {dataItem.item.subject}
                    </Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 8.5, }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.textColor, }]}>
                        {dataItem.item.obtain_marks}
                    </Text>
                </View>

                <View style={{
                    flex: 1.2,
                    borderBottomRightRadius: dataItem.index == this.props.reportCardMarksList.length - 1 ? 20 : 0,
                    justifyContent: 'center', backgroundColor: '#F0F9EE', alignItems: "center", paddingHorizontal: 10
                }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, textAlign: 'center', color: Colors.textColor, }]}>
                        {dataItem.item.grade}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }


    // for the heading
    _renderHeader = () => {
        return (

            <View  >
                <ReportRow
                    containerStyle={{ marginBottom: 5 }}
                    grade={this.props.reportCardMarksList.grade}
                    percentage={this.props.reportCardMarksList.marks_percent}
                    rank={this.props.reportCardMarksList.rank}
                />

                {/*Circle Progress */}
                <View style={[myStyle.myshadow2, {
                    flexDirection: 'row',
                    borderBottomRightRadius: 20,
                    borderBottomLeftRadius: 20,
                    justifyContent: 'space-around',
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                    borderRadius: 15,
                    marginHorizontal: 25,
                    paddingVertical: 15,
                    marginTop: 10,
                    marginBottom: 15
                }]}>

                    <View>
                        <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>
                            {'Status'}</Text>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}> {this.props.reportCardMarksList.exam_status}  </Text>
                    </View>

                    <View style={{ width: 1, height: 50, backgroundColor: 'lightgray' }} />

                    <View >
                        <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]} >
                            {'Mark Obtain'}  </Text>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}> {this.props.reportCardMarksList.totak_marks_obtain}  </Text>
                    </View>

                </View>


                <View style={[myStyle.myshadow2, {
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    // alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                    paddingLeft: 15,
                    borderTopLeftRadius: 20,
                    marginHorizontal: 25,

                    borderTopRightRadius: 20
                }]}>


                    <View style={{ flex: 1, justifyContent: 'center', borderTopLeftRadius: 20, paddingVertical: 12, }} >
                        <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                            {'Date'}
                        </Text>
                    </View>

                    <View style={{ flex: 1.5, justifyContent: 'center', paddingVertical: 12, }} >
                        <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                            {'Subject'}
                        </Text>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 12, }} >
                        <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                            {'Total'}
                        </Text>
                    </View>

                    <View style={{ flex: 1.2, justifyContent: 'center', backgroundColor: '#F0F9EE', alignItems: "center", borderTopRightRadius: 20, paddingHorizontal: 10 }} >
                        <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                            {'Mark/Grade'}
                        </Text>
                    </View>

                </View>
            </View>
        )
    }

    onPressMenu = (event) => {
        this.setState({
            isPopUpMenu: !this.state.isPopUpMenu,
            x: event.layout.x,
            y: event.layout.y
        })
    }
    onPressClassTest = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }
    onPressQuarterly = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }
    onPressHalfYearly = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }
    onPressAnnually = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }
    render() {
        // console.log(this.props.sessionYear);
        return (
            <SafeAreaView style={styles.container}>
                <View
                    style={styles.container}
                >
                    <ReportCardHeader
                        onPressBack={this.onPressBack}
                        icon1={require('../../assets/profile.png')}
                        txt="Report Card"
                        icon2={require('../../assets/filter1.png')}
                        isMenu={true}
                        onPressMenu={this.onPressMenu}
                        year={"21-22"}
                        // year={this.props.sessionYear}
                        exam_name={this.props.reportCardMarksList.exam_name}
                        data={this.state.quartterlyExamList}
                    />







                    {/* Report Card */}

                    {/* <View style={[myStyle.myshadow2, { borderRadius: 20, marginHorizontal: 25, marginBottom: 100, backgroundColor: 'white' }]}> */}

                    <FlatList
                        contentContainerStyle={{ paddingBottom: 150 }}
                        data={this.props.reportCardMarksList.marks}
                        renderItem={this._renderItem}
                        ListHeaderComponent={this._renderHeader}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item, index) => 'key' + index}
                    />

                    {/* </View> */}


                    <TouchableOpacity style={{
                        backgroundColor: Colors.blue,
                        alignSelf: 'center',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '80%',
                        position: 'absolute',
                        elevation: 3,
                        bottom: 20,
                        paddingVertical: 15,
                        borderRadius: 18
                    }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, color: 'white' }]} >
                            {'Download PDF'} </Text>

                        <Image
                            source={require('../../assets/pdf.png')}
                            style={{ height: 18, width: 18, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>



                    {this.state.isPopUpMenu ?
                        <View style={{
                            position: 'absolute',
                            backgroundColor: Colors.whiteText,
                            elevation: 20,
                            left: this.state.x - 100,
                            top: this.state.y + 50,
                            borderRadius: 10
                        }}>
                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressClassTest()}
                                style={{ paddingVertical: 12, borderRadius: 10, paddingHorizontal: 25, backgroundColor: Colors.backgroundColor }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'Class Test'}
                                </Text>
                            </Pressable>

                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressQuarterly()}
                                style={{ paddingVertical: 12, paddingHorizontal: 25 }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'Quarterly'}
                                </Text>
                            </Pressable>

                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressHalfYearly()}
                                style={{ paddingVertical: 12, paddingHorizontal: 25 }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'HalfYearly'}
                                </Text>
                            </Pressable>

                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressAnnually()}
                                style={{ paddingVertical: 12, paddingHorizontal: 25 }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'Annually'}
                                </Text>
                            </Pressable>
                        </View>
                        : null}
                </View>


                {this.props.is_loading ? (
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#0008',
                            elevation: 10,
                            position: 'absolute',
                        }}>
                        <View style={{ borderRadius: 10, backgroundColor: "white", height: 200, width: 300, justifyContent: "center", alignItems: "center", }}>
                            <ActivityIndicator
                                size={"large"}
                                color={Colors.blue}
                            />
                            <Text style={{ fontSize: 14 }} >{"Please wait for a while..."} </Text>
                        </View>
                    </View>
                ) : null}

            </SafeAreaView>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'space-between',
        backgroundColor: Colors.backgroundColor
    },
    txt: {
        fontSize: 34,
        fontWeight: 'bold',
        paddingTop: 40,
        color: Colors.textColor
    }
})

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jas = state.indexReducerJaswant;


    return {
        reportCardMarksList: jas.reportCardMarksList,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {

            // setEmptyWeek: () => setEmptyWeek(),
        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ReportCardScreen);