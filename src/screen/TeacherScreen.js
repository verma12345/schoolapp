import React, { Component, PureComponent } from "react";
import { ActivityIndicator, FlatList, Image, PixelRatio, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { phoneCall, sendEmail } from "../common/common";
import Constants, { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import { Loader } from "../components/Loader";
import { TeacherHeader } from "../components/TeacherHeader";
import { setClassTeacherDetails, setTeacherDetailsList } from "../redux_store/actions/indexActionAkshita";
import { hitTeacherListApi } from "../redux_store/actions/indexActionApi";
import { setLoading } from "../redux_store/actions/indexActions";

class TeacherScreen extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    // api for teacher_list
    componentDidMount() {

        let params = {
            school: {
                school_id: 1,
            }
        }

        this.props.hitTeacherListApi(params).then(res => {
            if (res.school.status == 1) {
                this.props.setTeacherDetailsList(res.school.teacher_list)
                this.props.setClassTeacherDetails({
                    name: res.school.class_teacher,
                    email: res.school.email,
                    mobile: res.school.mobile,
                    profile_pic: res.school.profile_pic,
                })
            }
        })
    }

    // for go back navigation
    onPressBack = () => {
        this.props.navigation.goBack()
    }

    //on call btn
    onCallTeacher = () => {
        phoneCall('')
    }

    //on email btn
    onEmailPress = () => {
        let email = ['akshitainfra30@gmail.com', 'jaswant.raj44@gmail.com'];
        let subject = 'Send Email for testing';
        let body = "Hi there! Congratulation you have send your message successfully!"
        sendEmail(email, subject, body)
    }

    // onPressSubjectTeacher = (teachers_details) => {
    //     debugLog(teachers_details)
    //     this.setState({
    //         subject_teacher_pic: teachers_details.teacher_pic,
    //         subject_teacher_name: teachers_details.class_teacher,
    //         subject_name: teachers_details.subject
    //     })
    // }


    // to get class teacher details
    renderHeader = (data) => {
        return (
            <View style={{ marginBottom: 12 }}>
                <Text style={[myStyle.textBold, { fontSize: 14, paddingHorizontal: 25, marginTop: 15 }]}>
                    {'Class Teachers'}
                </Text>

                <View style={{ marginHorizontal: 14, marginTop: 15 }}>

                    <View style={[myStyle.myshadow, {
                        paddingHorizontal: 10,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        backgroundColor: Colors.whiteText
                    }]} >
                        <View style={{ flexDirection: 'row', paddingTop: 8, paddingBottom: 14, alignItems: 'center', }} >
                            <Image
                                style={{
                                    height: 40,
                                    width: 40
                                }}
                                // source={{ uri: this.state.subject_teacher_pic }}
                                source={this.props.classTeacherDetails.profile_pic == undefined ? require('../../assets/user.png') : { uri: this.props.classTeacherDetails.profile_pic }}
                            />
                            <View style={{ paddingHorizontal: 15 }} >
                                <Text style={[myStyle.textBold, { fontSize: 14 }]} >
                                    {/* {this.state.subject_teacher_name != null ? this.state.subject_teacher_name : "David Jones"} */}
                                    {this.props.classTeacherDetails.name}
                                </Text>


                                {/* <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]} >
                                    {this.state.subject_name != null ? this.state.subject_name : "English"}
                                </Text> */}

                                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.textColor }]} >
                                    {this.props.classTeacherDetails.mobile}
                                </Text>

                                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.textColor }]} >
                                    {this.props.classTeacherDetails.email}
                                </Text>
                            </View>
                        </View>





                    </View>

                    <View style={{ height: 0.5, width: '100%', backgroundColor: Colors.lightGray }} />


                    <View style={[myStyle.myshadow, {
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        backgroundColor: 'white',
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                    }]} >
                        <TouchableOpacity
                            onPress={() => this.onCallTeacher()}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 14 }]}>
                                {'Call'}
                            </Text>
                        </TouchableOpacity>

                        <View style={{ height: 40, width: 1, backgroundColor: Colors.lightGray }} />

                        <TouchableOpacity
                            onPress={() => this.onEmailPress()}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={[myStyle.textBold, { fontSize: 14 }]}>
                                {'Email'}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <Text style={[myStyle.textBold, { fontSize: 14, paddingHorizontal: 25, marginTop: 30 }]}>
                    {'Subject Teachers'}
                </Text>
            </View>
        )
    }



    // to get the teacher details
    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => debugLog(dataItem.item)}
                // onPress={() => this.props.onPress(dataItem.item)}
                style={[myStyle.myshadow2, {
                    flexDirection: 'row',
                    marginHorizontal: 14,
                    marginVertical: 7,
                    paddingHorizontal: 10,
                    paddingVertical: 7,
                    borderRadius: 20,
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                }]} >
                <Image
                    style={{
                        height: 55,
                        width: 55
                    }}
                    source={{ uri: dataItem.item.profile_pic }}
                />
                <View style={{ paddingHorizontal: 15 }} >
                    <Text style={[myStyle.textBold, { fontSize: 14 }]} >
                        {dataItem.item.teacher_name}
                    </Text>


                    <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]} >
                        {dataItem.item.subject}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {

        return (
            <SafeAreaView style={myStyle.container}>
                <View style={[myStyle.container]}>
                    <TeacherHeader onPressBack={this.onPressBack}
                        teacher_pic={require('../../assets/profile.png')}
                        txt="Teachers"
                    />

                    <View style={{ flex: 1, marginTop: 12 }} >

                        <FlatList
                            contentContainerStyle={{ paddingBottom: 100 }}
                            data={this.props.teacher_list}
                            renderItem={this._renderItem}
                            ListHeaderComponent={this.renderHeader}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => index + 'key'}

                        />
                    </View>


                </View >

                {this.props.is_loading ? (
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#0008',
                            elevation: 10,
                            position: 'absolute',
                        }}>
                        <View style={{ borderRadius: 10, backgroundColor: "white", height: 200, width: 300, justifyContent: "center", alignItems: "center", }}>
                            <ActivityIndicator
                                size={"large"}
                                color={Colors.blue}
                            />
                            <Text style={{ fontSize: 14 }} >{"Please wait for a while..."} </Text>
                        </View>
                    </View>
                ) : null}

                {
                    this.props.isLoading ?
                        <Loader />
                        : null
                }
            </SafeAreaView >

        )
    }
}


const styles = StyleSheet.create({



});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;


    return {
        isLoading: common.isLoading,
        teacher_list: akshita.teacher_list,
        classTeacherDetails: akshita.classTeacherDetails
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setLoading: (isTrue) => setLoading(isTrue),
            setTeacherDetailsList: (list) => setTeacherDetailsList(list),
            setClassTeacherDetails: (list) => setClassTeacherDetails(list),

            hitTeacherListApi: (params) => hitTeacherListApi(params)

        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(TeacherScreen);