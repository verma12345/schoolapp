import React, { Component } from "react";
import { ActivityIndicator, SafeAreaView, ScrollView, StyleSheet, Text, View, TextInput, Dimensions } from "react-native";
import WebView from "react-native-webview";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Constants, { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import { setPrefs } from "../common/Prefs";
import { setAlotExam, setCoupanCode, setExamInfo, setIsChecked, setQuizSession, setTotalSeconds, setTotalTime } from "../redux_store/actions/indexActionsJaswant";
import Colors from "../common/Colors";
import { hitAlotExamApi, hitExamInfoApi } from "../redux_store/actions/indexActionApi";
import ELearningSyllabusQuiz_Row from "../row_component/ELearningSyllabusQuiz_Row";
import HeaderComponent from "../components/HeaderComponent";
import CheckBox from "../components/CheckBox";
import { CustomeButtton } from "../components/CustomeButtton";


class AboutQuizScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            is_check: false,
            on_simple: true,
            on_moderate: false,
            on_hard: false,
            isLoading: false,
        }
    }


    componentDidMount() {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "school": {
                "stu_id": this.props.user_id,
                "exam_id": 3

            }
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        // fetch("https://demoapps.in/school/index.php/user_controller/exam_info", requestOptions)
        fetch(`${Constants.API_BASE_URL_PHP}user_controller/exam_info` , requestOptions)
            .then(response => response.json())
            .then(response => {
                let _response = response.school;
                // console.log(_response)
                setPrefs('about_quiz', JSON.stringify(_response))
                this.props.setExamInfo(_response)
                this.props.setTotalSeconds(_response.time_alloted * 60)
                this.getTotalTime(_response.time_alloted * 60)
            })
            .catch(error => console.log('error', error));
    }

    getTotalTime(secs) {
        let hours = Math.floor(secs / (60 * 60));

        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        let obj = {
            "h": hours,
            "m": minutes,
            "s": seconds,
            "progressIterval": 100 / minutes,
        };
        this.props.setTotalTime(obj)
        console.log(obj);
    }


    onNext = () => {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "user_id": this.props.user_id,
            "coupon_code": 7789
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        this.setState({ isLoading: true })
        // fetch("http://35.159.9.64:5512/api/allot_exam", requestOptions)
        fetch(`${Constants.API_BASE_URL}allot_exam`, requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.status == 1) {
                    debugLog(result)
                    setPrefs('session', JSON.stringify(result.session_id))
                    this.props.setQuizSession(result.session_id)
                    this.props.setAlotExam(result)
                    this.setState({ isLoading: false })
                    this.props.navigation.navigate('QuizSheetScreen')
                } else {
                    alert(result.msg)
                    // alert("Coupan code invalid");
                }
            })
            .catch(error => {
                this.setState({ isLoading: false })
                alert(error)
                console.log('error', error)
            });
    }

    _onNext = () => {
        let param = {
            user_id: this.props.user_id,
            coupon_code: 7789
        };
        this.props.hitAlotExamApi(param).then((response) => {
            if (response.status == 1) {
                debugLog('oooooooooo')
                debugLog(response.status)
                setPrefs('session', JSON.stringify(response.session_id))
                this.props.setQuizSession(response.session_id)
                this.props.setAlotExam(response)
                // this.setState({ isLoading: false })
                this.props.navigation.navigate('QuizSheetScreen')

                this.props.navigation.dispatch(
                    StackActions.replace('QuizDashboard'),
                )
            } else {
                alert(response.msg);
            }
        });
    }

    setCheckUncheck = () => {
        this.setState({ is_check: !this.state.is_check })
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }

    onPressSimple = () => {
        this.setState({
            on_simple: true,
            on_moderate: false,
            on_hard: false
        })
    }
    onPressModerate = () => {
        this.setState({
            on_simple: false,
            on_moderate: true,
            on_hard: false
        })
    }
    onPressHard = () => {
        this.setState({
            on_simple: false,
            on_moderate: false,
            on_hard: true
        })
    }

    _renderItem = (dataItem) => {
        return (
            <ELearningSyllabusQuiz_Row
                dataRow={dataItem.item}
            //   onPress={this.onPressElearningSubject}
            />
        )
    }
    onClickNext = () => {
        this.props.navigation.navigate('QuizSheetScreen')
    }

    renderLoadingView = () => {
        return (
            <View style={{
                borderRadius: 10,
                backgroundColor: "white",
                flex: 1,
            }}>
                <ActivityIndicator
                    size={"large"}
                    color={Colors.textColor}
                    hidesWhenStopped={true}
                />
            </View>
        );
    }


    render() {
        return (
            <SafeAreaView style={myStyle.container}>
                <HeaderComponent onPressBack={() => this.onPressBack()}
                    // title={this.props.eLearningSyllabusList.subject_name}
                    title={this.props.eLearningSyllabusList.subject_name + ' Syllabus'}

                    style={{
                        height: 62, elevation: 3, 
                        borderBottomRightRadius: 20, borderBottomLeftRadius: 20
                    }}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={[myStyle.container]}>


                    <View style={{ flex: 1, marginHorizontal: 15, paddingVertical: 20, }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center', }}>

                            <Text
                                style={[myStyle.textBold, { fontSize: 14, marginBottom: 3, marginRight: 20 }]}>
                                {'Enter Coupan Code'}
                            </Text>

                            <TextInput
                                placeholder="Enter Coupan Code here"
                                onChangeText={(txt) => this.props.setCoupanCode(txt)}
                                keyboardType="number-pad"
                            />
                        </View>

                        {/* 
                        <Text style={[myStyle.textBold, { fontSize: 14, marginBottom: 10 }]}>
                            {'Complexity'}
                        </Text> */}

                        {/* <QuizComplexityComponent
                            complexity_type={this.state.complexity_type}
                            onPressSimple={() => this.onPressSimple()}
                            onPressModerate={() => this.onPressModerate()}
                            onPressHard={() => this.onPressHard()}

                            on_simple={this.state.on_simple}
                            on_moderate={this.state.on_moderate}
                            on_hard={this.state.on_hard}
                        /> */}

                        <View style={{ marginTop: 20, }} >
                            <Text style={[myStyle.textBold, { fontSize: 14, }]}>
                                {'Description'}
                            </Text>

                            <Text style={[myStyle.textNormal, { fontSize: 14, marginTop: 3, color: Colors.grayColor }]}>
                                {this.props.exam_info.description}
                            </Text>

                            <Text style={[myStyle.textBold, { fontSize: 14, marginTop: 30 }]}>
                                {'Instruction'}
                            </Text>


                            <WebView
                                originWhitelist={['*']}
                                source={{ html: this.props.exam_info.introduction }}
                                style={[{
                                    height: Dimensions.get('window').height / 5,
                                    backgroundColor: Colors.backgroundColor,
                                }]}
                                renderLoading={this.renderLoadingView}
                                javaScriptEnabled={true}
                                domStorageEnabled={true}
                                scalesPageToFit={(Platform.OS === 'android') ? false : true}
                            />

                        </View>


                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 25 }}>
                            <Text style={[myStyle.textBold, { fontSize: 15 }]}>{'Time: ' + this.props.exam_info.time_alloted + ' mins'} </Text>
                            <Text style={[myStyle.textBold, { fontSize: 15 }]}>{'Question: ' + this.props.exam_info.total_questions} </Text>
                        </View>
                        <Text style={[myStyle.textBold, { fontSize: 15, marginTop: 12 }]}>{'Maximum Marks: ' + this.props.exam_info.max_marks} </Text>


                        <CheckBox
                            onPress={() => this.props.setIsChecked(!this.props.isCheckedQuiz)}
                            is_check={this.props.isCheckedQuiz}
                            onPressUrl={() => this.props.navigation.navigate('QuizTncScreen')}
                        />


                        <CustomeButtton
                            onPress={() => { this.props.isCheckedQuiz ? this.onNext() : null }}
                            disabled={this.props.isCheckedQuiz ? false : true}
                            title='Next'
                            style={{
                                marginTop: 45,  backgroundColor: this.props.isCheckedQuiz ? Colors.blue : Colors.btncolor,
                                elevation: this.props.isCheckedQuiz ? 5 : 0,
                                alignSelf:'center',
                                width: '90%'
                            }}
                            titleStyle={{ color: this.props.isCheckedQuiz ? Colors.whiteText : Colors.textColor }}
                        >
                            {this.state.isLoading ? <ActivityIndicator style={{ marginLeft: 20 }} size="small" color="white" /> : null}
                        </CustomeButtton>

                    </View>

                </ScrollView >
            </SafeAreaView >

        )
    }
}


const styles = StyleSheet.create({



});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jazz = state.indexReducerJaswant;



    return {
        user_id: common.user_id,
        alot_exam: jazz.alot_exam,
        exam_info: jazz.exam_info,
        quiz_session: jazz.quiz_session,
        coupan_code: jazz.coupan_code,
        isCheckedQuiz: jazz.isCheckedQuiz,
        eLearningSyllabusList: jazz.eLearningSyllabusList


    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setQuizSession: (list) => setQuizSession(list),
            setAlotExam: (details) => setAlotExam(details),
            setExamInfo: (details) => setExamInfo(details),
            setCoupanCode: (coupan) => setCoupanCode(coupan),
            setIsChecked: (isTrue) => setIsChecked(isTrue),

            hitExamInfoApi: (param) => hitExamInfoApi(param),
            hitAlotExamApi: (param) => hitAlotExamApi(param),
            setTotalSeconds: (seconds) => setTotalSeconds(seconds),
            setTotalTime: (t) => setTotalTime(t),

        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(AboutQuizScreen);