import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { StackActions } from "@react-navigation/routers";
import React, { Component } from "react";
import { Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import { getPrefs, setPrefs } from "../common/Prefs";
import { CustomeButtton } from "../components/CustomeButtton";
import { LoginHeader } from "../components/LoginHeader";
import MyInput from "../components/MyInput";
import { hitLoginApi } from '../redux_store/actions/indexActionApi';
import { setUserId } from "../redux_store/actions/indexActions";
import { setLoginInfo, setMobile, setPass, setShowPass, setStudentDetails, setStudentDetailsList } from '../redux_store/actions/indexActionsJaswant';

class LoginScreen extends Component {
    constructor(props) {
        super(props)
    }

    onChangeTextPass = (password) => {
        this.props.setPass(password)
    }

    showPassword = () => {
        this.props.setShowPass(!this.props.showpass)
    }

    _validation = () => {
        // const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        const pass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{5,})/;
        this.props.navigation.navigate('DashboardScreen')


    }

    componentDidMount() {
        // GoogleSignin.configure({
        //     webClientId: "446921249604-7j6885k27e15ivordbn5n1eeis3ormlr.apps.googleusercontent.com", // client ID of type WEB for your server(needed to verify user ID and offline access)
        //     offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        //     forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
        //     accountName: '', // [Android] specifies an account name on the device that should be used
        // });
        GoogleSignin.configure()
    }

    //google signin
    googleSignIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const info = await GoogleSignin.signIn();
            debugLog(info.user.email)
            debugLog(info.user.familyName)
            debugLog(info.user.givenName)
            debugLog(info.user.name)
            debugLog(info.user.id)
            debugLog(info.user.photo)

            // Hit Api

            this.props.navigation.navigate('DashboardScreen')

        } catch (error) {
            alert(error.code)
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    // signOut = async () => {
    //     try {
    //         await GoogleSignin.revokeAccess();
    //         await GoogleSignin.signOut();
    //         // setUserInfo(null); // Remember to remove the user from your app's state as well
    //     } catch (error) {
    //         console.error(error);
    //     }
    // };


    // for login api
    onClickLogin = () => {
        if (this.props.mobile == '' || this.props.password == '') {
            alert('Please enter email and password');
            return;
        }

        if (this.props.mobile.length < 5) {
            alert('Please enter valid mobile ');
            return;
        }

        let param = {
            school: {
                email: this.props.mobile.includes('@') ? this.props.mobile : '',
                password: this.props.password,
                device_os: Platform.OS == "android" ? 'Android' : 'iOS',
                phone: this.props.mobile.includes('@') ? '' : this.props.mobile,
                device_token: "00007A0E4-517dsD-9D908-1EB3C9B8E749"
            }
        };
        debugLog(param)
        // return
        this.props.hitLoginApi(param).then((response) => {
            let _response = response.school;
            if (_response.status == 1) {
                // debugLog(_response.student_detail)
                // this._setUserInfo(_response)
                this.props.setLoginInfo(_response)
                this.props.setUserId(_response.user_id)
                this.props.setStudentDetailsList(_response.student_detail)
                setPrefs('user_login', '1')
                setPrefs('user_id', JSON.stringify(_response.user_id))

                this.props.navigation.dispatch(
                    StackActions.replace('DashboardScreen'),
                )
            } else {
                alert(_response.msg);
            }
        });
    }

    // called on signup btn
    onSignUp = () => {
    
        getPrefs('user_signedup').then((value => {

            if (value !== null && value == '1') {
                this.props.navigation.navigate('OtpScreen')

            } else {
                this.props.navigation.navigate('SignupScreen')
            }
        }))
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar
                    hidden={false} />
                <View style={styles.container}>
                    <ScrollView style={styles.container}>
                        <View style={{ paddingHorizontal: 40 }}>
                            <LoginHeader
                                navigation={this.props.navigation}
                                backpic={require('../../assets/back.png')}
                                txtheading={'Login'}
                                txt={'Sign in with your data that you have\nentered during your registration'}
                            />

                            <MyInput
                                label='Enter Mobile Number / Email'
                                value={this.props.mobile}
                                onChangeText={(mobile) => { this.props.setMobile(mobile) }}
                                keyboardType='email-address'
                                maxLength={this.props.mobile.match(/^\d/) ? 10 : 35}
                                style={{ marginTop: 120 }}
                            />

                            <MyInput
                                type='password'
                                label='Your Password'
                                value={this.props.password}
                                onChangeText={(password) => { this.onChangeTextPass(password) }}
                                keyboardType='email-address'
                                maxLength={15}
                                // secureTextEntry={this.props.showpass}
                                secureTextEntry={true}
                                onPress={() => this.showPassword()}
                                showpass={this.props.showpass}
                                style={{ marginTop: 20, alignItems: 'center' }}
                            />

                            <CustomeButtton
                                onPress={() => { this.onClickLogin() }}
                                title='Login'
                                style={{ marginTop: 30 }}
                            />


                            <TouchableOpacity style={{ marginTop: 20, alignItems: 'center', }}
                                onPress={() => this.props.navigation.navigate('ForgotPasswordScreen')}
                            >
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>{'Forgot Password?'}</Text>
                            </TouchableOpacity>


                            <CustomeButtton
                                onPress={() => { this.onClickLogin() }}
                                title='Continue with Google'
                                style={{ marginTop: 50 }}
                                icon_left={require('../../assets/google1.png')}
                                txtStyle={{ color: '#E45F37' }}
                            />


                            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 60 }}>
                                <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]}>{"Don't have an account?"}</Text>
                                <TouchableOpacity onPress={() => this.onSignUp()}>
                                    <Text style={[myStyle.textBold, { color: Colors.signuptextcolor, fontSize: 12 }]}>{' Sign up'}</Text>
                                </TouchableOpacity>
                            </View>

                        </View>







                    </ScrollView>
                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: Colors.backgroundColor
    },

    view2: {
        height: 50,
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,
        marginTop: 120,
        paddingLeft: 40,
        elevation: 1

    },

    btn: {
        height: 56,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,
        marginTop: 30,
        borderRadius: 20,
    }
})



const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jas = state.indexReducerJaswant;

    return {
        is_loading: common.is_loading,
        user_id: common.user_id,

        mobile: jas.mobile,
        password: jas.password,
        showpass: jas.showpass,
        // loginInfo: akshita.loginInfo,
        students_list:jas.students_list
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setMobile: (mobile) => setMobile(mobile),
        setPass: (password) => setPass(password),
        setShowPass: (showpass) => setShowPass(showpass),
        hitLoginApi: (param) => hitLoginApi(param),
        setLoginInfo: (login_info) => setLoginInfo(login_info),
        setUserId: (id) => setUserId(id),
        setStudentDetails: (list) => setStudentDetails(list),
        setStudentDetailsList: (list) => setStudentDetailsList(list),

    }, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)