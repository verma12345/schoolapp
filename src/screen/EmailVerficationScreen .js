import React, { Component } from "react";
import { ActivityIndicator, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import Font from "../common/Font";
import { myStyle } from "../common/myStyle";
import { getPrefs } from "../common/Prefs";
import OTPTextInput from "../components/OTPTextInput";
import { VarificationHeader } from "../components/VarificationHeader";
import { setEmailOtp } from "../redux_store/actions/indexActionAkshita";
import { setSignupEmail } from "../redux_store/actions/indexActions";


class EmailVerficationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            otp: ''
        }
    }

    // get otp from local storage
    componentDidMount() {

        getPrefs('email_otp').then((value => {
            // console.log(value);
            if (value != undefined) {
                this.props.setEmailOtp(value)
            }
        }))

        //get signup email from  local storage
        getPrefs('email').then((value => {
            console.log(value);
            if (value !== null) {
                this.props.setSignupEmail(value)
            }
        }))
    }

    // api for verifyOtp 
    verifyOtp = () => {
        if (this.state.otp == '') {
            alert('Please enter otp')
            return;
        }

        if (this.props.email_otp != this.state.otp) {
            alert('Please enter correct otp')
            return;
        }

        var myHeaders = new Headers();

        var raw = JSON.stringify({
            school: {
                user_id: this.props.user_id,
                email: this.props.email
            }
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://demoapps.in/school/index.php/user_controller/verify_email", requestOptions)
            .then(response => response.json())
            .then(response => {
                if (response.school.status == 1) {
                    // alert(response.school.msg)
                    this.props.navigation.navigate('SuccessScreen')
                    console.log(response)
                }
            })

            .catch(error => console.log('error', error));
    }

    // for VarificationScreen navigation
    _onChange = () => {
        this.props.navigation.navigate('VarificationScreen')
    }


    onResendOtp = () => {
        this.props.navigation.navigate('EmailVerficationScreen')
    }

    // main component
    render() {
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.container}>
                    <ScrollView style={{ marginBottom: 50 }}>
                        <View style={{ alignItems: 'center', paddingTop: 130 }}>
                            <VarificationHeader
                                txtHeading={'Email Verification'}
                                txt={'We just sent you an Email with\n 5-digit code.'}
                            />
                        </View>
                        {/* <View style={{ flex: 3 }}> */}


                        <View style={{ alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', padding: 20 }}>
                                <Text style={myStyle.textNormal, [{ color: Colors.textColor }]}>{this.props.signup_email} </Text>
                                <TouchableOpacity onPress={() => this._onChange()}>
                                    <Text style={[myStyle.textNormal, { color: Colors.blue, fontFamily: Font.Roboto }]}>{'Change'}</Text>
                                </TouchableOpacity>

                            </View>
                            <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{`Enter this code into field below:`}</Text>

                            <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{'OTP: '}{this.props.email_otp}</Text>


                            {/* otp text input */}
                            <OTPTextInput
                                // value={this.state.otp}
                                onChange={(otp) => {
                                    // debugLog(a)
                                    // this.props.setSmsOtp(otp)
                                    this.setState({
                                        otp: otp
                                    })
                                }}
                            />

                            {/* </View> */}
                            <TouchableOpacity style={[styles.btn, { flexDirection: 'row' }]}
                                onPress={() => {
                                    // this.props.getOTP == '' ? this._onPressGetOtp() : this.verifyAndSignUp()
                                    this.verifyOtp()

                                }}>
                                <Text style={[, myStyle.textBold, { color: Colors.whiteText }]}>{'Verify'}</Text>

                                {this.props.is_loading ?
                                    <ActivityIndicator
                                        size='small'
                                        color='white'
                                    />
                                    : null
                                }
                            </TouchableOpacity>


                            <TouchableOpacity
                                onPress={()=> this.onResendOtp()}
                            >
                                <Text style={[myStyle.textBold, { fontSize: 14, paddingTop: 30, color: Colors.blue }]}>
                                    {'Resend OTP'}
                                </Text>
                            </TouchableOpacity>

                        </View>
                        {/* </View> */}

                    </ScrollView>

                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // padding: 40,
        backgroundColor: Colors.backgroundColor
    },

    btn: {
        height: 56,
        width: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,
        marginTop: 45,
        borderRadius: 20,
    },
    txtinput: {
        height: 40,
        height: 65,
        width: 65,
        backgroundColor: Colors.backgroundColor,
        borderRadius: 10,
        paddingLeft: 25,
        fontSize: 30,
        // borderWidth: 0.2
        elevation: 5
    }

})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita
    const jaswant = state.indexReducerJaswant;


    return {
        is_loading: common.is_loading,
        user_id: common.user_id,
        email_otp: akshita.email_otp,
        signup_email: common.signup_email,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

        setEmailOtp: (param) => setEmailOtp(param),
        setSignupEmail: (email) => setSignupEmail(email),

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EmailVerficationScreen)