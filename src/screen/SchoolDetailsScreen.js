import React, { Component } from "react";
import { Dimensions, Image, ImageBackground, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import { setUpcomingEventList } from "../redux_store/actions/indexActionAkshita";
import SchoolDetailsMap from "./SchoolDetailsMap";
const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 0
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class SchoolDetailsScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      height: 0,
      apsoluteHeight: 0,
      width: 0,
      is_going: true,
      mayBe: false,
      notGoing: false,

      coordinates: null,
      currentPosition: 0,
      isPopup: false

    }
    this.mapView = null;

  }


  onPressBack = () => {
    this.props.navigation.goBack()
  }


  onLayout = (e) => {
    this.setState({ height: e.nativeEvent.layout.height, width: e.nativeEvent.layout.width })
  }

  onLayoutApsolute = (e) => {
    this.setState({ apsoluteHeight: e.nativeEvent.layout.height })
  }

  render() {
    return (

      <SafeAreaView style={myStyle.container} >
        <StatusBar
          hidden={true} />

        <View style={myStyle.container}>
          {/* ********************************************TOP COMPONENT************************************** */}
          <View onLayout={(e) => this.onLayout(e)}
            style={{ flex: 1 }} >
            <ImageBackground
              style={{ height: 300, flex: 1 }}
              source={require('../../assets/college3.png')}
            >
              <View style={{ flexDirection: 'row', alignItems: "center", marginTop: 14 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                  style={{ padding: 15 }}>
                  <Image
                    style={{ height: 20, width: 20, tintColor: 'white', resizeMode: 'contain' }}
                    source={require('../../assets/back.png')}
                  />
                </TouchableOpacity>
                <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.whiteText, marginLeft: 25 }]}>{'School / College Details'} </Text>



              </View>
            </ImageBackground>
          </View>



          {/* {/********************************** * BOTTOM COMPONENT **********************************} */}

          <View style={{
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
            backgroundColor: Colors.backgroundColor,
            flex: 2.6,
            marginTop: 30,
            paddingHorizontal: 20,
            paddingVertical: 20,

          }}>


            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 100 }} >



              <Text style={[myStyle.textBold, { fontSize: 13, marginTop: 55 }]}>{'About'} </Text>

              <Text style={[myStyle.textNormal, { fontSize: 13, marginTop: 3, color: Colors.grayColor }]}>
                {'eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non p'}
              </Text>

              <View style={{
                flexDirection: "row",
                alignItems: 'center',
                justifyContent: 'space-between',
                height: 180,
                width: '100%',
                paddingHorizontal: 1
              }}>
                <View style={{ height: width / 2.4, width: width / 2.4, justifyContent: 'center', alignItems: 'center', elevation: 2, backgroundColor: Colors.backgroundColor, borderRadius: 15 }}>
                  <Image
                    source={require('../../assets/teacher5.png')}
                    style={{ height: 55, width: 55,borderRadius:20 }}
                  />
                  <Text style={[myStyle.textBold, { fontSize: 16, marginTop: 8 }]}>{'Narendra S.'} </Text>
                  <Text style={[myStyle.textBold, { fontSize: 14, marginTop: 5, color: Colors.grayColor }]}>{'Principal'} </Text>
                </View>

                <View style={{ height: width / 2.4, width: width / 2.4, justifyContent: 'center', alignItems: 'center', elevation: 2, backgroundColor: Colors.backgroundColor, borderRadius: 15 }}>
                  <Image
                    source={require('../../assets/teacher3.png')}
                    style={{ height: 55, width: 55,borderRadius:20 }}
                  />
                  <Text style={[myStyle.textBold, { fontSize: 16, marginTop: 8 }]}>{'Richard Das'} </Text>
                  <Text style={[myStyle.textBold, { fontSize: 14, marginTop: 5, color: Colors.grayColor }]}>{'Voice Principal'} </Text>
                </View>
              </View>

              <Text style={[myStyle.textBold, { fontSize: 13, marginVertical: 10 }]}>{'Get Direction'} </Text>
              <SchoolDetailsMap />



            </ScrollView>
          </View>



          {/* ******************************************** ABSOLUTE UI ******************************************** */}
          <View
            onLayout={(e) => this.onLayoutApsolute(e)}
            style={[{
              backgroundColor: Colors.whiteText,
              position: 'absolute',
              elevation: 6,
              width: "85%",
              alignSelf: 'center',
              top: this.state.height - this.state.apsoluteHeight / 3,
              borderRadius: 20,
              paddingTop: 17
            }, myStyle.myshadow2]}>

            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
              <View style={{ paddingLeft: 13, marginLeft: 7 }} >
                <Text style={[myStyle.textBold, { fontSize: 15, color: Colors.textDarkColor, marginLeft: 2, }]}> {"Lucknow Public School"} </Text>
                <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12, marginVertical: 3, marginLeft: 2, alignSelf: 'baseline', paddingHorizontal: 5 }]}> {"ICSE"} </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                  <Image
                    style={{ height: 12, width: 12, resizeMode: 'contain', marginLeft: 7 }}
                    source={require('../../assets/location_red.png')}
                  />
                  <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]}> {"Indira Nagar, Lucknow"} </Text>
                </View>
              </View>
            </View>


            <View style={{ flexDirection: 'row', borderTopWidth: 1, height: 46, marginTop: 10, borderColor: Colors.lightGray }}>
              <TouchableOpacity style={{
                flex: 1, alignItems: 'center', justifyContent: 'center', borderRightWidth: 1, borderColor: Colors.lightGray
              }}>
                <Text style={[myStyle.textNormal, { fontSize: 14 }]}>
                  {'Call'}
                </Text>
              </TouchableOpacity>


              <TouchableOpacity style={{
                flex: 1, alignItems: 'center', justifyContent: 'center'
              }}>
                <Text style={[myStyle.textNormal, { fontSize: 14 }]}>
                  {'Email'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>



        </View>




      </SafeAreaView >

    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor
  },

  btn: {
    height: 50,
    backgroundColor: Colors.blue,
    borderRadius: 18,
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
    flex: 1
  },

  active_btn: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.whiteText,
    borderRadius: 18,
    alignSelf: 'center',
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: Colors.blue,
    marginRight: 10,
    flex: 1

  },
});

const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const akshita = state.indexReducerAkshita;
  const jaswant = state.indexReducerJaswant;

  return {

    eventDetails: akshita.eventDetails
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

    setUpcomingEventList: (eventList) => setUpcomingEventList(eventList)
  }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SchoolDetailsScreen)
