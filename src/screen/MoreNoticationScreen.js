import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, StatusBar, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import Colors from "../common/Colors";
import Header from "../components/Header";
import { MoreNoticationRow } from "../row_component/MoreNoticationRow";

class MoreNoticationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isToggle: true,
            is_childNotification: true,
            is_remindNotification: true,
            is_busNotification: true
        }
    }


    onTogglePress = () => {
        this.setState({ isToggle: !this.state.isToggle })
    }
    onChildNotification = () => {
        this.setState({ is_childNotification: !this.state.is_childNotification })
    }
    onRemindMe = () => {
        this.setState({ is_remindNotification: !this.state.is_remindNotification })
    }
    onBusNotification = () => {
        this.setState({ is_busNotification: !this.state.is_busNotification })
    }
    onPressBack = () => {
        this.props.navigation.goBack()
    }
    render() {


        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={styles.container}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <SafeAreaView style={[styles.container, { justifyContent: "space-between" }]} >


                        <View style={{ flex: 1, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1 }} >

                                <Header
                                    onPressBack={() => this.onPressBack()}
                                    title="Notification"
                                    style={{
                                        elevation: 2,
                                        borderBottomLeftRadius: 20,
                                        borderBottomRightRadius: 20,
                                        backgroundColor: Colors.whiteText,
                                        paddingVertical: 32,
                                        paddingHorizontal: 15,
                                    }}
                                />

                                <MoreNoticationRow
                                    notification_title="Child Notification"
                                    notification_description="Details about this Notification"
                                    onPressToggle={() => this.onChildNotification()}
                                    // onPressNotification={() => this.onPressNotification()}
                                    isToggle={this.state.is_childNotification}
                                />

                                <View style={{ height: 1, backgroundColor: Colors.lightGray, width: '100%', alignSelf: 'center' }} />
                                <MoreNoticationRow
                                    notification_title="Remind me"
                                    notification_description="Details about this Notification"
                                    onPressToggle={() => this.onRemindMe()}
                                    // onPressNotification={() => this.onPressNotification()}
                                    isToggle={this.state.is_remindNotification}
                                />

                                <View style={{ height: 1, backgroundColor: Colors.lightGray, width: '100%', alignSelf: 'center' }} />
                                <MoreNoticationRow
                                    notification_title="Child Notification"
                                    notification_description="Details about this Notification"
                                    onPressToggle={() => this.onChildNotification()}
                                    // onPressNotification={() => this.onPressNotification()}
                                    isToggle={this.state.is_childNotification}
                                />

                                <View style={{ height: 1, backgroundColor: Colors.lightGray, width: '100%', alignSelf: 'center' }} />
                                <MoreNoticationRow
                                    notification_title="Bus track Notification"
                                    notification_description="Details about this Notification"
                                    onPressToggle={() => this.onBusNotification()}
                                    // onPressNotification={() => this.onPressNotification()}
                                    isToggle={this.state.is_busNotification}
                                />

                                <View style={{ height: 1, backgroundColor: Colors.lightGray, width: '100%', alignSelf: 'center' }} />
                                <MoreNoticationRow
                                      notification_title="Remind me"
                                      notification_description="Details about this Notification"
                                      onPressToggle={() => this.onRemindMe()}
                                      // onPressNotification={() => this.onPressNotification()}
                                      isToggle={this.state.is_remindNotification}
                                />
                                <View style={{ height: 1, backgroundColor: Colors.lightGray, width: '100%', alignSelf: 'center' }} />


                            </View>
                        </View>




                    </SafeAreaView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    // i
});

export default MoreNoticationScreen;