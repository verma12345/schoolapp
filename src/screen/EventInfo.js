import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, StatusBar, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback, TouchableOpacity, FlatList, ScrollView, Dimensions } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import { setUpcomingEventList } from "../redux_store/actions/indexActionAkshita";

class EventInfo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            height: 0,
            apsoluteHeight: 0,
            width: 0,
            is_going: true,
            mayBe: false,
            notGoing: false,
            whos_going_list: [
                { profile: require('../../assets/teacher1.png') },
                { profile: require('../../assets/teacher2.png') },
                { profile: require('../../assets/teacher3.png') },
                { profile: require('../../assets/teacher4.png') },
                { profile: require('../../assets/teacher5.png') },
                { profile: require('../../assets/teacher5.png') },
                { profile: require('../../assets/teacher5.png') },


            ]
        }
    }

    // for going btn
    onPressGoing = () => {
        this.setState({
            // is_going: !this.state.is_going,
            is_going: true,
            mayBe: false,
            notGoing: false
        })
    }

    //for mayBe btn
    onPressMayBe = () => {
        this.setState({
            is_going: false,
            mayBe: true,
            notGoing: false
        })
    }

    // for notGoing btn
    onPressNotGoing = () => {
        this.setState({
            is_going: false,
            mayBe: false,
            notGoing: true
        })
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }

    // for upcoming Btn
    onPressUpcoming = () => {
        this.setState({
            is_upcoming: true,
            is_past: false,
        })
    }

    //for upcoming Btn
    onPressPast = () => {
        this.setState({
            is_upcoming: false,
            is_past: true,
        })
    }

    // get component layout
    onLayout = (e) => {
        this.setState({ height: e.nativeEvent.layout.height, width: e.nativeEvent.layout.width })
    }

    // get component layout
    onLayoutApsolute = (e) => {
        this.setState({ apsoluteHeight: e.nativeEvent.layout.height })
    }

    // main component
    render() {
        let row = this.state.whos_going_list.map((item, index) => {
            return (
                index < 5 ?
                    index == 4 ?
                        <TouchableOpacity style={{
                            backgroundColor: '#E9E9E9',
                            height: width / 7, width: width / 7,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 15
                        }} >
                            <Text style={[myStyle.textBold, { fontSize: 14 }]} > {this.state.whos_going_list.length - 1 + "+"}</Text>
                        </TouchableOpacity>

                        :

                        <Image
                            source={item.profile}
                            style={{ height: width / 7, width: width / 7, marginRight: 15 }}
                        /> :
                    null
            )
        })
        return (

            <SafeAreaView style={myStyle.container} >

                <View style={myStyle.container}>
                    {/* ********************************************UPCOMING EVENTS************************************** */}
                    <View onLayout={(e) => this.onLayout(e)}
                        style={{ flex: 1 }} >
                        <ImageBackground
                            style={{ height: 300, flex: 1 }}
                            source={require('../../assets/event1.png')}
                        >
                            <TouchableOpacity
                                onPress={() => this.props.navigation.goBack()}
                                style={{ padding: 15 }}>
                                <Image
                                    style={{ height: 20, width: 20, tintColor: 'white', resizeMode: 'contain' }}
                                    source={require('../../assets/back.png')}
                                />
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>


                    {/* {/********************************** * BOTTOM COMPONENT **********************************} */}

                    <View style={{
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        backgroundColor: Colors.backgroundColor,
                        flex: 1.9,
                        marginTop: 15,
                        paddingHorizontal: 20,
                        paddingVertical: 15,
                        // backgroundColor: 'lightgreen'

                    }}>
                        <ScrollView showsVerticalScrollIndicator={false} >
                            <Text style={[myStyle.textBold, { fontSize: 13, marginTop: 55 }]}>{'About'} </Text>

                            <Text style={[myStyle.textNormal, { fontSize: 13, marginTop: 3, color: Colors.grayColor }]}>
                                {'eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non p'}
                            </Text>

                            <Text style={[myStyle.textBold, { fontSize: 13, marginTop: 30 }]}>{'Host'} </Text>

                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
                                <Image
                                    source={require('../../assets/teacher3.png')}
                                    style={{ height: 50, width: 50 }}
                                />
                                <Text style={[myStyle.textBold, { fontSize: 13, marginLeft: 10, textAlign: 'center' }]}>{'Larry page'} </Text>
                            </View>

                            <Text style={[myStyle.textBold, { fontSize: 13, marginTop: 22 }]}>{`Who's going?`} </Text>
                            <ScrollView horizontal style={{ marginTop: 15 }}>
                                {row}
                            </ScrollView>

                            {/* *******************************************BUTTON*************************************** */}

                            <View style={{ flexDirection: 'row', marginTop: 40, marginBottom: 100 }}>

                                <TouchableOpacity style={[this.state.is_going ? styles.btn : styles.active_btn]}
                                    onPress={() => { this.onPressGoing() }}>
                                    <Text style={[myStyle.textNormal, { color: this.state.is_going ? 'white' : Colors.blue, fontSize: 14 }]}>{"Going"}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={[this.state.mayBe ? styles.btn : styles.active_btn]}
                                    onPress={() => { this.onPressMayBe() }}>
                                    <Text style={[myStyle.textNormal, { color: this.state.mayBe ? 'white' : Colors.blue, fontSize: 14 }]}> {"Maybe"}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={[this.state.notGoing ? styles.btn : styles.active_btn]}
                                    onPress={() => { this.onPressNotGoing() }}>
                                    <Text style={[myStyle.textNormal, { color: this.state.notGoing ? 'white' : Colors.blue, fontSize: 14 }]}>{"Not going"}</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>



                    {/* ******************************************** ABSOLUTE UI ******************************************** */}
                    {/* event top component */}
                    <View
                        onLayout={(e) => this.onLayoutApsolute(e)}
                        style={{
                            backgroundColor: Colors.whiteText,
                            position: 'absolute',
                            elevation: 6,
                            width: "85%",
                            alignSelf: 'center',
                            top: this.state.height - this.state.apsoluteHeight / 3,
                            borderRadius: 20,
                            paddingHorizontal: 10,
                            paddingVertical: 8
                        }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            <View style={{ paddingHorizontal: 14, paddingVertical: 8, backgroundColor: Colors.blue, borderRadius: 23 }}>
                                <Text style={[myStyle.textBold, { fontSize: 16, color: Colors.whiteText }]}>{this.props.eventDetails.event_month} </Text>
                                <Text style={[myStyle.textBold, { fontSize: 20, textAlign: 'center', color: Colors.whiteText }]}>{this.props.eventDetails.event_date} </Text>
                            </View>

                            <View style={{ paddingLeft: 13, marginLeft: 7 }} >
                                <Text style={[myStyle.textBold, { fontSize: 15, color: Colors.textDarkColor, marginLeft: 2, }]}> {this.props.eventDetails.event_name} </Text>
                                <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12, marginVertical: 3, marginLeft: 2, alignSelf: 'baseline', paddingHorizontal: 5 }]}> {this.props.eventDetails.event_timing} </Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                                    <Image
                                        style={{ height: 12, width: 12, resizeMode: 'contain', marginLeft: 7 }}
                                        source={require('../../assets/location_red.png')}
                                    />
                                    <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]}> {this.props.eventDetails.event_address} </Text>
                                </View>
                            </View>
                        </View>
                    </View>


                </View>
            </SafeAreaView>

        )
    }
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    btn: {
        height: 50,
        backgroundColor: Colors.blue,
        borderRadius: 18,
        paddingVertical: 12,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        flex: 1
    },

    active_btn: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.whiteText,
        borderRadius: 18,
        alignSelf: 'center',
        paddingVertical: 12,
        borderWidth: 1,
        borderColor: Colors.blue,
        marginRight: 10,
        flex: 1

    },
});

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    const jaswant = state.indexReducerJaswant;

    return {

        eventDetails: akshita.eventDetails
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

        setUpcomingEventList: (eventList) => setUpcomingEventList(eventList)
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EventInfo)
