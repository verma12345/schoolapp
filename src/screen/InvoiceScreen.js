import React, { Component } from "react";
import { FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import RNFS from 'react-native-fs';
import Share from 'react-native-share';
import ViewShot from 'react-native-view-shot';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import Header from "../components/Header";

class InvoiceScreen extends Component {
    constructor(props) {
        super(props)

    }

    // to display invoice table content
    renderItem = (dataItem) => {
        return (
            <View style={{
                flexDirection: 'row', paddingLeft: 10, flex: 1, justifyContent: 'center',
                width: '95%',
                backgroundColor: Colors.whiteText
            }}>

                <View style={{ borderColor: 'lightgray', flex: 4, justifyContent: 'center', }}>
                    <Text style={[myStyle.textNormal, { fontSize: 13, padding: 5 }]}>
                        {dataItem.item.fee_head}
                    </Text>
                </View>
                <View style={{ borderColor: 'lightgray', flex: 1, justifyContent: 'center', }}>
                    <Text style={[myStyle.textNormal, { fontWeight: '700', fontSize: 13 }]}>
                        {dataItem.item.amount}
                    </Text>
                </View>
            </View>
        )
    }

    // to display invoice table heading
    listHeaderComponent = () => {
        return (
            <View style={{
                flexDirection: 'row', flex: 1, justifyContent: 'center', paddingLeft: 10,
                width: '95%',
                height: 40, backgroundColor: Colors.whiteText
            }}>


                <View style={{ flex: 4, justifyContent: 'center', padding: 5, backgroundColor: Colors.whiteText }}>
                    <Text style={[myStyle.textBold, { fontSize: 14, }]}>
                        {'Particulars'}
                    </Text>
                </View>
                <View style={{ flex: 1, justifyContent: 'center', backgroundColor: Colors.whiteText, }}>
                    <Text style={[myStyle.textBold, { fontSize: 14, }]}>
                        {'Amount'}
                    </Text>
                </View>
            </View>
        )
    }

    //invoice table bottom component
    listFooterComponent = () => {
        return (
            <View
                style={{
                    // width: '100%',
                    // alignSelf: 'center',
                    flex: 1,
                    marginTop: 30
                }}>

                <View style={styles.rowOption}>

                    <View style={{ flex: 4, justifyContent: 'center' }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Total Paid'} </Text>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center' }}>
                        <Text style={[myStyle.textNormal, { fontSize: 14, fontWeight: '700' }]}>   {this.props.feeReceiptDetailsList.total_paid} </Text>
                    </View>
                </View>

                {this.props.feeReceiptDetailsList.discount_amount != 0 ?
                    <View style={styles.rowOption}>
                        <View style={{ flex: 4, justifyContent: 'center' }}>
                            <Text style={[myStyle.textBold, { fontSize: 14, fontWeight: '700' }]}>   {'Discount'} </Text>
                        </View>

                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.greenColor, fontWeight: '700' }]}>   {this.props.feeReceiptDetailsList.discount_amount} </Text>
                        </View>
                    </View>
                    : null}


                <View style={styles.rowOption}>
                    <View style={{ flex: 4, justifyContent: 'center' }}>
                        <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Tax'} </Text>
                    </View>

                    <View style={{ flex: 2, justifyContent: 'center' }}>
                        <Text style={[myStyle.textNormal, { fontSize: 14, fontWeight: '700' }]}>   {'280'} </Text>
                    </View>
                </View>

                {this.props.feeReceiptDetailsList.balance_amount != 0 ?
                    <View style={styles.rowOption}>
                        <View style={{ flex: 4, justifyContent: 'center' }}>
                            <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Balance Amount'} </Text>
                        </View>

                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={[myStyle.textNormal, { fontSize: 16, fontWeight: '700' }]}>   {this.props.feeReceiptDetailsList.balance_amount} </Text>
                        </View>
                    </View>
                    : null}

                {this.props.feeReceiptDetailsList.fee_amount != 0 ?
                    <View style={styles.rowOption}>
                        <View style={{ flex: 4, justifyContent: 'center' }}>
                            <Text style={[myStyle.textBold, { fontSize: 14 }]}>   {'Total Fee Amount'} </Text>
                        </View>

                        <View style={{ flex: 2, justifyContent: 'center' }}>
                            <Text style={[myStyle.textNormal, { fontSize: 16, fontWeight: '700' }]}>   {this.props.feeReceiptDetailsList.fee_amount} </Text>
                        </View>
                    </View>
                    : null}

            </View>
        )
    }


    onBack = () => {
        this.props.navigation.goBack()
    }

    //to capture screenshot of invoice
    captureAndShareScreenshot = () => {
        this.refs.viewShot.capture().then((uri) => {
            RNFS.readFile(uri, 'base64').then((res) => {
                let urlString = 'data:image/jpeg;base64,' + res;

                let options = {
                    title: 'Share Title',
                    message: 'Share Message',
                    url: urlString,
                    type: 'image/jpeg',
                };
                Share.open(options)
                    .then((res) => {
                        console.log(res);
                    })
                    .catch((err) => {
                        err && console.log(err);
                    });
            });
        });
    };

    render() {
        debugLog(this.props.feeReceiptDetailsList)
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.container}>

                    {/* header */}
                    <View style={{ backgroundColor: Colors.whiteText, elevation: 1, paddingBottom: 10, borderBottomRightRadius: 30, borderBottomLeftRadius: 30 }}>
                        <Header
                            title="Invoice"
                            onPressBack={() => this.onBack()}
                            style={{ backgroundColor: Colors.whiteText }}
                        />




                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 40 }} >
                            <Image
                                style={{ height: 65, width: 65 }}
                                source={require('../../assets/college.jpeg')}
                            />
                            <View style={{ paddingHorizontal: 20, paddingVertical: 10 }}>
                                <Text style={[myStyle.textBold, { fontSize: 12 }]}>{this.props.students_details.student_name} </Text>
                                <Text style={[myStyle.textBold, { fontSize: 12 }]}>{'Dues on 28-July-21'} </Text>
                                <Text style={[myStyle.textBold, { fontSize: 12 }]}>{'From Month  ' + this.props.feeReceiptDetailsList.month_from} </Text>
                                <Text style={[myStyle.textBold, { fontSize: 12 }]}>{'To Month  ' + this.props.feeReceiptDetailsList.month_to} </Text>

                            </View>
                        </View>

                        <TouchableOpacity
                            onPress={this.captureAndShareScreenshot}
                            style={{ height: 40, width: 40, tintColor: 'gray', position: 'absolute', right: 20, top: 50 }}
                        >
                            <Image
                                style={{ height: 45, tintColor: 'gray', width: 45, }}
                                source={require('../../assets/share1.png')}
                            />
                        </TouchableOpacity>

                    </View>

                    <ScrollView showsVerticalScrollIndicator={false} >
                        <ViewShot style={{
                            marginHorizontal: 20,
                            paddingHorizontal: 10,
                            marginTop: 20,
                            // flex: 1,
                            backgroundColor: Colors.whiteText,
                            elevation: 3,
                            borderRadius: 20,
                            marginBottom: 80,
                            paddingVertical: 5
                        }}
                            ref="viewShot"
                            options={{ format: 'jpg', quality: 0.9 }}
                        >

                            <FlatList
                                // contentContainerStyle={{ paddingBottom: 10 }}
                                showsVerticalScrollIndicator={false}
                                data={this.props.feeReceiptDetailsList.fee_detail}
                                renderItem={this.renderItem}
                                keyExtractor={(item, index) => 'key' + index}
                                ListHeaderComponent={this.listHeaderComponent}
                                ListFooterComponent={this.listFooterComponent}
                                showsVerticalScrollIndicator={false}
                            />

                        </ViewShot>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    rowOption: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: Colors.whiteText,
        borderTopWidth: 1,
        borderColor: Colors.lightGray
    }

})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;


    return {
        students_details: common.students_details,
        feeReceiptDetailsList: akshita.feeReceiptDetailsList,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({


    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceScreen)