import React, { Component } from "react"
import { Image, StyleSheet, Text, View, SafeAreaView, ScrollView, TouchableOpacity, FlatList, Pressable, ToastAndroid } from "react-native";
import Colors from "../common/Colors";
import { ReportRow } from "../components/ReportRow";
import { myStyle } from "../common/myStyle";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ReportCardHeader } from "../components/ReportCardHeader";
import { setUpdateClassTestSubjects } from "../redux_store/actions/indexActionsJaswant";


class ClassTestScreen1 extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isPopUpMenu: false,
            x: 0,
            y: 0,
            marks_list: [],
            marks_list_filter: []
        }
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }


    // to display report table content
    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    // alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                    paddingLeft: 15,
                    borderTopWidth: 1,
                    borderTopColor: Colors.lightGray,
                    borderBottomRightRadius: dataItem.index == this.props.class_test_data.length - 1 ? 20 : 0,
                    borderBottomLeftRadius: dataItem.index == this.props.class_test_data.length - 1 ? 20 : 0,
                }}
            >

                <View style={{
                    flex: 1,
                    borderBottomLeftRadius: dataItem.index == this.props.class_test_data.length - 1 ? 20 : 0,
                    justifyContent: 'center', paddingVertical: 8.5,
                }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.textColor, }]}>
                        {dataItem.item.exam_date1}
                    </Text>
                </View>

                <View style={{ flex: 1.5, justifyContent: 'center', paddingVertical: 8.5, }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.blue, }]}>
                        {dataItem.item.subject}
                    </Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 8.5, }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.textColor, }]}>
                        {dataItem.item.obtain_marks}
                    </Text>
                </View>

                <View style={{
                    flex: 1.2,
                    borderBottomRightRadius: dataItem.index == this.props.class_test_data.length - 1 ? 20 : 0,
                    justifyContent: 'center', backgroundColor: '#F0F9EE', alignItems: "center", paddingHorizontal: 10
                }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12, textAlign: 'center', color: Colors.textColor, }]}>
                        {dataItem.item.grade}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }


    // to display report table heading
    _renderHeader = () => {
        return (

            <View style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                // alignItems: 'center',
                backgroundColor: Colors.whiteText,
                paddingLeft: 15,
                borderTopLeftRadius: 20,
                borderTopRightRadius: 20
            }}>


                <View style={{ flex: 1, justifyContent: 'center', borderTopLeftRadius: 20, paddingVertical: 12, }} >
                    <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                        {'Date'}
                    </Text>
                </View>

                <View style={{ flex: 1.5, justifyContent: 'center', paddingVertical: 12, }} >
                    <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                        {'Subject'}
                    </Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', paddingVertical: 12, }} >
                    <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                        {'Total'}
                    </Text>
                </View>

                <View style={{ flex: 1.2, justifyContent: 'center', backgroundColor: '#F0F9EE', alignItems: "center", borderTopRightRadius: 20, paddingHorizontal: 10 }} >
                    <Text style={[myStyle.textBold, { fontSize: 12, }]}>
                        {'Mark/Grade'}
                    </Text>
                </View>

            </View>
        )
    }

    // top header icon
    onPressMenu = (event) => {
        this.setState({
            isPopUpMenu: !this.state.isPopUpMenu,
            x: event.layout.x,
            y: event.layout.y
        })
    }

    onPressClassTest = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }

    onPressQuarterly = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }

    onPressHalfYearly = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }

    onPressAnnually = (event) => {
        this.setState({ isPopUpMenu: !this.state.isPopUpMenu })
        // this.props.navigation.navigate('FeeScreenMemo')

    }



    componentDidMount() {
        this.setState({
            marks_list: this.props.class_test_data.marks,
            marks_list_filter: this.props.class_test_data.marks,

        })
    }

    onSubject = (subject) => {
        console.log(subject);
        let newData = []

        newData = this.state.marks_list.filter((item) => {
            return item.subject.includes(subject)
        })

        if (newData.length == 0) {
            newData = this.props.class_test_data.marks.filter((item) => {
                return item.subject.includes(subject)
            })
            this.setState({
                marks_list: newData
            })
        } else {
            this.setState({
                marks_list: newData
            })
        }
    }

    onAll = () => {
        this.setState({
            marks_list: this.props.class_test_data.marks,
        })
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View
                    style={styles.container}
                >
                    <ReportCardHeader
                        onPressBack={this.onPressBack}
                        icon1={require('../../assets/profile.png')}
                        txt="Report Card"
                        icon2={require('../../assets/filter1.png')}
                        isMenu={true}
                        onPressMenu={this.onPressMenu}
                        onSubject={this.onSubject}
                        onAll={this.onAll}
                        year="2021-2022"
                        exam_name={null}
                        data={this.props.class_test_data.subject}
                    />
                    <ScrollView >

                        {/* to display student grade, percent and rank */}
                        <ReportRow
                            rank={this.props.class_test_data.rank}
                            percentage={this.props.class_test_data.marks_percent}
                            grade={this.props.class_test_data.grade}

                            containerStyle={{ marginBottom: 5 }}
                        />

                        {/*Circle Progress */}
                        <View style={{
                            flexDirection: 'row',
                            borderBottomRightRadius: 20,
                            borderBottomLeftRadius: 20,
                            justifyContent: 'space-around',
                            alignItems: 'center',
                            backgroundColor: Colors.whiteText,
                            borderRadius: 15,
                            elevation: 3,
                            marginHorizontal: 25,
                            paddingVertical: 15,
                            marginTop: 10,
                            marginBottom: 15
                        }}>
                            <View>
                                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>
                                    {'Status'}</Text>
                                <Text style={[myStyle.textBold, { fontSize: 14 }]}> {this.props.class_test_data.exam_status}  </Text>
                            </View>

                            <View style={{ width: 1, height: 50, backgroundColor: 'lightgray' }} />

                            <View >
                                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]} >
                                    {'Mark Obtain'}  </Text>
                                <Text style={[myStyle.textBold, { fontSize: 14 }]}> {this.props.class_test_data.totak_marks_obtain}  </Text>
                            </View>

                        </View>


                        {/* Report Card */}

                        <View style={{ borderRadius: 20, marginHorizontal: 25, marginBottom: 100, elevation: 4, backgroundColor: 'white' }}>

                            {
                                this.state.marks_list.length == 0 ?
                                    <Text style={[myStyle.textBold, { fontSize: 22 ,paddingVertical:50,textAlign:'center'}]}> {'Subject Not Found!'}  </Text>

                                    :
                                    <FlatList
                                        data={this.state.marks_list}
                                        renderItem={this._renderItem}
                                        ListHeaderComponent={this._renderHeader}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor={(item, index) => 'key' + index}
                                    />}

                        </View>

                    </ScrollView>

                    <TouchableOpacity style={{
                        backgroundColor: Colors.blue,
                        alignSelf: 'center',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: '80%',
                        position: 'absolute',
                        elevation: 13,
                        bottom: 20,
                        paddingVertical: 15,
                        borderRadius: 18
                    }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, color: 'white' }]} >
                            {'Download PDF'} </Text>

                        <Image
                            source={require('../../assets/pdf.png')}
                            style={{ height: 18, width: 18, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>


                    {this.state.isPopUpMenu ?
                        <View style={{
                            position: 'absolute',
                            backgroundColor: Colors.whiteText,
                            elevation: 20,
                            left: this.state.x - 100,
                            top: this.state.y + 50,
                            borderRadius: 10
                        }}>
                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressClassTest()}
                                style={{ paddingVertical: 12, borderRadius: 10, paddingHorizontal: 25, backgroundColor: Colors.backgroundColor }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'Class Test'}
                                </Text>
                            </Pressable>

                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressQuarterly()}
                                style={{ paddingVertical: 12, paddingHorizontal: 25 }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'Quarterly'}
                                </Text>
                            </Pressable>

                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressHalfYearly()}
                                style={{ paddingVertical: 12, paddingHorizontal: 25 }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'HalfYearly'}
                                </Text>
                            </Pressable>

                            <Pressable
                                android_ripple={{ color: Colors.blue, borderless: false }}
                                onPress={() => this.onPressAnnually()}
                                style={{ paddingVertical: 12, paddingHorizontal: 25 }}>
                                <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>
                                    {'Annually'}
                                </Text>
                            </Pressable>
                        </View>
                        : null}
                </View>

            </SafeAreaView>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'space-between',
        backgroundColor: Colors.backgroundColor
    },
    txt: {
        fontSize: 34,
        fontWeight: 'bold',
        paddingTop: 40,
        color: Colors.textColor
    }
})

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jas = state.indexReducerJaswant;


    return {
        class_test_data: jas.class_test_data,
        class_test_data_marks: jas.class_test_data_marks
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {

            // setEmptyWeek: () => setEmptyWeek(),
            setUpdateClassTestSubjects: (txt) => setUpdateClassTestSubjects(txt),

        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ClassTestScreen1);