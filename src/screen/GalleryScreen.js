import React, { Component } from "react";
import { View, StyleSheet, SafeAreaView, StatusBar, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback, TouchableOpacity, Text, Image, Dimensions } from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import Header from "../components/Header";
import { setGalleryImageDetails, setGalleryList, setGalleryPager, setSelectedGalleryList } from "../redux_store/actions/indexActionAkshita";
import ViewPagerGallery from "./ViewPagerGallery";

class GalleryScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      category_name: ''
    }
  }



  onPressBack = () => {
    this.props.navigation.goBack()
  }
  onViewAll = (selectedList) => {
    // debugLog(selectedList)
    this.props.setSelectedGalleryList(selectedList),
      this.props.navigation.navigate('SelectedGalleryList')
  }
  onClickImage = (data) => {
    // debugLog(data.item)
    this.props.setGalleryImageDetails(data.item)
    this.props.navigation.navigate('ShowImageScreen')
  }
  
  // for the list of gallery images
  _renderItem = (data) => {
    // if (data.item.category == 'College') {
    //   this.setState({ category_name: data.item.category })
    // }
    return (
      <View style={{ marginHorizontal: 5, marginBottom: 30 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text style={[myStyle.textNormal, { fontSize: 14, marginBottom: 8, marginLeft: 8 }]}>{data.item.category} </Text>
          <TouchableOpacity onPress={() => this.onViewAll(this.props.gallery_list[data.index])}>
            <Text style={[myStyle.textNormal, { fontSize: 14, marginBottom: 8, marginRight: 8, color: Colors.blue }]}>{'View All'}</Text>
          </TouchableOpacity>

        </View>
        <FlatList
          data={this.props.gallery_list[data.index].images}
          renderItem={this._renderItem2}
          keyExtractor={(item, index) => 'key' + index}
          horizontal
          showsHorizontalScrollIndicator={false}

        />

      </View>
    )
  }


// show selected gallery image
  _renderItem2 = (data) => {
    return (
      <View style={{ width: data.item.title == 'College' ? 200 : 150, marginRight: 10, borderRadius: 20, alignItems: 'center' }}>

        <TouchableOpacity onPress={() => this.onClickImage(data)}>
          <Image
            style={{ height: 120, width: data.item.title == 'College' ? 200 : 150, borderRadius: 20, resizeMode: 'stretch' }}
            // style={{resizeMode:'cover',height:200,width:300}}
            source={data.item.image}
          />

        </TouchableOpacity>
      </View>
    )
  }
  _renderHeader = (data) => {
    return (
      <View style={{ height: 200, }} >
        <ViewPagerGallery />
      </View>

    )
  }
  render() {


    return (

      <SafeAreaView style={[styles.container, { justifyContent: "space-between" }]} >
        <View style={myStyle.container}>

          <Header
            onPressBack={() => this.onPressBack()}
            title="Gallery"
            style={{
              elevation: 2,
              borderBottomLeftRadius: 20,
              borderBottomRightRadius: 20,
              backgroundColor: Colors.whiteText,
              paddingVertical: 32,
              paddingHorizontal: 15,
            }}
          />
          {/* <ViewPagerGallery /> */}

          <FlatList
            data={this.props.gallery_list}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => 'key' + index}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={this._renderHeader}
          />

        </View>



      </SafeAreaView>

    )
  }
}
const { width, height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor
  },
});


const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const akshita = state.indexReducerAkshita;


  return {
    gallery_list: akshita.gallery_list,
    gallery_pager: akshita.gallery_pager,

    // setGalleryImageDetails: akshita.setGalleryImageDetails

  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setGalleryList: (gallery_list) => setGalleryList(gallery_list),
      setGalleryPager: (pager_list) => setGalleryPager(pager_list),
      setSelectedGalleryList: (list) => setSelectedGalleryList(list),

      setGalleryImageDetails: (set_image) => setGalleryImageDetails(set_image)

    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(GalleryScreen);
