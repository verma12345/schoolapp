
import React, { useEffect } from 'react'
import { Dimensions, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Colors from '../common/Colors'
import { myStyle } from '../common/myStyle'


const FeeSlip = (props) => {

    // go back
    const onBack = () => {
        props.navigation.goBack()
    }

    useEffect(() => {

    }, [])

    const renderHeader = (dataItem) => {
        return (
            <View style={{
                borderTopWidth: 1,
                borderLeftWidth: 2,
                borderRightWidth: 2,
                marginHorizontal: 20,
                marginTop: 10
            }} >

                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, justifyContent: 'space-between' }} >
                    <Text style={[myStyle.textBold, { fontSize: 14 }]} >{'Particulars '} </Text>
                    <Text style={[myStyle.textBold, { fontSize: 14 }]} >{'Amount (in Rs.)'} </Text>
                </View>

            </View>
        )
    }
    const renderItem = (dataItem) => {
        return (
            <View style={{
                borderTopWidth: 0.5,
                borderLeftWidth: 2,
                borderRightWidth: 2,
                borderBottomWidth: dataItem.index == props.feeReceiptDetailsList.fee_detail.length - 1 ? 1 : 0,
                marginHorizontal: 20,
            }} >

                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, borderTopWidth: 1, justifyContent: 'space-between' }} >
                    <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{dataItem.item.fee_head} </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12, marginRight: 20 }]} >{dataItem.item.amount} </Text>
                </View>

            </View>
        )
    }

    const renderFooter = (dataItem) => {
        return (
            <View style={{
                marginRight: 20,
                borderBottomWidth: 1,
                borderLeftWidth: 1,
                borderRightWidth: 2,
                marginLeft: "35%",
            }} >

                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, borderTopWidth: 1, justifyContent: 'space-between' }} >
                    <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Total Paid '} </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12, marginRight: 20 }]} >{props.feeReceiptDetailsList.total_paid} </Text>
                </View>

                {props.feeReceiptDetailsList.discount_amount != 0 ?
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, borderTopWidth: 1, justifyContent: 'space-between' }} >
                        <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Discount '} </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, marginRight: 20 }]} >{props.feeReceiptDetailsList.discount_amount} </Text>
                    </View>
                    : null
                }

                {props.feeReceiptDetailsList.balance_amount != 0 ?
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, borderTopWidth: 1, justifyContent: 'space-between' }} >
                        <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Balance Amount '} </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, marginRight: 20 }]} >{props.feeReceiptDetailsList.balance_amount} </Text>
                    </View>
                    : null
                }

                {props.feeReceiptDetailsList.fee_amount != 0 ?
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10, borderTopWidth: 1, justifyContent: 'space-between' }} >
                        <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Tution Fee '} </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, marginRight: 20 }]} >{props.feeReceiptDetailsList.fee_amount} </Text>
                    </View>
                    : null
                }


            </View>
        )
    }
    // main component
    return (
        <SafeAreaView style={{ backgroundColor: Colors.backgroundColor, flex: 1, }}>
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.header}
                    onPress={() => onBack()}
                >
                    <Image
                        source={require('../../assets/back.png')}
                        style={{ height: 20, width: 20, resizeMode: 'contain' }}
                    />
                </TouchableOpacity>

                <View style={{ flex: 1, backgroundColor: "white" }} >
                    {/*college Heade */}
                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 20 }} >
                        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                            <Image
                                source={require('../../assets/college.jpeg')}
                                style={{ height: 80, width: 80 }}
                            />
                        </View>
                        <View style={{ flex: 1.3, paddingRight: 20 }} >

                            <Text style={[myStyle.textBold, { fontSize: 18 }]} >{props.students_details.school_name} </Text>
                            <Text style={[myStyle.textBold, { fontSize: 14 }]} >{props.students_details.school_address} </Text>

                        </View>
                    </View>

                    <Text style={[myStyle.textBold, { paddingTop: 10, fontSize: 16, textAlign: 'center', textDecorationLine: 'underline' }]} >{'EEE BILL CUM RECEIPT'} </Text>


                    {/* Slip details */}

                    <View style={{ alignSelf: 'center', paddingTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 30, width: "90%" }} >
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Slip No.'} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{props.feeReceiptDetailsList.fee_txn} </Text>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Date '} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{props.feeReceiptDetailsList.reciept_date} </Text>
                        </View>
                    </View>

                    <View style={{ alignSelf: 'center', paddingTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 30, width: "90%" }} >
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Reg No. '} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{'ST876'} </Text>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Class/Sec '} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{props.students_details.class} {props.students_details.section} </Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20, paddingTop: 10 }} >
                        <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Name '} </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{props.students_details.student_name} </Text>
                    </View>


                    <View style={{ alignSelf: 'center', paddingTop: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', height: 30, width: "90%" }} >
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'From Month '} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{props.feeReceiptDetailsList.month_from} </Text>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }} >
                            <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'To Month '} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{props.feeReceiptDetailsList.month_to} </Text>
                        </View>
                    </View>


                    <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 20, paddingTop: 10 }} >
                        <Text style={[myStyle.textBold, { fontSize: 12 }]} >{'Session '} </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12 }]} >{'21-22'} </Text>
                    </View>


                    <FlatList
                        // contentContainerStyle={{ paddingBottom: 20 }}
                        data={props.feeReceiptDetailsList.fee_detail}
                        keyExtractor={(item, index) => index + 'key'}
                        renderItem={renderItem}
                        ListHeaderComponent={renderHeader}
                        ListFooterComponent={renderFooter}
                        showsVerticalScrollIndicator={false}
                    />

                    <Text style={[myStyle.textBold, { fontSize: 14, textAlign: 'right', right: 30, paddingBottom: 40 }]} >{'Counter Signature'} </Text>

                </View>

            </View>
        </SafeAreaView>

    )
}
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
    },
    header: {
        height: 50,
        justifyContent: "center",
        paddingHorizontal: 20,
        backgroundColor: 'white',
        elevation: 2
    }
})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;

    return {
        students_details: common.students_details,
        feeReceiptDetailsList: akshita.feeReceiptDetailsList,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
        },
        dispatch,
    );
};

export const FeeSlipMemo = connect(mapStateToProps, mapDispatchToProps)(React.memo(FeeSlip));