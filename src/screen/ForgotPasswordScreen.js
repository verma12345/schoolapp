import React, { Component } from "react";
import { ScrollView, StyleSheet, Text, View, SafeAreaView,TouchableOpacity } from "react-native";
import TextInput from 'react-native-material-textinput'
import Font from "../common/Font";

// import { TouchableOpacity } from "react-native-gesture-handler";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import Colors from "../common/Colors";
import { LoginHeader } from "../components/LoginHeader";
import { setMobile } from "../redux_store/actions/indexActionsJaswant";

class ForgotPasswordScreen extends Component {
    constructor(props) {
        super(props)
    }



    _validation = () => {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (this.props.mobile == '') {
            alert('Enter the Mobile Number')
            return
        }

        if (this.props.mobile.length < 10) {
            alert('Please Type a valid Number')
            return
        }


        this.props.setMobile('')
        this.props.navigation.navigate('ChangePasswordScreen')

    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
               <View style={styles.container}>
                    <ScrollView style={styles.container}>
                        <View style={{ paddingHorizontal: 40, marginBottom: 50 }}>
                            <LoginHeader
                                navigation={this.props.navigation}
                                backpic={require('../../assets/back.png')}
                                txtheading={'Forgot\nPassword?'}

                                txt={"We'll send you an email or varification code\n to reset your password"}
                            />
                            <View style={styles.view1}>
                                <TextInput

                                    label='Enter The Mobile Number / Email'
                                    underlineHeight={1}
                                    underlineColor={Colors.textinputcolor}
                                    labelActiveColor={Colors.placeHolderColor}
                                    underlineActiveColor={Colors.textinputcolor}
                                    maxLength={this.props.mobile.match(/^\d/) ? 10 : 30}
                                    onChangeText={(txt) => {
                                        this.props.setMobile(txt)
                                        // this.props.setShow(txt)
                                    }}
                                    keyboardType='email-address'

                                />

                              
                            </View>
                            {
                                this.props.show !== '' ?
                                    <TouchableOpacity style={styles.btn1}
                                        onPress={() => this._validation()}>
                                        <Text style={[myStyle.textBold, { color: Colors.backgroundColor, fontSize: 14 }]}>{'Reset my Password'}</Text>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity style={styles.btn}
                                        onPress={() => this._validation()}>
                                        <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 14 }]}>{'Reset my Password'}</Text>
                                    </TouchableOpacity>

                            }


                        </View>


                    </ScrollView>
                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:Colors.backgroundColor
    },
    txtinput: {
        height: 60,
        width: '90%',
        // elevation:1,
        borderRadius: 10,
        borderWidth: 0.2,
        justifyContent: 'center',
        paddingLeft: 20
    },
    btn: {
        height: 56,
        width: '100%',
        backgroundColor:Colors.btncolor,

        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 20
    },
    btn1: {
        height: 56,
        width: '100%',
        backgroundColor:Colors.blue,


        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        marginTop: 30
    },
    view1: {
        height: 56,
        width: '100%',
        borderRadius: 20,
        backgroundColor:Colors.textinputcolor,
        paddingLeft: 40,

        marginTop: 60
    },
})

const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jas = state.indexReducerJaswant;

    return {
        mobile: jas.mobile,
        password: jas.password,

    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setMobile: (mobile) => setMobile(mobile),

    }, dispatch)
}






export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordScreen)