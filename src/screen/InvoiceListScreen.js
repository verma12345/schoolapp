import React, { Component } from "react";
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Share from 'react-native-share';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import FeeScreenHeader from "../components/FeeScreenHeader";
import { setFeeReceiptDetailList } from "../redux_store/actions/indexActionAkshita";
import { hitGetFeeReceiptApi } from "../redux_store/actions/indexActionApi";


class InvoiceListScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    onDownload = (item) => {
        // console.log(item);
        let params = {
            "school": {
                "stu_id": 64,
                "fee_txn": "INV - 1",
                "session_year": "21-22"
            }
        }
        this.props.hitGetFeeReceiptApi(params).then(res => {
            debugLog('feeReceipt', res.school)
            this.props.setFeeReceiptDetailList(res.school)
            this.props.navigation.navigate('FeeSlipMemo')

        })
    }

    // for share
    captureAndShareScreenshot = () => {
        let options = {
            title: 'Share Title',
            message: 'Share Message',
            // url: urlString,
            // type: 'image/jpeg',
            type:'simple/txt'
        };
        Share.open(options)
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                err && console.log(err);
            });
    };

    onFeeStructure = () => {
        this.props.navigation.navigate('FeeStructureScreenMemo')
    }

    // to display invoice table content
    renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('InvoiceScreen')}
                style={{ marginHorizontal: 25, backgroundColor: 'white', elevation: 3, borderRadius: 20, marginTop: 12 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 20, paddingVertical: 10 }} >


                    <View style={{ flexDirection: 'row', alignItems: 'center' }} >

                        <View style={{ paddingHorizontal: 10, paddingVertical: 15, marginRight: 15, borderWidth: 1, borderColor: '#219653', borderRadius: 20 }} >
                            <Text style={[myStyle.textBold, { color: '#219653', fontSize: 12, textAlign: 'center' }]} >{dataItem.item.invoice_status} </Text>
                        </View>

                        <View>
                            <Text style={[myStyle.textNormal, { fontSize: 14, }]} >{dataItem.item.invoice_number} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 14, }]}>{dataItem.item.invoice_date} </Text>

                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, }]} >{'Rs'} {dataItem.item.invoice_amount} </Text>
                        <Image
                            source={require('../../assets/next_button.png')}
                            style={{ height: 20, width: 20, resizeMode: 'contain' }}
                        />
                    </View>

                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 20, borderTopWidth: 1, borderTopColor: 'lightgray' }} >

                    <TouchableOpacity
                        onPress={() => this.captureAndShareScreenshot()}
                        style={{ flex: 1, alignItems: 'center', paddingVertical: 8 }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, }]}>{'Share'} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.onDownload(dataItem.item)}
                        style={{ flex: 1, borderLeftWidth: 1, borderColor: 'lightgray', alignItems: 'center', paddingVertical: 8 }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, }]}>{'Download'} </Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }


    onBack = () => {
        this.props.navigation.goBack()
    }

    render() {
        return (
            <SafeAreaView style={styles.container} >
                <View style={{ backgroundColor: Colors.backgroundColor, flex: 1, }}>

                    {/* header */}
                    <FeeScreenHeader
                        onBack={() => this.onBack()}
                        student_profile={{ uri: this.props.students_details.profile_pic }}
                        student_name={this.props.students_details.student_name}
                        school_name={this.props.students_details.school_name}
                        school_address={this.props.students_details.school_address}
                        onFeeStructure={() => this.onFeeStructure()} />


                    <FlatList
                        contentContainerStyle={{ paddingBottom: 80 }}
                        data={this.props.invoiceList}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => 'key' + index}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // padding: 40,
        backgroundColor: Colors.backgroundColor
    },
})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    const jaswant = state.indexReducerJaswant;

    return {
        invoiceList: akshita.invoiceList,
        students_details: common.students_details,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

        hitGetFeeReceiptApi: (param) => hitGetFeeReceiptApi(param),
        setFeeReceiptDetailList: (list) => setFeeReceiptDetailList(list)

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceListScreen)