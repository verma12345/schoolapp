
import React, { useState } from 'react'
import { Dimensions, FlatList, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { Calendar } from 'react-native-calendars'
import ProgressCircle from 'react-native-progress-circle'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Colors from '../common/Colors'
import { debugLog } from '../common/Constants'
import { myStyle } from '../common/myStyle'
import { CalendarHeader } from '../components/CalendarHeader'
import { HolidayRow } from '../row_component/HolidayRow'



const MyCalander = (props) => {
    const [is_attendance, setAttendance] = useState(true)
    const [is_holiday, setHoliday] = useState(false)
    // const [holiday_count, setHolidayCount] = useState(0)
    const [month, setMont] = useState(new Date().getMonth() + 1) // I added +1 because getDate giving -1 value of current month like for december it's giving 11


    let bgColor = 'white'
    const currentDate = new Date()
    const currentMonth = new Date()
    var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    var dayCount = null
    let dayName = null
    let holiday_count = 0
    let attendance_count = 0

    const changeBGColor = (date, state) => {

        if (dayCount == 6) {
            dayCount = 0
        } else {
            dayCount = dayCount + 1
        }
        dayName = days[dayCount];

        if (state !== 'disabled') {
            bgColor = ''
        }
    }

    const onPressBack = () => {
        props.navigation.goBack()
    }

    const onPressAttendance = () => {
        setAttendance(true)
        setHoliday(false)

    }

    const onPressHoliday = () => {
        setHoliday(true)
        setAttendance(false)

    }



    const _renderItem = (dataItem) => {
        debugLog(dataItem.item)
        return (
            <HolidayRow
                data_row={dataItem.item}
            />
        )
    }


    return (

        <View style={[{ borderRadius: 10, backgroundColor: Colors.backgroundColor, flex: 1 }]}>

            <CalendarHeader
                onPressBack={() => onPressBack()}
                onPressAttendance={() => onPressAttendance()}
                onPressHoliday={() => onPressHoliday()}
                is_attendance={is_attendance}
                is_holiday={is_holiday}
            />

            <ScrollView
                contentContainerStyle={{ flex: 2 }}
                showsVerticalScrollIndicator={false} >

                <View style={[{
                    borderRadius: 10, backgroundColor: 'white', elevation: 5, zIndex: 5, marginHorizontal: 20,
                    marginTop: 15, marginBottom: 2
                }]}>
                    <Calendar
                        firstDay={1}
                        style={[{ backgroundColor: 'white', borderRadius: 20 }]}
                        onMonthChange={(month) => { setMont(month.month) }}
                        dayComponent={({ date, state }) => {

                            let holiday_date = { "day": '', "name": "" }
                            let attendance_date = { "day": '', }

                            let holiday = 0
                            let attendance = 0 // in progress

                            if (date.month == month) {

                                holiday = props.holiday_list[date.month - 1].days;
                                attendance = props.attendanceList[date.month - 1].days

                                holiday_date = holiday[holiday_count]
                                attendance.length != 0 ? attendance_date = attendance[attendance_count] : null

                                if (holiday_date.day == date.day && holiday_count <= holiday.length - 2) {
                                    holiday_count = holiday_count + 1
                                }

                                console.log('gggggg', attendance, 'count: ', attendance_count, 'length: ', attendance.length);

                                // if (attendance_date.day == date.day && attendance_count <= attendance.length - 2) {
                                //     //     // console.log(holiday);
                                //     //     // console.log(props.holiday_list[month-1].days);
                                //     //     // console.log(holiday);
                                //     console.log('gggggggggg', attendance[attendance_count]);

                                //     attendance_count = attendance_count + 1
                                // }


                            }



                            let dataRow = []

                            dataRow.push({
                                dateString: date.dateString,
                                day: date.day,
                                month: date.month,
                                timestamp: date.timestamp,
                                year: date.year,
                                events: holiday_date.day == date.day ? holiday_date.name : '',
                                holiday: date.day == '20' ? 1 : 0,
                                present: date.day == '14' ? 0 : 1,
                                leave: date.day == '22' ? 1 : 0,
                                absent: date.day == '1' ? 1 : 0
                            })
                            changeBGColor(dataRow, state)
                            return (
                                <TouchableOpacity
                                    onPress={() => { alert(dataRow[0].events) }}
                                    style={{
                                        // paddingHorizontal: 2,
                                        // borderRadius: 5,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        backgroundColor: dayName == 'Sunday' ? state == 'disabled' ? 'white' : '#D4E2FF' : null,
                                        height: 25,
                                        width: 25,
                                        borderRadius: 30,
                                    }}
                                >
                                    <Text style={[myStyle.textBold, {
                                        textAlign: 'center',
                                        color: state !== 'disabled' ?
                                            dayName != 'Sunday' ?
                                                currentMonth.getMonth() + 1 == dataRow[0].month ?
                                                    dataRow[0].day == currentDate.getDate() ? '#B75656' :
                                                        dataRow[0].absent || dataRow[0].leave === 1 ? '#FF0A0A' : '#28293D' :
                                                    '#28293D' :
                                                Colors.textColor :
                                            'white'
                                    }]}>
                                        {dataRow[0].day}
                                    </Text>

                                    {
                                        dayName != 'Sunday' ?
                                            <View
                                                style={{
                                                    height: 7, width: 7, borderRadius: 30,
                                                    backgroundColor: state !== 'disabled' ?
                                                        dataRow[0].holiday === 1 ? '#F89E18' :
                                                            dataRow[0].absent || dataRow[0].leave === 1 ? '#FF0A0A' :
                                                                '#000D83' :
                                                        'white'
                                                }}
                                            />
                                            : null}

                                </TouchableOpacity>
                            );
                        }}
                        onDayPress={(day) => { console.log(day); }}
                    />


                    <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 30, marginTop: 15 }} >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginRight: 20, paddingBottom: 20 }}  >
                            <View style={{ height: 12, width: 12, borderRadius: 3, backgroundColor: '#F89E18', marginRight: 5, }} />
                            <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]} >{'Holiday'}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginRight: 20, paddingBottom: 20 }}  >
                            <View style={{ height: 12, width: 12, borderRadius: 3, backgroundColor: '#000D83', marginRight: 5 }} />
                            <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]} >{'Present'}</Text>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginRight: 20, paddingBottom: 20 }}  >
                            <View style={{ height: 12, width: 12, borderRadius: 3, backgroundColor: '#FF0A0A', marginRight: 5 }} />
                            <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]} >{'Leave/ Absent'}</Text>
                        </View>
                    </View>

                </View>



                {/**********************Attendance Circle Progress *****************************/}
                {is_attendance ?
                    <View>
                        <Text style={[myStyle.textBold, { marginHorizontal: 20, marginTop: 20, fontSize: 14 }]}>{'Attendance History'} </Text>
                        <View style={{
                            flexDirection: 'row',
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            justifyContent: 'space-around',
                            alignItems: 'center',
                            backgroundColor: Colors.whiteText,
                            elevation: 3,
                            marginHorizontal: 20,
                            paddingTop: 20,
                            paddingBottom: 12,
                            marginTop: 15,
                            paddingHorizontal: 20
                        }}>
                            <View>
                                <ProgressCircle
                                    percent={79}
                                    radius={28}
                                    borderWidth={5}
                                    color={Colors.blue}
                                    shadowColor={Colors.lightGray}
                                    bgColor="#fff"
                                >
                                    <Text style={myStyle.textBold}>{'79%'}</Text>
                                </ProgressCircle>
                                <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor, marginTop: 10 }]}> {'Monthly'}  </Text>
                            </View>

                            <View style={{ height: 40, width: 1, backgroundColor: Colors.lightGray }} />
                            <View>
                                <ProgressCircle
                                    percent={92}
                                    radius={28}
                                    borderWidth={5}
                                    color={Colors.blue}
                                    shadowColor={Colors.lightGray}
                                    bgColor="#fff"
                                >
                                    <Text style={myStyle.textBold}>{'92%'}</Text>
                                </ProgressCircle>
                                <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor, marginTop: 10 }]}> {'Yearly'}  </Text>

                            </View>


                        </View>

                        <View style={{ height: 0.5, width: '90%', marginHorizontal: 15, backgroundColor: Colors.lightGray }} />
                        <View style={{
                            flexDirection: 'row',
                            borderBottomRightRadius: 20,
                            borderBottomLeftRadius: 20,
                            justifyContent: 'space-around',
                            alignItems: 'center',
                            backgroundColor: Colors.whiteText,
                            elevation: 3,
                            marginHorizontal: 20,
                            paddingVertical: 15,
                            marginBottom: 30
                        }}>
                            <View style={{ alignItems: 'center', marginBottom: 5 }} >
                                <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{"Attendance"}</Text>
                                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 15, marginTop: 4 }]}>{'180/365'}</Text>
                            </View>

                            <View style={{ alignItems: 'center', marginBottom: 5 }} >
                                <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{"Holidays"}</Text>
                                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 15, marginTop: 4 }]}>{'5/15'}</Text>
                            </View>
                            <View style={{ alignItems: 'center', marginBottom: 5 }} >
                                <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{"Absent"}</Text>
                                <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 15, marginTop: 4 }]}>{'14'}</Text>
                            </View>

                        </View>
                    </View>

                    :

                    null

                }
                {/* **********************Holidays********************* */}


            </ScrollView>

            {is_holiday ?
                <View style={{ flex: 0.8 }} >
                    <Text style={[myStyle.textBold, { marginHorizontal: 20, marginTop: 15, fontSize: 14 }]}>{'List of Holidays'} </Text>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: 50 }}
                        data={props.holiday_list[month - 1].days}
                        renderItem={_renderItem}
                    />

                </View>
                : null}
        </View>

    )
}
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({

})


const mapStateToProps = (state) => {
    const Jaswant = state.indexReducerJaswant;
    const common = state.indexReducer;


    return {
        holiday_list: Jaswant.holiday_list,
        attendanceList: Jaswant.attendanceList,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            // setHolidayList: (list) => setHolidayList(list),
            // setAtendanceList: (list) => setAtendanceList(list),
            // hitGetAttendanceApi: (params) => hitGetAttendanceApi(params)
        },
        dispatch,
    );
};

export const MyCalanderMemo = connect(mapStateToProps, mapDispatchToProps)(React.memo(MyCalander));