

import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';
import Header from '../components/Header';

class NotificationScreen extends Component {

    // for the notification details
    _renderNotification = (dataItem) => {
        return (
            <View style={myStyle.container} >
                <View style={{ marginVertical: 10, paddingHorizontal: 20 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={[myStyle.textBold, { fontSize: 13 }]}>{dataItem.item.title}</Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, color: 'gray' }]}>{dataItem.item.time_ago}</Text>
                    </View>

                    {/* <Text style={[myStyle.textNormal, { color: 'gray', fontSize: 12 }]}>{dataItem.item.title}</Text> */}
                    <Text style={[myStyle.textBold, { color: 'gray', fontSize: 12 }]}>{dataItem.item.message}</Text>
                </View>
                <View style={{ height: 1, marginVertical: 10, flex: 1, marginLeft: 20, backgroundColor: 'lightgray' }} />
            </View>
        )
    }


    onPressBack = () => {
        this.props.navigation.goBack()
    }

    render() {
       
        return (
            <View style={myStyle.container}>
                <Header
                    onPressBack={() => this.onPressBack()}
                    title="Notification"
                    style={{
                        elevation: 2,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20,
                        backgroundColor: Colors.whiteText,
                        paddingVertical: 14,
                        paddingHorizontal: 15,
                    }}
                />

                <FlatList
                    data={this.props.notification}
                    keyExtractor={(item, index) => 'key' + index}
                    renderItem={this._renderNotification}
                    contentContainerStyle={styles.contentContainer}
                />

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingTop: 200,
    },
    contentContainer: {
        backgroundColor: 'white',
    },
    itemContainer: {
        padding: 6,
        margin: 6,
        backgroundColor: '#eee',
    },
});



const mapStateToProps = (state) => {
    let common = state.indexReducer;
    let jas = state.indexReducerJaswant;

    return {
        notification: jas.notification,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        // setNotification: (notification) => setNotification(notification),
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen)
