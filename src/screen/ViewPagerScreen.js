import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import ViewPagerComponent from "../components/ViewPagerComponent";
import ViewPagerDot from "../components/ViewPagerDot";
import { setPosition } from "../redux_store/actions/indexActionsJaswant";


class ViewPagerScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      pager_list: [
        {
          icon: require('../../assets/pager1.png'),
          heading: 'Heading 1',
          message: "Lorem ipsum dolor sit amet, consectet piscing elit. Nam felis lectus, efficitur at erat vitae, tempor mattis quam."
        },
        {
          icon: require('../../assets/pager2.png'),
          heading: 'Heading 2',
          message: "Lorem ipsum dolor sit amet, consectet piscing elit. Nam felis lectus, efficitur at erat vitae, tempor mattis quam."
        },
        {
          icon: require('../../assets/pager3.png'),
          heading: 'Heading 3',
          message: "Lorem ipsum dolor sit amet, consectet piscing elit. Nam felis lectus, efficitur at erat vitae, tempor mattis quam."
        }
      ],
    }
  }
  // on skip btn
  onSkip = () => {
    this.props.navigation.navigate('LoginHomeScreen')
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>

        <ViewPagerComponent
          onPageSelected={(index) => { this.props.setPosition(index.nativeEvent.position) }}
          pager_list={this.state.pager_list}
        />

        <View style={{ width: "100%", flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', bottom: 10 }} >
          <View style={{ paddingLeft: 30 }} >
            <ViewPagerDot
              list={this.state.pager_list}
              position={this.props.position}
            />
          </View>

          <TouchableOpacity
            onPress={() => this.onSkip()}
            style={{
              width: 70, height: 40,
              // backgroundColor: 'red', 
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 20
            }} >
            <Text style={[myStyle.textBold, { color: '#000D83' }]} >{"Skip"}</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0"
  }
});

const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const akshita = state.indexReducerAkshita;
  const jaswant = state.indexReducerJaswant;


  return {
    position: jaswant.position,


  };
};


const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setPosition: (list) => setPosition(list),
    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewPagerScreen);