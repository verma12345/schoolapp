import React, { Component } from "react";
import { View, StyleSheet, SafeAreaView, StatusBar, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback, TouchableOpacity, ScrollView } from "react-native";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import { setPrefs } from "../common/Prefs";
import BottomNavigation from "../components/BottomNavigation";
import Header from "../components/Header";
import MoreRow from "../components/MoreRow";
import NotificationRow from "../components/NotificationRow";

class ProfileScreen extends Component {
  constructor(props) {
    super(props)
  
  }

// logout btn
  onLogout = () => {
    setPrefs('user_login','')
    setPrefs('mobile','')
    setPrefs('email','')
    this.props.navigation.navigate('LoginScreen')
  }

  onPressBack = ()=>{
    this.props.navigation.goBack()
  }

  render() {
    return (
      <SafeAreaView style={myStyle.container} >

        <Header
          onPressBack={() => this.onPressBack()}
          title="Profile"
          style={{
            elevation: 2,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            backgroundColor: Colors.whiteText,
            paddingVertical: 32,
            paddingHorizontal: 15,
          }}
        />
        <ScrollView contentContainerStyle={{ marginBottom: 50 }}>
          <View style={{ flex: 1 }} >




            <MoreRow
              image={require('../../assets/logout.png')}
              backgroundColor='#DFE6F3'
              title="Logout"
              onPress={() => this.onLogout()}
              mainStyle={{marginBottom:20}}
              iconStyle={{tintColor:"blue"}}
            />

          </View>


        </ScrollView>

        <BottomNavigation
          navigation={this.props.navigation}
          isProfile={true}
        />
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor
  },
});

export default ProfileScreen;
