import { StackActions } from "@react-navigation/routers";
import React, { Component } from "react";
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
// import TextInput from 'react-native-material-textinput'
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import { getPrefs } from "../common/Prefs";
import { VarificationHeader } from "../components/VarificationHeader";
import { setRandomNumOtp } from "../redux_store/actions/indexActionAkshita";
import { setSignupEmail, setSignupMobile } from "../redux_store/actions/indexActions";



class VarificationScreen extends Component {
    constructor(props) {
        super(props)
    }


    componentDidMount() {
        var val = Math.floor(1000 + Math.random() * 9000);
        this.props.setRandomNumOtp(val)
        // console.log(val);
        getPrefs('email').then((value => {
            console.log(value);
            if (value !== null) {
                this.props.setSignupEmail(value)
            }
        }))
    }

    //  fxn for change mobile / email
    _onPressChangeEmailMobile = () => {
        if (this.props.signup_email == '') {
            alert('Please enter email')
            return
        }
        if (this.props.signup_mobile == '') {
            alert('Enter Mobile Number')
            return
        }
        if (this.props.signup_mobile.length < 10) {
            alert('Enter a Valid Number')
            return
        }

        let param = {
            school: {
                email: this.props.signup_email,
                mobile: this.props.signup_mobile,
                otp: this.props.random_num,
            },
        };
        // debugLog(param)
        this.props.hitMobileApi(param).then((response) => {
            let _response = response.school;
            if (_response.status == 1) {
                console.log(_response);
                this.props.setRandomNumOtp(_response.otp)
                // this._setUserInfo(_response)
                this.props.navigation.dispatch(
                    StackActions.replace('OtpScreen'),
                )
            } else {
                alert(_response.msg);
            }
        });
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View >
                    <ScrollView>
                        <View style={{ justifyContent: 'center', alignItems: 'center', paddingTop: 120 }}>
                            <VarificationHeader
                                txtHeading={'Change Mobile / Email'}
                            />
                        </View>
                        {/* <View style={{ flex: 2 }}> */}

                        <View style={styles.view5}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '35%', alignItems: 'center' }}>
                                <Image
                                    style={{ height: 24, width: 32 }}
                                    source={require('../../assets/india.png')}
                                />
                                <Text style={{ color: Colors.signuptextcolor, fontSize: 18 }}>+91</Text>
                                <Image
                                    style={{ height: 5.83, width: 11.67 }}
                                    source={require('../../assets/downaro.png')}
                                />
                            </View>
                            <TextInput
                                style={{ marginLeft: 10 }}
                                placeholder={'Enter mobile number'}
                                value={this.props.signup_mobile}
                                onChangeText={(txtmobile) => {
                                    this.props.setSignupMobile(txtmobile)
                                }}
                                keyboardType='phone-pad'
                                maxLength={10}
                                autoFocus={true}
                                placeholderTextColor={Colors.placeHolderColor}

                            />
                        </View>

                        <View style={styles.view5}>
                            <TextInput
                                width={200}
                                label='Email Address'
                                value={this.props.email}
                                underlineHeight={1}
                                underlineColor={Colors.backgroundColor}
                                labelActiveColor={Colors.placeHolderColor}
                                underlineActiveColor={Colors.textinputcolor}
                                onChangeText={(email) => {
                                    this.props.setSignupEmail(email)
                                }}
                                keyboardType='email-address'
                            />
                        </View>
                        <View style={{ marginTop: 40, paddingHorizontal: 10, marginBottom: 50 }}>
                            <TouchableOpacity style={styles.btn}
                                onPress={() => {
                                    // this._onPressChangeEmailMobile()
                                }}>
                                <Text style={[myStyle.textBold, { color: Colors.whiteText, }]}>{"Change Mobile OR Email"}</Text>
                            </TouchableOpacity>
                        </View>
                        {/* </View> */}
                    </ScrollView>


                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginBottom: 50,
        backgroundColor: Colors.backgroundColor,
        paddingHorizontal: 40,

    },
    view5: {
        height: 56,
        width: '100%',
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,
        marginTop: 130,
        // paddingLeft: 20,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        // paddingRight: 20,
        alignItems: 'center',
        elevation: 1,
        paddingHorizontal: 20,


    },
    btn: {
        height: 56,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,
        paddingHorizontal: 10,
        borderRadius: 20,
    },
    view5: {
        height: 56,
        width: '100%',
        borderRadius: 20,
        backgroundColor: Colors.textinputcolor,
        marginTop: 15,
        paddingHorizontal: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        elevation: 1

    },
})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita
    return {
        signup_mobile: common.signup_mobile,
        random_num: akshita.random_num,
        signup_email: common.signup_email

    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setSignupEmail: (email) => setSignupEmail(email),
        setSignupMobile: (mobile) => setSignupMobile(mobile),
        setRandomNumOtp: (random_num) => setRandomNumOtp(random_num),
        hitMobileApi: (params) => hitMobileApi(params),


    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(VarificationScreen)