import React, { Component } from "react";
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import { TeacherHeader } from "../components/TeacherHeader";
import { searchStudent, setAddStudentList, setAddUpdateStudentList, setUpdateAddStudentList } from "../redux_store/actions/indexActionAkshita";
import { hitAddStudentApi, hitGetStudentListApi } from "../redux_store/actions/indexActionApi";
import { setLoading } from "../redux_store/actions/indexActions";
class AddStudentScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            student_ids: [],
        }
    }

    // remove duplicate ustudent id from selected students
    removeDuplicates() {
        var newArray = [];
        var lookupObject = {};
        console.log(this.state.list.length,'bbbbbbbbb');
        for (var i in this.state.list) {
            lookupObject[this.state.list[i]['student_id']] = this.state.list[i];
        }
        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    }

// hit api add_student
    onAddStudent = () => {

        let lfilterdList = this.removeDuplicates()

        const dataArrWithSet = new Set(this.state.student_ids);
        const resultArr = [...dataArrWithSet];

        if (this.state.student_ids.length == 0) {
            alert('Please select student')
            return
        }

        let params = {
            "school": {
                user_id: this.props.user_id,
                student_ids: resultArr
            }
        }

        debugLog(params)
        this.props.hitAddStudentApi(params).then(res => {
            if (res.school.status == 1) {
                this.props.setAddUpdateStudentList(lfilterdList)

                this.props.navigation.goBack()
            }
        })
    }

    // for back to screen
    onPressBack = () => {
        this.props.navigation.goBack()
    }

    // add selected student in loacal app
    addToStudentList = (data, index) => {
        let newData = [data, ...this.state.list]
        let ids = [...this.state.student_ids, data.student_id]
        this.setState({ list: newData, student_ids: ids })
        this.props.setUpdateAddStudentList(index)
    }

    // hit get_student_list first time when screen open
    componentDidMount() {
        this.setState({ list: this.props.students_list != undefined ? this.props.students_list : [] })
        let params = {
            school: {
                school_id: 1,
            }
        }

        this.props.hitGetStudentListApi(params).then(res => {
            if (res.school.status == 1) {
                this.props.setAddStudentList(res.school.student_list)
            }
        })
    }

// list row for flat list
    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                style={[myStyle.myshadow2, {
                    flexDirection: 'row',
                    marginHorizontal: 14,
                    flex: 1,
                    marginVertical: 7,
                    paddingHorizontal: 10,
                    paddingVertical: 7,
                    borderRadius: 20,
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                }]} >
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                }}>
                    <Image
                        style={{ height: 55, width: 55, resizeMode: 'contain' }}
                        source={{ uri: dataItem.item.profile_pic }}
                    />
                    <View style={{ paddingHorizontal: 15 }} >
                        <Text style={[myStyle.textBold, { fontSize: 14 }]} >
                            {dataItem.item.student_name}
                        </Text>
                        <Text style={[myStyle.textNormal, { color: Colors.textColor, fontSize: 12 }]} >
                            {"class:" + dataItem.item.class}
                        </Text>
                        <Text style={[myStyle.textNormal, { color: Colors.textColor, fontSize: 12 }]} >
                            {"section:" + dataItem.item.section}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    onPress={() => { this.addToStudentList(dataItem.item, dataItem.index) }}
                    style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                    <Image style={{ height: 30, width: 30 }}
                        source={dataItem.item.is_selected ? require('../../assets/paid.png') : require('../../assets/plus.png')}
                    />
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }

// main component
    render() {
        return (
            <SafeAreaView style={myStyle.container}>
                <View style={[myStyle.container]}>
                    <TeacherHeader onPressBack={this.onPressBack}
                        teacher_pic={require('../../assets/profile.png')}
                        txt="Add Student"
                    />
                    <View style={styles.searchBar} >
                        <TextInput
                            placeholder='Search...'
                            onChangeText={(txt) => this.props.searchStudent(txt)}
                            style={{ flex: 1, backgroundColor: "white", borderRightWidth: 1, marginRight: 5, borderRightColor: Colors.placeHolderColor }}
                        />
                        <Image
                            source={require("../../assets/filter.png")}
                            style={{ height: 25, width: 25, resizeMode: "contain" }}
                        />
                    </View>
                    <View style={{ flex: 1 }} >
                        <FlatList
                            contentContainerStyle={{ paddingBottom: 100 }}
                            data={this.props.addStudentList}
                            renderItem={this._renderItem}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => index + 'key'}
                        />
                    </View>
                    <TouchableOpacity
                        onPress={() => this.onAddStudent()}
                        style={{ position: 'absolute', bottom: 20, borderRadius: 10, padding: 10, right: 20, elevation: 5, backgroundColor: Colors.blue }}>
                        <Text style={[myStyle.textNormal, { color: Colors.whiteText }]} >
                            {'Save'}
                        </Text>
                    </TouchableOpacity>
                </View >


                {this.props.is_loading ? (
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#0008',
                            elevation: 10,
                            position: 'absolute',
                        }}>
                        <View style={{ borderRadius: 10, backgroundColor: "white", height: 200, width: 300, justifyContent: "center", alignItems: "center", }}>
                            <ActivityIndicator
                                size={"large"}
                                color={Colors.blue}
                            />
                            <Text style={{ fontSize: 14 }} >{"Please wait for a while..."} </Text>
                        </View>
                    </View>
                ) : null}
            </SafeAreaView >
        )
    }
}

const styles = StyleSheet.create({
    searchBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: "white",
        elevation: 5,
        margin: 10,
        padding: 5,
        borderWidth: 1,
        borderColor: Colors.placeHolderColor,
        borderRadius: 10,
        height: 50
    }
});
const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    return {
        user_id: common.user_id,
        is_loading: common.is_loading,
        student_details: akshita.student_details,
        students_list: akshita.students_list,
        addStudentList: akshita.addStudentList,
        searchStudentList:akshita.searchStudentList
      
    };
};
const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setLoading: (isTrue) => setLoading(isTrue),
            setAddStudentList: (addStudent) => setAddStudentList(addStudent),
            setAddUpdateStudentList: (add) => setAddUpdateStudentList(add),
            setUpdateAddStudentList: (index) => setUpdateAddStudentList(index),
            searchStudent: (txt) => searchStudent(txt),

            hitGetStudentListApi: (params) => hitGetStudentListApi(params),
            hitAddStudentApi: (params) => hitAddStudentApi(params)
        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(AddStudentScreen);