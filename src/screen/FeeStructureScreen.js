
import React, { useEffect, useState } from 'react'
import { Dimensions, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Colors from '../common/Colors'
import { myStyle } from '../common/myStyle'
import FeeBillPopUp from '../components/FeeBillPopUp'
import { TeacherHeader } from '../components/TeacherHeader'
import { setFeeStructureList } from '../redux_store/actions/indexActionAkshita'
import { hitGetFeeStructureApi } from '../redux_store/actions/indexActionApi'


const FeeStructureScreen = (props) => {
    const [monthlyBill, setMonthlyBill] = useState([])
    const [isBill, setIsBill] = useState(false)


    const onPressBack = () => {
        props.navigation.goBack()
    }

    // fetch fee structure api
    useEffect(() => {
        let params = {
            "school": {
                "class": 1,
                "session_year": "21-22"
            }
        }
        props.hitGetFeeStructureApi(params).then(res => {
            props.setFeeStructureList(res.school.fee_months)
        })
    }, [])

    // get the fee month and fee amount list
    const renderItem = (data) => {
        return (
            <View style={{
                height: 40, flexDirection: 'row',
            }} >
                <TouchableOpacity
                    style={{
                        backgroundColor: '#F8FAFF', flex: 1, justifyContent: 'center', alignItems: 'center',
                        marginTop: 1,
                        marginRight: 1,
                        borderBottomWidth: 1,
                        borderColor: 'lightgray',
                        borderRightWidth: 1,

                    }} >
                    <Text style={[myStyle.textNormal, { color: '#28293D', fontSize: 14, }]} >{data.item.fee_month} </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {
                        setMonthlyBill(data.item.data)
                        setIsBill(!isBill)
                    }}
                    style={{
                        backgroundColor: '#F8FAFF', flex: 1, justifyContent: 'center', alignItems: 'center',
                        marginTop: 1,
                        borderBottomWidth: 1,
                        borderColor: 'lightgray',
                    }} >
                    <Text style={[myStyle.textNormal, { color: '#28293D', fontSize: 14, }]} >{data.item.fee_amount} </Text>
                </TouchableOpacity>
            </View>
        )
    }


    // heading for fee month and amount 
    const renderItemHeader = () => {
        return (
            <View style={{ height: 50, elevation: 0.5, flexDirection: 'row', backgroundColor: 'white', borderTopRightRadius: 25, borderTopLeftRadius: 25, }} >
                <TouchableOpacity style={{ backgroundColor: 'white', flex: 1, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 25, }} >
                    <Text style={[myStyle.textBold, { color: '#28293D', fontSize: 16, }]} >{'Months'} </Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: 'white', flex: 1, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 25, }} >
                    <Text style={[myStyle.textBold, { color: '#28293D', fontSize: 16, }]} >{'Amount(INR)'} </Text>
                </TouchableOpacity>
            </View>
        )
    }

    return (
        <View style={{ backgroundColor: Colors.whiteText, flex: 1, }}>

            <TeacherHeader onPressBack={onPressBack}
                txt="Fee Structure"
            />

            <View style={{ flex: 1, backgroundColor: '#000D83', }} >
                <View style={{
                    flexDirection: 'row', alignItems: 'center', backgroundColor: '#000D83', paddingHorizontal: 40,
                    paddingVertical: 20
                }} >
                    <Image
                        source={{ uri: props.students_details.profile_pic }}
                        style={{ height: 65, width: 65, resizeMode: 'contain', marginHorizontal: 10, borderRadius: 25 }}
                    />
                    <View>
                        <Text style={[myStyle.textBold, { fontSize: 14, marginTop: 5, color: '#FFFFFF' }]}>{props.students_details.student_name} </Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, marginTop: 3, color: '#51C9FE' }]}>{props.students_details.school_name}</Text>
                        <Text style={[myStyle.textNormal, { fontSize: 12, marginTop: 2, color: '#FFFFFF' }]}>{props.students_details.school_address} </Text>
                    </View>

                </View>
                <View style={{ flex: 1, backgroundColor: '#F8FAFF', elevation: 5, flexDirection: 'row', borderTopRightRadius: 20, borderTopLeftRadius: 20, }} >
                    <FlatList
                        data={props.feeStructureList}
                        contentContainerStyle={{ paddingBottom: 200 }}
                        renderItem={renderItem}
                        ListHeaderComponent={renderItemHeader}
                        keyExtractor={(item, index) => 'key' + index}
                        showsVerticalScrollIndicator={false}
                    />
                </View>

            </View>


            {isBill ?
                <FeeBillPopUp
                    bill={monthlyBill}
                    onOk={() => setIsBill(!isBill)}
                />
                : null
            }
        </View>

    )
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
        justifyContent: 'center',
        alignItems: "center"
    },
})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    let jas = state.indexReducerJaswant;

    return {
        feeStructureList: akshita.feeStructureList,
        students_details: common.students_details,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setFeeStructureList: (list) => setFeeStructureList(list),
            hitGetFeeStructureApi: (param) => hitGetFeeStructureApi(param)

        },
        dispatch,
    );
};

export const FeeStructureScreenMemo = connect(mapStateToProps, mapDispatchToProps)(React.memo(FeeStructureScreen));