import React, { Component } from "react";
import { View, StyleSheet, SafeAreaView, StatusBar, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback, TouchableOpacity, ScrollView } from "react-native";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import BottomNavigation from "../components/BottomNavigation";
import Header from "../components/Header";
import MoreRow from "../components/MoreRow";
import NotificationRow from "../components/NotificationRow";

class MoreScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isToggle: true
    }
  }


  onTogglePress = () => {
    this.setState({ isToggle: !this.state.isToggle })
  }

  onMessage = (msg) => {
    debugLog(msg.nativeEvent.data)
  }
  onPressReportCard = () => {
    this.props.navigation.navigate('ReportCardListScreen')
  }
  onPressFee = () => {
    this.props.navigation.navigate('FeeScreen1Memo');
  }
  onPressNotification = () => {
    this.props.navigation.navigate('MoreNoticationScreen')
  }
  onPressGallery = () => {
    this.props.navigation.navigate('GalleryScreen')
  }
  
  onPressEvent = () => {
    this.props.navigation.navigate('EventScreen')
  }

  onSchoolDetail = () => {
    this.props.navigation.navigate('SchoolDetailsScreen')
  }

  onPressBack = () => {
    this.props.navigation.goBack()
  }

  render() {
    return (
      <SafeAreaView style={myStyle.container} >

        <Header
          onPressBack={() => this.onPressBack()}
          title="More"
          style={{
            elevation: 2,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            backgroundColor: Colors.whiteText,
            paddingVertical: 32,
            paddingHorizontal: 15,
          }}
        />
        <ScrollView contentContainerStyle={{ marginBottom: 50 }}>
          <View style={{ flex: 1 }} >




            <MoreRow
              image={require('../../assets/report.png')}
              backgroundColor='#DFE6F3'
              title="Report Card"
              onPress={() => this.onPressReportCard()}
            // onPress={onPressReortCard}
            />

            <MoreRow
              image={require('../../assets/fee.png')}
              backgroundColor='#DFF3E7'
              title="Fee (Pay or Check)"
              onPress={() => this.onPressFee()}
            />

            <NotificationRow
              title="Notification"
              image={require('../../assets/notification1.png')}
              onPressToggle={() => this.onTogglePress()}
              onPressNotification={() => this.onPressNotification()}
              isToggle={this.state.isToggle}
              backgroundColor='rgba(235, 87, 87, 0.15)'
            />

            <MoreRow
              image={require('../../assets/gallery.png')}
              backgroundColor='rgba(242, 153, 74, 0.15)'
              title="Gallery"
              onPress={() => this.onPressGallery()}
            />
            <MoreRow
              image={require('../../assets/calendar.png')}
              backgroundColor='rgba(155, 81, 224, 0.15)'
              title="Events"
              onPress={() => this.onPressEvent()}

            />
            <MoreRow
              image={require('../../assets/school_detail.png')}
              backgroundColor='rgba(155, 81, 224, 0.15)'
              title="School/College Contact Details"
              onPress={() => this.onSchoolDetail()}
            />
            <MoreRow
              image={require('../../assets/home_work.png')}
              backgroundColor='rgba(86, 204, 242, 0.15)'
              title="Home Work"
              mainStyle={{ marginBottom: 60 }}
            />




          </View>


        </ScrollView>

        <BottomNavigation
          navigation={this.props.navigation}
          isMore={true}
        />
      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.backgroundColor
  },
});

export default MoreScreen;
