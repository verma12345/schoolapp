import React, { Component } from "react";
import { Image, PixelRatio, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { debugLog } from "../common/Constants";
import { myStyle } from "../common/myStyle";
import BottomNavigation from "../components/BottomNavigation";
import { Loader } from "../components/Loader";
import NotificationFeedScreen from "../components/NotificationFeedScreen";
import { StudentDetailsOptionMemo } from "../components/StudentDetailsOption";
import { hitGetAttendanceApi } from "../redux_store/actions/indexActionApi";
import { setAtendanceList, setEmptyWeek, setHolidayList } from "../redux_store/actions/indexActionsJaswant";

class StudentDetailsScreen extends Component {

    constructor(props) {
        super(props);
        // this.props.setEmptyWeek()
        this.state = {
            isCalendar: false,
            isLoading: false,
        }

        this.listView = null
        this.listViewY = null
        this.scrollInterval = null
    }

    onPressTeacher = () => {
        this.props.navigation.navigate('TeacherScreen');
        // alert('ccccccccccccc')
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }
    onPressLocation = () => {
        this.props.navigation.navigate('MapTestingNew');
    }

    onPressTimeTable = () => {
        this.props.navigation.navigate('LectureTimeTableScreen');
    }
    onPressFee = () => {
        this.props.navigation.navigate('FeeScreen1Memo');
    }
    onPressElearning = () => {
        this.props.navigation.navigate('ELearningScreen');
    }
    onPressReport = () => {
        this.props.navigation.navigate('ReportCardListScreen');
    }

    onPressCalandar = () => {
        // this.setState({ isCalendar: !this.state.isCalendar })
        // this.scrollListReftop.scrollTo({ x: 0, y: 200, animated: true })

        let params = { school: { stu_id: 64 } }
        this.setState({ isLoadingCalender: true })
        this.props.hitGetAttendanceApi(params).then(res => {
            debugLog(res)
            if (res.school.status == 1) {
                // props.setHolidayList(res.school.holiday)
                this.props.setAtendanceList(res.school.attendance)
                this.setState({ isLoadingCalender: false })
                this.props.navigation.navigate('MyCalanderMemo')
            }
        })
    }


    render() {
        return (
            <SafeAreaView style={myStyle.container}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    ref={(ref) => { this.scrollListReftop = ref; }}
                    style={[myStyle.container, { marginBottom: 90 }]}>
                    {/* <Header
                        title={false}
                        onPressBack={this.onPressBack}
                    /> */}

                    <View style={[myStyle.myshadow,
                    {
                        backgroundColor: 'white',
                        // marginHorizontal: 8,
                        marginBottom: 10,
                        borderBottomLeftRadius: 20,
                        borderBottomRightRadius: 20
                    }]}>




                        <View style={[{ padding: 10, borderTopRightRadius: 20, borderTopLeftRadius: 20, backgroundColor: Colors.whiteText }]} >
                            <View style={{ flexDirection: 'row', padding: 8 }} >

                                <TouchableOpacity
                                    onPress={this.onPressBack}
                                    style={{ alignItems: 'center', marginRight: 13 }}
                                >
                                    <Image
                                        source={require('../../assets/back.png')}
                                        style={{ height: 17, width: 17, resizeMode: 'contain' }}
                                    />
                                </TouchableOpacity>

                                <Image
                                    style={{
                                        height: PixelRatio.getPixelSizeForLayoutSize(30),
                                        width: PixelRatio.getPixelSizeForLayoutSize(30)
                                    }}
                                    source={{ uri: this.props.students_details.profile_pic }}
                                />

                                <View style={{ paddingHorizontal: 15 }} >
                                    <Text style={[myStyle.textBold, { fontSize: 14, color: '#28293D' }]} >
                                        {/* {"Akshita"} */}
                                        {this.props.students_details.student_name}
                                    </Text>

                                    <Text style={[myStyle.textNormal, { color: '#28293D', fontSize: 12 }]} >
                                        {this.props.students_details.school_name}
                                    </Text>

                                    <Text style={[myStyle.textNormal, { color: '#28293D', fontSize: 12 }]} >
                                        {this.props.students_details.school_address}
                                    </Text>

                                    {/* <Text style={[myStyle.textNormal, { fontSize: 12, color: '#909090' }]} >
                                        {"Aliganj, Lucknow"}
                                    </Text> */}
                                </View>

                                {/* ***************************************location**************************************************** */}
                                <TouchableOpacity
                                    onPress={() => this.onPressLocation()}
                                    style={{ position: 'absolute', right: 10, top: 10, padding: 3, borderColor: Colors.blue, borderWidth: 1, borderRadius: 9 }}
                                >
                                    <Image
                                        source={require('../../assets/location.png')}
                                        style={{ height: 18, width: 18, resizeMode: 'contain' }}
                                    />
                                </TouchableOpacity>
                            </View>





                            {/* Bottom row */}
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                <View style={{ alignItems: 'center' }} >
                                    <Text style={[myStyle.textNormal, { fontSize: 14, color: '#909090' }]}>
                                        {'Class'}
                                    </Text>

                                    <Text style={[myStyle.textBold, { color: '#28293D', fontSize: 14 }]}>
                                        {this.props.students_details.class} {this.props.students_details.section}
                                    </Text>

                                </View>
                                <View style={{ alignItems: 'center' }} >
                                    <Text style={[myStyle.textNormal, { fontSize: 14, color: '#909090' }]}>
                                        {'Attendance'}
                                    </Text>
                                    <Text style={[myStyle.textBold, { color: '#28293D', fontSize: 14 }]}>
                                        {'95%'}
                                    </Text>
                                </View>

                                <View style={{ alignItems: 'center' }} >
                                    <Text style={[myStyle.textNormal, { fontSize: 14, color: '#909090' }]}>
                                        {"Rank"}
                                    </Text>
                                    <Text style={[myStyle.textBold, { color: '#28293D', fontSize: 14 }]}>
                                        {'2'}
                                    </Text>
                                </View>

                            </View>
                        </View>


                        <View style={[{ borderTopWidth: 1, marginTop: 5, marginBottom: 5, borderTopColor: '#E9E9E9', flexDirection: 'row', justifyContent: 'space-around', backgroundColor: 'white', borderBottomLeftRadius: 20, borderBottomRightRadius: 20, }]} >
                            <TouchableOpacity
                                onPress={() => this.onPressTimeTable()}
                                // onPress={() => alert(this.props.students_details.name)}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }} >
                                <Text style={[myStyle.textBold, { fontSize: 13 }]}>
                                    {'Time Table'}
                                </Text>
                            </TouchableOpacity>

                            <View style={{ height: 40, width: 1, backgroundColor: "#E9E9E9" }} />

                            <TouchableOpacity
                                onPress={() => this.onPressReport()}
                                style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                                <Text style={[myStyle.textBold, { fontSize: 13 }]}>
                                    {'Report'}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{ marginHorizontal: 5, marginTop: 10 }}>
                        <StudentDetailsOptionMemo
                            headerCount={this.props.totalTeacherCounts}
                            title='Teachers'
                            message='Teachers'
                            onPress={() => this.onPressTeacher()}
                            leftIcon={require('../../assets/trachers.png')}
                            rightIcon={require('../../assets/next_button.png')}
                            iconStyle={{ backgroundColor: '#EFEDF9' }}
                        />

                        <StudentDetailsOptionMemo
                            loader={this.state.isLoadingCalender}
                            title='Attendence Calendar'
                            message='You can see the complete attendence details'
                            onPress={() => this.onPressCalandar()}
                            leftIcon={require('../../assets/attandence.png')}
                            rightIcon={require('../../assets/next_button.png')}
                            iconStyle={{ backgroundColor: '#F1F9ED' }}

                        />


                        <StudentDetailsOptionMemo
                            title='Fees'
                            message='You may check dues and undues'
                            onPress={() => this.onPressFee()}
                            leftIcon={require('../../assets/fee_details.png')}
                            rightIcon={require('../../assets/next_button.png')}
                            // style={{ marginBottom: 150 }}
                            iconStyle={{ backgroundColor: '#F9EDED' }}

                        />

                        <StudentDetailsOptionMemo
                            title='E-Learning'
                            message='You may check dues and undues'
                            onPress={() => this.onPressElearning()}
                            leftIcon={require('../../assets/elearning.png')}
                            rightIcon={require('../../assets/next_button.png')}
                            style={{ marginBottom: 150 }}
                            iconStyle={{ backgroundColor: 'rgba(47, 128, 237, 0.2)' }}

                        />
                    </View>

                </ScrollView >
                <NotificationFeedScreen />
                {/* {
                    this.state.isLoadingCalender ?
                        <Loader />
                        : null
                } */}
                {/* <BottomNavigation
                    navigation={this.props.navigation}
                    isDashboard={true}
                /> */}
            </SafeAreaView >

        )
    }
}


const styles = StyleSheet.create({



});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const jas = state.indexReducerJaswant;


    return {
        students_details: common.students_details,
        totalTeacherCounts: jas.totalTeacherCounts
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setEmptyWeek: () => setEmptyWeek(),

            setHolidayList: (list) => setHolidayList(list),
            setAtendanceList: (list) => setAtendanceList(list),
            hitGetAttendanceApi: (params) => hitGetAttendanceApi(params)
        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(StudentDetailsScreen);