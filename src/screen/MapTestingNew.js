import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, StyleSheet, View, Text, TouchableOpacity, ProgressBarAndroid, ProgressBarAndroidComponent, Image, SafeAreaView } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, Polyline, Callout } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import Colors from '../common/Colors';
import { debugLog, GOOGLE_MAPS_APIKEY } from '../common/Constants';
import { CustomeMarker } from '../components/CustomeMarker';
import Geolocation from '@react-native-community/geolocation';
import { myStyle } from '../common/myStyle';
import BottomNavigation from '../components/BottomNavigation';
import Header from '../components/Header';


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 0
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


class MapTestingNew extends Component {

    constructor(props) {
        super(props);

        this.state = {
            coordinates: null,
            currentPosition: 0,
            isPopup: false
        };

        this.mapView = null;
    }

    componentDidMount() {

        // to get current position  in map
        
        Geolocation.getCurrentPosition(
            position => {
                this.setState({
                    coordinates: [
                        position.coords,
                    ],
                });

            },
            (error) => console.log(error.message),
            { enableHighAccuracy: true, timeout: 2000, maximumAge: 300 },
        );
        this.watchID = Geolocation.watchPosition(
            position => {
                this.setState({
                    coordinates: [
                        position.coords,
                    ],
                });
                debugLog('watchId:::::::::')
                debugLog(position)
            }
        );
        debugLog(this.state.coordinates)
    }



    componentWillUnmount() {
        Geolocation.clearWatch(this.watchID);
    }


    onMapPress = (e) => {
        this.setState({
            coordinates: [
                ...this.state.coordinates,
                e.nativeEvent.coordinate,
            ],
        });
        debugLog(e.nativeEvent.coordinate)
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }

    // to get bus detail popup on downarrow btn
    setPopup = () => {
        this.setState({ isPopup: !this.state.isPopup })
    }
    
    render() {

        return (
            <SafeAreaView style={myStyle.container}>
                <View style={{ flex: 1, }} >

                    {/* <TouchableOpacity
                        onPress={() => this.onPressBack()}
                        style={{ elevation: 10, padding: 20, }}
                    >
                        <Image
                            source={require('../../assets/back.png')}
                            style={{ height: 16, width: 16, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity> */}

                    <View style={[myStyle.myshadow, { position: 'absolute', }]}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}  >
                            <TouchableOpacity
                                onPress={() => this.onPressBack()}
                                style={{ justifyContent: 'center', alignItems: 'center', padding: 20, }} >
                                <Image
                                    source={require('../../assets/back.png')}
                                    style={{ height: 16, width: 16, resizeMode: 'contain' }}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>



                    {this.state.coordinates != null ? (this.state.coordinates != null) && (

                        <MapView
                            initialRegion={{
                                latitude: this.state.coordinates[0].latitude,
                                longitude: this.state.coordinates[0].longitude,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            }}
                            // showsTraffic={true}
                            // showsUserLocation={true}
                            // followsUserLocation={true}
                            style={StyleSheet.absoluteFill}
                            ref={c => this.mapView = c}
                            onPress={this.onMapPress}
                        >
                            {this.state.coordinates.map((coordinate, index) =>
                                <MapView.Marker
                                    key={`coordinate_${index}`} coordinate={coordinate}
                                    pinColor={Colors.red}

                                    ref={marker => { this.marker = marker }}
                                    flat
                                    style={{
                                        transform: [{
                                            rotate: '100deg'
                                        }]
                                    }}
                                >
                                    {index == 0 ?
                                        <CustomeMarker
                                            index={index}
                                        />
                                        :
                                        <CustomeMarker />
                                    }
                                </MapView.Marker>

                            )}



                            {this.state.coordinates != null ? (this.state.coordinates.length >= 2) && (
                                <MapViewDirections
                                    origin={this.state.coordinates[0]}
                                    // waypoints={ (this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1): null}
                                    destination={this.state.coordinates[this.state.coordinates.length - 1]}
                                    apikey={GOOGLE_MAPS_APIKEY}
                                    strokeWidth={5}
                                    strokeColor={Colors.blue}
                                    optimizeWaypoints={true}
                                    onStart={(params) => {
                                        console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                                    }}
                                    onReady={result => {
                                        console.log(`Distance: ${result.distance} km`)
                                        console.log(`Duration: ${result.duration} min.`)

                                        this.mapView.fitToCoordinates(result.coordinates, {
                                            edgePadding: {
                                                right: (width / 20),
                                                bottom: (height / 20),
                                                left: (width / 20),
                                                top: (height / 20),
                                            }
                                        });
                                    }}
                                    onError={(errorMessage) => {
                                        // console.log('GOT AN ERROR');
                                    }}
                                />
                            ) : null}
                        </MapView>


                    ) :
                        <View style={[StyleSheet.absoluteFill, { backgroundColor: '#0002', justifyContent: 'center', alignItems: 'center' }]} >
                            <MapView
                                initialRegion={{
                                    latitude: 26.88728259276827,
                                    longitude: 80.96731346100569,
                                    latitudeDelta: LATITUDE_DELTA,
                                    longitudeDelta: LONGITUDE_DELTA,
                                }}

                                style={StyleSheet.absoluteFill}
                                ref={c => this.mapView = c}
                            >

                            </MapView>
                            <View style={{ position: 'absolute', elevation: 10, zIndex: 2, alignItems: 'center', justifyContent: 'center' }} >
                                <ActivityIndicator
                                    size='large'
                                    color={Colors.blue} />
                            </View>
                        </View>
                    }

                    {/* <MapBottomFeed /> */}

                    {/********************************************************  navigation status  ***********************************************/}
                    <View style={[myStyle.myshadow, { position: 'absolute', bottom: 0, width: '100%', alignSelf: 'center', backgroundColor: '#0000' }]}>
                        {this.state.isPopup ?
                            <View style={[{ padding: 10, borderTopRightRadius: 20, borderTopLeftRadius: 20, backgroundColor: Colors.whiteText, paddingHorizontal: 15 }]} >

                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}  >

                                    <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                        <Image
                                            style={{ height: 45, width: 45 }}
                                            source={require('../../assets/teacher.png')}
                                        />
                                        <View style={{ paddingLeft: 10 }} >
                                            <Text style={[myStyle.textBold, { color: Colors.textDarkColor }]}> {'Tony Jhons'} </Text>
                                            <Text style={[myStyle.textNormal, { color: 'white', fontSize: 11, backgroundColor: '#6BB893', borderRadius: 10, marginVertical: 5, marginLeft: 2, alignSelf: 'baseline', paddingHorizontal: 5 }]}> {'Picked up'} </Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                                                <Image
                                                    style={{ height: 15, width: 15, resizeMode: 'contain' }}
                                                    source={require('../../assets/location_red.png')}
                                                />
                                                <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 11 }]}> {'Indira Nagar, Lucknow'} </Text>
                                            </View>
                                        </View>
                                    </View>

                                    <TouchableOpacity
                                        // onPress={() => this.onPressTimeTable()}
                                        style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 10, borderWidth: 1, backgroundColor: Colors.blue, paddingHorizontal: 10, paddingVertical: 5, borderColor: '#E9E9E9' }} >
                                        <Text style={[myStyle.textBold, { color: Colors.whiteText }]}> {'Call'} </Text>
                                    </TouchableOpacity>

                                </View>

                            </View>
                            : null}

                        {this.state.isPopup ?
                            <View style={[{
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                backgroundColor: 'white',
                                borderTopWidth: 1,
                                borderBottomWidth: 1,
                                borderColor: '#E9E9E9',
                            }]} >
                                <TouchableOpacity
                                    // onPress={() => this.onPressTimeTable()}
                                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }} >
                                    <Text style={[myStyle.textNormal, { fontSize: 13 }]}>
                                        {'40 mins'}
                                    </Text>
                                </TouchableOpacity>

                                <View style={{ height: 35, width: 1, backgroundColor: "gray" }} />

                                <TouchableOpacity
                                    // onPress={() => this.onPressReport()}
                                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                                    <Text style={[myStyle.textNormal, { fontSize: 13 }]}>
                                        {'60 KM/H'}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            : null
                        }

                        <TouchableOpacity
                            onPress={() => this.setPopup()}

                            style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 8, backgroundColor: 'white' }} >
                            <Image
                                style={{ height: 15, width: 15, resizeMode: 'contain' }}
                                source={this.state.isPopup ? require('../../assets/downaro.png'): require('../../assets/up_arrow.png')}
                            />
                        </TouchableOpacity>

                        {/* <BottomNavigation
                            navigation={this.props.navigation}
                            isMap={true}
                        /> */}
                    </View>



                </View >
            </SafeAreaView>

        );
    }
}

export default MapTestingNew;