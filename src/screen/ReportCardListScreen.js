import React, { Component } from "react"
import { Image, StyleSheet, Text, View, SafeAreaView, ScrollView, TouchableOpacity, FlatList, StatusBar } from "react-native";
import Colors from "../common/Colors";
import { ReportHeader } from "../components/ReportHeader";
import { myStyle } from "../common/myStyle";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { hitReportCardListApi, hitReportCardMarksApi } from "../redux_store/actions/indexActionApi";
import { setClassTestData, setClassTestSubjects, setReportCardList, setReportCardMarksList } from "../redux_store/actions/indexActionsJaswant";
import { debugLog } from "../common/Constants";
import { Loader } from "../components/Loader";


class ReportCardListScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reportCardList: []
        }
    }

    onPressBack = () => {
        this.props.navigation.goBack()
    }


    //api for report card Exam list
    componentDidMount() {
        let params = {
            school: {
                class: 1,
            }
        }

        this.props.hitReportCardListApi(params).then(res => {
            if (res.school.status == 1) {
                this.setState({ reportCardList: res.school.exam_list })
                // this.props.setReportCardList(res.school.exam_list)
            }
        })
    }

    onPress = (item) => {
        console.log(item);
        // return
        let params = {
            "school": {
                "stu_id": 64,
                "exam_id": item.exam_id,
                "session_year": '21-22',
                class: 1
            }
        }

        console.log(params);
        this.props.hitReportCardMarksApi(params).then(res => {
            if (res.school.status == 1) {

                if (item.exam_name == 'Class Test') {
                    this.props.setClassTestData(res.school.report_card)

                    this.props.navigation.navigate('ClassTestScreen1')
                } else {
                    this.props.setReportCardMarksList(res.school.report_card)
                    this.props.navigation.navigate('ReportCardScreen')

                }
            }
        })
    }


    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => this.onPress(dataItem.item)}
                style={{ flexDirection: "row", borderTopWidth: dataItem.index != 0 ? 1 : 0, paddingHorizontal: 10, borderTopColor: Colors.lightGray, paddingVertical: 20 }}
            >
                <Text style={[myStyle.textBold, { flex: 2, color: Colors.grayColor, fontSize: 14, marginLeft: 30 }]}>
                    {dataItem.item.exam_name}
                </Text>
                <Text style={[myStyle.textBold, { fontSize: 16, flex: 0.5 }]}>
                    {dataItem.item.exam_percent}
                </Text>
                <Image
                    style={{ height: 20, width: 20, resizeMode: 'contain' }}
                    source={require('../../assets/next_button.png')}
                />

            </TouchableOpacity>
        )
    }



    render() {

        return (
            <SafeAreaView style={styles.container}>
                <StatusBar
                    // translucent={true}
                    backgroundColor={'white'}
                    barStyle="dark-content" />
                <View
                    style={styles.container}
                >
                    <ReportHeader
                        isMenu={false}
                        onPressBack={this.onPressBack}
                        icon1={require('../../assets/profile.png')}
                        txt="Report Card"
                        icon2={require('../../assets/filter1.png')}
                        style={{ borderBottomRightRadius: 20, borderBottomLeftRadius: 20, paddingVertical: 5 }}
                    />


                    <View style={[myStyle.myshadow2, { borderRadius: 20, margin: 20, backgroundColor: 'white' }]}>
                        <FlatList
                            data={this.state.reportCardList}
                            renderItem={this._renderItem}
                            showsVerticalScrollIndicator={false}
                            keyExtractor={(item, index) => 'key' + index}
                        />

                    </View>

                    {this.props.isLoading ?
                        <Loader />
                        : null
                    }
                </View>
            </SafeAreaView>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'space-between',
        backgroundColor: Colors.backgroundColor
    },
    txt: {
        fontSize: 34,
        fontWeight: 'bold',
        paddingTop: 40,
        color: Colors.textColor
    }
})

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    const jaswant = state.indexReducerJaswant

    return {
        isLoading: common.isLoading,
        reportCardList: jaswant.reportCardList,
        reportCardMarksList: jaswant.reportCardMarksList
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {

            // setEmptyWeek: () => setEmptyWeek(),
            hitReportCardListApi: (param) => hitReportCardListApi(param),
            setReportCardList: (list) => setReportCardList(list),
            hitReportCardMarksApi: (params) => hitReportCardMarksApi(params),
            setReportCardMarksList: (list) => setReportCardMarksList(list),
            setClassTestData: (list) => setClassTestData(list),



        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ReportCardListScreen);