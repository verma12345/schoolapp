import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import GalleryPagerComponent from "../components/GalleryPagerComponent";
import GalleryPagerDot from "../components/GalleryPagerDot";
import { setGalleryPager } from "../redux_store/actions/indexActionAkshita";
import { setPosition } from "../redux_store/actions/indexActionsJaswant";

class ViewPagerGallery extends Component {

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <GalleryPagerComponent
          onPageSelected={(index) => { this.props.setPosition(index.nativeEvent.position) }}
          pager_list={this.props.gallery_pager}
        />

          <View style={{ paddingLeft: 30 }} >
            <GalleryPagerDot
              list={this.props.gallery_pager}
              position={this.props.position}
            />
          </View>

      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },

});

const mapStateToProps = (state) => {
  const common = state.indexReducer;
  const akshita = state.indexReducerAkshita;
  let jas = state.indexReducerJaswant;



  return {
    position: jas.position,
    gallery_pager: akshita.gallery_pager
  };
};


const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      setPosition: (list) => setPosition(list),
      setGalleryPager: (pager_list) => setGalleryPager(pager_list)

    }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(ViewPagerGallery);