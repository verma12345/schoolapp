
import React, { Component } from 'react'
import { Dimensions, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Colors from '../common/Colors'
import { debugLog } from '../common/Constants'
import { myStyle } from '../common/myStyle'
import FeeScreenHeader from '../components/FeeScreenHeader'
import { setFeeDetailList, setInvoiceDetails } from '../redux_store/actions/indexActionAkshita'
import { hitFeeStatusApi, hitInvoiceApi } from '../redux_store/actions/indexActionApi'
import Share from 'react-native-share'


class FeeScreen1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fee_data: [],
            currentPosition: 0
        }
    }

    // api for fee status
    componentDidMount() {
        let params = {
            school: {
                "stu_id": 64,
                "class": 1,
                "session_year": "21-22"
            }
        }

        this.props.hitFeeStatusApi(params).then(res => {
            debugLog(res.school.fee_months)
            if (res.school.status == 1) {
                this.setState({ fee_data: res.school.fee_months })
                this.props.setFeeDetailList(res.school)
            }
        })
    }

    // for share
    captureAndShareScreenshot = () => {
        let options = {
            title: 'Share Title',
            message: 'Share Message',
            // url: urlString,
            // type: 'image/jpeg',
            type: 'simple/txt'
        };
        Share.open(options)
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                err && console.log(err);
            });
    };

    // in click View
    onView = () => {
        let params = {
            school: {
                "stu_id": 64,
                "session_year": "21-22"
            }
        }
        this.props.hitInvoiceApi(params).then(res => {
            debugLog(res.school.invoice)
            if (res.school.status == 1) {
                this.props.setInvoiceDetails(res.school.invoice)
                this.props.navigation.navigate('InvoiceListScreen')
            }
        })
    }

    // update list for colaps view boolean
    _onPress = (position) => {
        this.setState({ index: position })
        let newdata = []
        this.state.fee_data.map((item, index) => {
            if (position == index) {
                newdata.push({
                    fee_month: item.fee_month,
                    fee_amount: item.fee_amount,
                    // dateTime: item.dateTime,
                    isColaps: !item.isColaps,
                    paid_status: item.paid_status,
                })
            }
            else {
                newdata.push({
                    fee_month: item.fee_month,
                    fee_amount: item.fee_amount,
                    // dateTime: item.dateTime,
                    isColaps: item.isColaps,
                    paid_status: item.paid_status,
                })
            }
        })
        this.setState({ fee_data: newdata })
    }

    // for go back
    onBack = () => {
        props.navigation.goBack()
    }

    // for changing position
    nextStep = () => {
        this.setState({ currentPosition: this.state.currentPosition + 1 })
    }

    onPay = () => {
        this.props.navigation.navigate('PaymentScreen')
    }


    // fee details content
    renderItem = (data) => {

        return (
            <TouchableOpacity
                onPress={() => {
                    this._onPress(data.index)
                }}
                style={[{
                    marginTop: 12,
                    backgroundColor: data.item.paid_status == 'PAID' ? '#F8FAFF' : 'white',
                    elevation: data.item.paid_status == '' ? 1 : data.item.paid_status == 'DUE' ? 1 : 1,
                    borderRadius: 20,
                    paddingVertical: data.item.isColaps ? 0 : 10,
                    marginHorizontal: 25,
                    paddingHorizontal: data.item.isColaps ? 0 : 10,
                    paddingLeft: data.item.isColaps ? 0 : 0,
                    marginBottom: data.index == this.state.fee_data.length - 1 ? 40 : 0
                }]}>

                {
                    data.item.isColaps != true ?

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                            {/* indicator */}
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginHorizontal: 10 }} >
                                <View

                                    style={{ height: 25, width: 25, borderRadius: 100, marginHorizontal: 10 }} >
                                    {data.item.paid_status == '' ?

                                        <View style={{ position: 'absolute', elevation: 5 }} >
                                            <Image
                                                source={require('../../assets/circle.png')}
                                                style={{ height: 24, width: 24, resizeMode: 'contain', }}
                                            />
                                        </View>

                                        : data.item.paid_status == 'DUE' ?
                                            <View style={{ position: 'absolute', elevation: 5 }} >
                                                <Image
                                                    source={require('../../assets/pending.png')}
                                                    style={{ height: 24, width: 24, resizeMode: 'contain', }}
                                                />
                                            </View>
                                            :

                                            <View style={{ position: 'absolute', elevation: 5 }} >
                                                <Image
                                                    source={require('../../assets/paid.png')}
                                                    style={{ height: 24, width: 24, resizeMode: 'contain' }}
                                                />
                                            </View>

                                    }
                                </View>

                                <View
                                    style={{ paddingVertical: data.item.paid_status == '' ? 10 : 0 }}
                                >
                                    <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>
                                        {data.item.fee_month}
                                    </Text>
                                    {data.item.paid_status == '' ?

                                        null :
                                        <Text style={[myStyle.textBold, { fontSize: 12, color: data.item.paid_status == 'DUE' ? '#F2994A' : 'green' }]}>
                                            {data.item.paid_status}
                                        </Text>
                                    }
                                </View>





                            </View>

                            <View style={{ flexDirection: 'row' }}>

                                <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor, marginRight: 15 }]}>
                                    {'Rs '}{data.item.fee_amount}
                                </Text>
                                <Image
                                    source={require('../../assets/next_button.png')}
                                    style={{ height: 17, width: 17, resizeMode: 'contain' }}
                                />
                            </View>

                        </View>

                        :
                        // Collaps
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                            <View style={{ height: 24, width: 24, borderRadius: 100, marginHorizontal: 10 }} >
                                {data.item.fee_status == 'DUE' ?
                                    <View style={{ position: 'absolute', }} >
                                        <Image
                                            source={require('../../assets/pending.png')}
                                            style={{ height: 24, width: 24, resizeMode: 'contain', }}
                                        />
                                    </View>
                                    :

                                    <View style={{ position: 'absolute', }} >
                                        <Image
                                            source={require('../../assets/paid.png')}
                                            style={{ height: 24, width: 24, resizeMode: 'contain' }}
                                        />
                                    </View>

                                }
                            </View>


                            <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', }} >
                                <View>
                                    <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.grayColor }]}>
                                        {data.item.fee_month}
                                    </Text>
                                    <Text style={[myStyle.textBold, { fontSize: 12, color: data.item.fee_status == 'DUE' ? '#F2994A' : 'green' }]}>
                                        {data.item.fee_status}
                                    </Text>
                                </View>
                            </View>


                            <View style={{ flex: 1, paddingTop: 20 }} >
                                {/* indicator */}
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20 }} >
                                    <Text style={[myStyle.textBold, { fontSize: 15, color: Colors.textDarkColor, marginRight: 15 }]}>
                                        {'Rs. '}{data.item.fee_amount}
                                    </Text>
                                    <Image
                                        source={require('../../assets/next_button.png')}
                                        style={{ height: 20, width: 20, resizeMode: 'contain' }}
                                    />
                                </View>

                                <View style={{
                                    flexDirection: 'row',
                                    flex: 1,
                                    height: 50,
                                    marginTop: 10,
                                    borderTopWidth: 1,
                                    borderTopColor: 'gray',
                                    marginLeft: 20
                                }} >

                                    <TouchableOpacity
                                        onPress={() => this.captureAndShareScreenshot()}
                                        style={[{ flex: 1, justifyContent: 'center', alignItems: 'center' }]}
                                    >
                                        <Text style={[styles.status, { color: Colors.grayColor }]}>
                                            {'Share'}
                                        </Text>
                                    </TouchableOpacity>


                                    <TouchableOpacity
                                        onPress={() => this.props.navigation.navigate('FeeSlipMemo')}
                                        style={[{ flex: 1, justifyContent: 'center', alignItems: 'center', borderLeftWidth: 1, borderLeftColor: 'gray' }]} >
                                        <Text style={[styles.status, { color: Colors.grayColor }]}>
                                            {'Download'}
                                        </Text>
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                }

            </TouchableOpacity>
        );
    }

    onBack = () => {
        this.props.navigation.goBack()
    }

    onFeeStructure = () => {
        this.props.navigation.navigate('FeeStructureScreenMemo')
    }

    // due fee content
    listHeaderItem = (data) => {
        return (
            <View>
                {this.listFooterItem()}
                <View style={{ marginHorizontal: 25, backgroundColor: Colors.whiteText, elevation: 3, borderRadius: 20, marginTop: 11 }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 12, marginLeft: 10 }} >
                        <View>
                            <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 15 }]}>{this.props.feeDetailList.length != 0 ? this.props.feeDetailList.fee_due.title : null} </Text>
                            <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 12 }]}>{this.props.feeDetailList.length != 0 ? this.props.feeDetailList.fee_due.months : null} </Text>

                        </View>

                        <TouchableOpacity
                            style={{ backgroundColor: '#000D83', paddingHorizontal: 26, paddingVertical: 10, borderRadius: 10 }}
                            onPress={() => this.onPay()}
                        >
                            <Text style={[myStyle.textNormal, { color: 'white', fontSize: 14 }]} >{'PAY'} </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginLeft: 10, paddingHorizontal: 10, paddingBottom: 7, borderTopWidth: 1, borderTopColor: 'lightgray' }} >
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                            <Image
                                source={require('../../assets/pending.png')}
                                style={{ height: 18, width: 18, resizeMode: 'contain', marginRight: 5 }}
                            />
                            <Text style={[myStyle.textBold, { color: Colors.grayColor, fontSize: 13 }]} >{'Pending'} </Text>
                        </View>

                        <TouchableOpacity style={{ marginTop: 5 }} >
                            <Text style={[myStyle.textBold, { color: Colors.textColor, fontSize: 14 }]} >{'Rs'} {this.props.feeDetailList.length != 0 ? this.props.feeDetailList.fee_due.amount : null} </Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }

    // invoice details
    listFooterItem = (data) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('InvoiceScreen')}
                style={{ marginHorizontal: 25, backgroundColor: 'white', elevation: 3, borderRadius: 20, marginTop: 12 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 20, paddingVertical: 10 }} >


                    <View style={{ flexDirection: 'row', alignItems: 'center' }} >

                        <View style={{ paddingHorizontal: 10, paddingVertical: 15, marginRight: 15, borderWidth: 1, borderColor: '#219653', borderRadius: 20 }} >
                            <Text style={[myStyle.textBold, { color: '#219653', fontSize: 12, textAlign: 'center' }]} >{this.props.feeDetailList.length != 0 ? this.props.feeDetailList.invoice[0].invoice_status : ''} </Text>
                        </View>

                        <View>
                            <Text style={[myStyle.textNormal, { fontSize: 14, }]} >{this.props.feeDetailList.length != 0 ? this.props.feeDetailList.invoice[0].invoice_number : ''} </Text>
                            <Text style={[myStyle.textNormal, { fontSize: 14, }]}>{this.props.feeDetailList.length != 0 ? this.props.feeDetailList.invoice[0].invoice_date : ''} </Text>

                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, }]} >{'Rs'} {this.props.feeDetailList.length != 0 ? this.props.feeDetailList.invoice[0].invoice_amount : ''} </Text>
                        <Image
                            source={require('../../assets/next_button.png')}
                            style={{ height: 20, width: 20, resizeMode: 'contain' }}
                        />
                    </View>

                </View>

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 20, borderTopWidth: 1, borderTopColor: 'lightgray' }} >

                    <TouchableOpacity
                        onPress={() => this.captureAndShareScreenshot()}
                        style={{ flex: 1, alignItems: 'center', paddingVertical: 8 }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, }]}>{'Share'} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.onView()}
                        style={{ flex: 1, borderLeftWidth: 1, borderColor: 'lightgray', alignItems: 'center', paddingVertical: 8 }} >
                        <Text style={[myStyle.textNormal, { fontSize: 14, }]}>{'View'} </Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }


    render() {

        return (
            <View style={{ backgroundColor: Colors.backgroundColor, flex: 1, }}>

                <FeeScreenHeader
                    onBack={() => this.onBack()}
                    student_profile={{ uri: this.props.students_details.profile_pic }}
                    student_name={this.props.students_details.student_name}
                    school_name={this.props.students_details.school_name}
                    school_address={this.props.students_details.school_address}
                    onFeeStructure={() => this.onFeeStructure()} />


                <FlatList
                    contentContainerStyle={{ paddingBottom: 80 }}
                    data={this.state.fee_data}
                    renderItem={this.renderItem}
                    ListHeaderComponent={this.listHeaderItem}
                    keyExtractor={(item, index) => 'key' + index}
                    showsVerticalScrollIndicator={false}
                />


            </View >

        )
    }
}
const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor,
        justifyContent: 'center',
        alignItems: "center"
    },
    header: {
        height: 80,
        padding: 10,
        paddingHorizontal: 20,
        backgroundColor: Colors.backgroundColor,
        elevation: 3,
        justifyContent: 'space-between',
        flexDirection: 'row',
        alignItems: "center",
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20
    },

    indicatorContainer: {
        flex: 1,
        padding: 20,
    },
    downloadBtn: {
        backgroundColor: '#EEEEEE',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: "baseline",
    },
    labelContainer: {
        marginTop: 5,
        paddingVertical: 20,
        paddingHorizontal: 10,
        marginHorizontal: 20,
        backgroundColor: 'white',
        elevation: 0.5,
        borderRadius: 20,
        alignSelf: 'center',
        // left:-20
    },
    labelText: {
        fontSize: 20,
        color: Colors.headerText,
        fontWeight: 'bold'
    },
    status: {
        fontSize: 15,
        color: Colors.grayColor
    },
    nextBtn: {
        alignSelf: 'flex-end',
        marginBottom: 50
    },
    text: {
        color: 'red',
        fontSize: 18
    }

})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const Jaswant = state.indexReducerJaswant;
    const akshita = state.indexReducerAkshita;

    return {
        feeDetailList: akshita.feeDetailList,
        students_details: common.students_details,

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setFeeDetailList: (list) => setFeeDetailList(list),
            hitFeeStatusApi: (param) => hitFeeStatusApi(param),
            setInvoiceDetails: (list) => setInvoiceDetails(list),
            hitInvoiceApi: (param) => hitInvoiceApi(param)
        },
        dispatch,
    );
};

export const FeeScreen1Memo = connect(mapStateToProps, mapDispatchToProps)(React.memo(FeeScreen1));