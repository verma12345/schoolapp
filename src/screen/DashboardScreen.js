import React, { Component } from "react";
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import Font from "../common/Font";
import { myStyle } from "../common/myStyle";
import { getPrefs } from "../common/Prefs";
import BottomNavigation from "../components/BottomNavigation";
import { Loader } from "../components/Loader";
import { hitGetDashboardApi, hitNotification } from "../redux_store/actions/indexActionApi";
import { setUserId } from "../redux_store/actions/indexActions";
import { setNotification, setSessionYear, setStudentDetails, setStudentDetailsList, setTotalTeacherCount } from "../redux_store/actions/indexActionsJaswant";


class DashboardScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false
        }
    }

    // navigate to StudentDetailsScreen
    onPressStudent = (data_row) => {
        console.log(data_row);
        // return
        this.props.setStudentDetails(data_row)
        this.props.navigation.navigate('StudentDetailsScreen')
    }

   

    // to display student list
    // _renderItem = (dataItem) => {
    //     return (
    //         <StudentDetailsRow
    //             index={dataItem.index}
    //             length={this.props.students_list.length}
    //             data_row={dataItem.item}
    //             onStudentDetails={this.onPressStudent}
    //         // length={this.props.students_list.length - 1}
    //         />
    //     )
    // }

    // row for flat list
    _renderItem = (dataItem) => {
        return (
            <TouchableOpacity
                onPress={() => this.onPressStudent(dataItem.item)}
                style={[myStyle.myshadow2, {
                    flexDirection: 'row',
                    marginHorizontal: 14,
                    flex: 1,
                    marginVertical: 7,
                    paddingHorizontal: 10,
                    paddingVertical: 7,
                    borderRadius: 20,
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                }]} >
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                }}>
                    <Image
                        style={{
                            height: 55,
                            width: 55,
                            resizeMode: 'contain'
                        }}
                        source={{ uri: dataItem.item.profile_pic }}
                    />
                    <View style={{ paddingHorizontal: 15 }} >
                        <Text style={[myStyle.textBold, { fontSize: 14 }]} >
                            {dataItem.item.student_name}
                        </Text>


                        <Text style={[myStyle.textNormal, { color: Colors.textColor, fontSize: 12 }]} >
                            {"class:" + dataItem.item.class}
                        </Text>

                        <Text style={[myStyle.textNormal, { color: Colors.textColor, fontSize: 12 }]} >
                            {"section:" + dataItem.item.section}
                        </Text>
                    </View>
                </View>

                {/* <TouchableOpacity
            onPress={() => this.onAddStudent(dataItem.item)}
            style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end' }}>
            <Image
                style={{ height: 30, width: 30 }}
                source={require('../../assets/plus.png')}
            />
        </TouchableOpacity> */}

            </TouchableOpacity>
        )
    }

    // for heading
    renderHeader = () => {
        return (
            <View style={{ marginTop: 10, paddingHorizontal: 15 }} >
                <Text style={[myStyle.textBold, { fontSize: 22, color: '#000D83' }]}>{"Welcome,"}{ }</Text>
                <Text style={[myStyle.textBold, { fontSize: 15, marginTop: 44, marginBottom: 10, color: '#909090' }]}>{"Student's Details"}</Text>
            </View>
        )
    }

     // hit api get_dashboard
     componentDidMount() {

        getPrefs('user_id').then((value) => {
            if (value != undefined) {
                this.props.setUserId(value)
                let params = {
                    "school": {
                        user_id: value
                    }
                }
                this.setState({ isLoading: true })
                this.props.hitGetDashboardApi(params).then(res => {
                    // debugLog('dashboard', res.school.session_year)
                    this.props.setStudentDetailsList(res.school.student_detail)
                    this.props.setTotalTeacherCount(res.school.total_teachers)
                    // this.props.setSessionYear(res.school.session_year) // bug
                    this.setState({ isLoading: false })

                    this.onNotification()
                })
            }
        })
    }


    // natigate to NotificationScreen


    onNotification = () => {
        let params = {
            "school": {
                "class": 1,
                "session_year": "21-22"
            }
        }

        this.props.hitNotification(params).then(res => {
            this.props.setNotification(res.school.notification)

        })
    }

    // navigate to AddStudentScreen
    onAddStudent = () => {
        this.props.navigation.navigate('AddStudentScreen')
    }

    // main component
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: '#F8FAFF' }}>
                    {/* <DashboardHeader
                        onPressNotification={() => this.onNotification()}
                    /> */}


                    <View style={{
                        height: 50,
                        backgroundColor: Colors.backgroundColor,
                        justifyContent: "space-between",
                        alignItems: "center",
                        flexDirection: 'row',
                        paddingHorizontal: 15
                    }}>
                        <Text style={[myStyle.textBold, { fontSize: 18, fontFamily: Font.Montserrat }]}>
                            {'School'}<Text style={[myStyle.textNormal, { fontSize: 16, fontFamily: Font.Montserrat }]} >{' App'}</Text>
                        </Text>

                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }} >
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('NotificationScreen')
                                }
                                style={{ marginRight: 20, padding: 10 }}
                            >
                                <Image
                                    source={require('../../assets/notification.png')}
                                    style={{ height: 24, width: 24, resizeMode: 'contain' }}
                                />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.onAddStudent()}>
                                <Image
                                    source={require('../../assets/plus.png')}
                                    style={{ height: 24, width: 24, resizeMode: 'contain' }}
                                />
                            </TouchableOpacity>
                        </View>


                    </View>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: 50 }}
                        data={this.props.students_list}
                        keyExtractor={(item, index) => index + 'key'}
                        renderItem={this._renderItem}
                        ListHeaderComponent={this.renderHeader}
                        showsVerticalScrollIndicator={false}
                    />

                </View>

                {/* <NotificationFeeds /> */}
                <BottomNavigation
                    navigation={this.props.navigation}
                    isDashboard={true}
                />

                {this.props.is_loading ? (
                    <View
                        style={{
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#0008',
                            elevation: 10,
                            position: 'absolute',
                        }}>
                        <View style={{ borderRadius: 10, backgroundColor: "white", height: 200, width: 300, justifyContent: "center", alignItems: "center", }}>
                            <ActivityIndicator
                                size={"large"}
                                color={Colors.blue}
                            />
                            <Text style={{ fontSize: 14 }} >{"Please wait for a while..."} </Text>
                        </View>
                    </View>
                ) : null}

                {
                    this.state.isLoading ?
                        <Loader />
                        : null
                }

            </SafeAreaView>

        )
    }
}


const styles = StyleSheet.create({
    container: {

    },
    myshadow: {
        shadowColor: '#0008',
        shadowOffset: {
            height: 1,
            width: 0,
        },
        shadowOpacity: 0.5,
        elevation: 5,
    },

});


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    let jas = state.indexReducerJaswant;

    return {
        isLoading: common.isLoading,
        loginInfo: akshita.loginInfo,
        user_id: common.user_id,
        students_list: common.students_list

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setStudentDetailsList: (list) => setStudentDetailsList(list),

            setStudentDetails: (details) => setStudentDetails(details),
            setUserId: (id) => setUserId(id),
            hitGetDashboardApi: (params) => hitGetDashboardApi(params),
            setTotalTeacherCount: (counts) => setTotalTeacherCount(counts),
            setSessionYear: (year) => setSessionYear(year),
            hitNotification: (param) => hitNotification(param),
            setNotification: (list) => setNotification(list),

        }, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(DashboardScreen);