import React, { Component } from "react";
import { ActivityIndicator, Alert, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import Font from "../common/Font";
import { myStyle } from "../common/myStyle";
import { getPrefs, setPrefs } from "../common/Prefs";
import OTPTextInput from "../components/OTPTextInput";
import { VarificationHeader } from "../components/VarificationHeader";
import { setSmsOtp } from "../redux_store/actions/indexActionAkshita";
import { setSignupMobile } from "../redux_store/actions/indexActions";


class MobileVerficationScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            otp: ''
        }
    }

    componentDidMount() {
        getPrefs('sms_otp').then((value => {
            // console.log(value);
            if (value !== null) {
                this.props.setSmsOtp(value)
            }
        }))

        getPrefs('mobile').then((value => {
            console.log('prefs',value);
            if (value != undefined) {
                this.props.setSignupMobile(value)
            }
        }))
    }

    //on resend btn
    onResendOtp = () => {
        this.props.navigation.navigate('MobileVerficationScreen')
    }

    //alert on OK btn
    showAlert(msg) {
        Alert.alert(
            'Mobile Verification',
            msg,
            [{ text: 'OK', onPress: () => this.props.navigation.navigate('EmailVerficationScreen') }]
        );
    }

    // for the verify mobile api
    verifyOtp = () => {
        if (this.state.otp == '') {
            alert('Please enter otp')
            return;
        }
        if (this.props.sms_otp != this.state.otp) {
            alert('Please enter correct otp')
            return;
        }
        var myHeaders = new Headers();
        var raw = JSON.stringify({
            school: {
                user_id: this.props.user_id,
                mobile: this.props.signup_mobile
            }
        });
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://demoapps.in/school/index.php/user_controller/verify_mobile", requestOptions)
            .then(response => response.json())
            .then(result => {
                if (result.school.status == 1) {
                    setPrefs('mobile_verified', '1')
                    this.showAlert(result.school.msg)
                    console.log(result)
                }
            })
            .catch(error => console.log('error', error));
    }


    _onChange = () => {
        this.props.navigation.navigate('VarificationScreen')
    }
    render() {
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.container}>
                    <ScrollView style={{ marginBottom: 50 }}>
                        <View style={{ alignItems: 'center', paddingTop: 130 }}>
                            <VarificationHeader
                                txtHeading={'Mobile Verification'}
                                txt={'We just sent you an SMS with\n 5-digit code on your Phone Number and Email'}
                            />
                        </View>
                        {/* <View style={{ flex: 3 }}> */}


                        <View style={{ alignItems: 'center' }}>
                            <View style={{ flexDirection: 'row', padding: 20 }}>
                                <Text style={myStyle.textNormal, [{ color: Colors.textColor }]}>+91 {this.props.signup_mobile} </Text>
                                <TouchableOpacity onPress={() => this._onChange()}>
                                    <Text style={[myStyle.textNormal, { color: Colors.blue, fontFamily: Font.Roboto }]}>{'Change'}</Text>
                                </TouchableOpacity>

                            </View>
                            <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{`Enter this code into field below:`}</Text>

                            <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{'OTP: '}{this.props.sms_otp}</Text>


                            <OTPTextInput
                                // value={this.state.otp}
                                onChange={(otp) => {
                                    // debugLog(a)
                                    // this.props.setSmsOtp(otp)
                                    this.setState({
                                        otp: otp
                                    })
                                }}
                            />

                            {/* </View> */}
                            <TouchableOpacity style={[styles.btn, { flexDirection: 'row' }]}
                                onPress={() => {
                                    // this.props.getOTP == '' ? this._onPressGetOtp() : this.verifyAndSignUp()
                                    this.verifyOtp()

                                }}>
                                <Text style={[, myStyle.textBold, { color: Colors.whiteText }]}>{'Verify'}</Text>

                                {this.props.is_loading ?
                                    <ActivityIndicator
                                        size='small'
                                        color='white'
                                    />
                                    : null
                                }
                            </TouchableOpacity>


                            <TouchableOpacity onPress={() => this.onResendOtp()}>
                                <Text style={[myStyle.textBold, { fontSize: 14, paddingTop: 30, color: Colors.blue }]}>{'Resend OTP'}</Text>
                            </TouchableOpacity>
                        </View>
                        {/* </View> */}

                    </ScrollView>

                </View>
            </SafeAreaView>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },

    btn: {
        height: 56,
        width: '80%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.blue,
        marginTop: 45,
        borderRadius: 20,
    },
    txtinput: {
        height: 40,
        height: 65,
        width: 65,
        backgroundColor: Colors.backgroundColor,
        borderRadius: 10,
        paddingLeft: 25,
        fontSize: 30,
        elevation: 5
    }

})


const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita
    const jaswant = state.indexReducerJaswant;


    return {

        is_loading: common.is_loading,
        user_id: common.user_id,
        signup_mobile: common.signup_mobile,
        sms_otp: akshita.sms_otp,
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setSmsOtp: (param) => setSmsOtp(param),
        setSignupMobile: (mobile) => setSignupMobile(mobile),

    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MobileVerficationScreen)