import Geolocation from '@react-native-community/geolocation';
import React, { Component } from 'react';
import { ActivityIndicator, Dimensions, View } from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import Colors from '../common/Colors';
import { debugLog, GOOGLE_MAPS_APIKEY } from '../common/Constants';
import { CustomeMarker } from '../components/CustomeMarker';


const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 0
const LONGITUDE = 0;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


class SchoolDetailsMap extends Component {

    constructor(props) {
        super(props);

        this.state = {
            coordinates: null,
            currentPosition: 0,
            isPopup: false
        };

        this.mapView = null;
    }

    componentDidMount() {

        // to get current position of map
        Geolocation.getCurrentPosition(
            position => {
                this.setState({
                    coordinates: [
                        position.coords,
                    ],
                });

            },
            (error) => console.log(error.message),
            { enableHighAccuracy: true, timeout: 2000, maximumAge: 300 },
        );
        this.watchID = Geolocation.watchPosition(
            position => {
                this.setState({
                    coordinates: [
                        position.coords,
                    ],
                });
                debugLog('watchId:::::::::')
                debugLog(position)
            }
        );
        debugLog(this.state.coordinates)
    }



    componentWillUnmount() {
        Geolocation.clearWatch(this.watchID);
    }


    onMapPress = (e) => {
        this.setState({
            coordinates: [
                ...this.state.coordinates,
                e.nativeEvent.coordinate,
            ],
        });
        debugLog(e.nativeEvent.coordinate)
    }

    // popup for the bus details
    setPopup = () => {
        this.setState({ isPopup: !this.state.isPopup })
    }
    render() {

        return (
            <View style={{ height: width / 2, width: width }} >

                {this.state.coordinates != null ? (this.state.coordinates != null) && (

                    <MapView
                        initialRegion={{
                            latitude: this.state.coordinates[0].latitude,
                            longitude: this.state.coordinates[0].longitude,
                            latitudeDelta: LATITUDE_DELTA,
                            longitudeDelta: LONGITUDE_DELTA,
                        }}
                        style={[{ height: width / 2, width: width }]}
                        ref={c => this.mapView = c}
                        onPress={this.onMapPress}
                    >
                        {this.state.coordinates.map((coordinate, index) =>
                            <MapView.Marker
                                key={`coordinate_${index}`} coordinate={coordinate}
                                pinColor={Colors.red}

                                ref={marker => { this.marker = marker }}
                                flat
                                style={{
                                    transform: [{
                                        rotate: '100deg'
                                    }]
                                }}
                            >
                                {index == 0 ?
                                    <CustomeMarker
                                        index={index}
                                    />
                                    :
                                    <CustomeMarker />
                                }
                            </MapView.Marker>

                        )}



                        {this.state.coordinates != null ? (this.state.coordinates.length >= 2) && (
                            <MapViewDirections
                                origin={this.state.coordinates[0]}
                                // waypoints={ (this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1): null}
                                destination={this.state.coordinates[this.state.coordinates.length - 1]}
                                apikey={GOOGLE_MAPS_APIKEY}
                                strokeWidth={3}
                                strokeColor={Colors.blue}
                                optimizeWaypoints={true}
                                onStart={(params) => {
                                    console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                                }}
                                onReady={result => {
                                    console.log(`Distance: ${result.distance} km`)
                                    console.log(`Duration: ${result.duration} min.`)

                                    this.mapView.fitToCoordinates(result.coordinates, {
                                        edgePadding: {
                                            right: (width / 20),
                                            bottom: (height / 20),
                                            left: (width / 20),
                                            top: (height / 20),
                                        }
                                    });
                                }}
                                onError={(errorMessage) => {
                                    // console.log('GOT AN ERROR');
                                }}
                            />
                        ) : null}
                    </MapView>


                ) :
                    <View style={[{ height: width / 2, width: width }]} >
                        <MapView
                            initialRegion={{
                                latitude: 26.88728259276827,
                                longitude: 80.96731346100569,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            }}

                            style={[{ height: width / 2, width: width }]}
                            ref={c => this.mapView = c}
                        >

                        </MapView>
                        <View style={{ position: 'absolute', elevation: 10, zIndex: 2, alignItems: 'center', justifyContent: 'center' }} >
                            <ActivityIndicator
                                size='large'
                                color={Colors.blue} />
                        </View>
                    </View>
                }

            </View>

        );
    }
}

export default SchoolDetailsMap;