import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, StatusBar } from "react-native";
import { debugLog } from "../common/Constants";
import { getPrefs } from "../common/Prefs";

class SplashScreen extends Component {

  componentDidMount() {
    setTimeout(() => {
     
      getPrefs('user_login').then((value) => {
        if (value != undefined && value == '1') {
          this.props.navigation.navigate('DashboardScreen');
          // this.props.navigation.navigate('AboutQuizScreen');

          return;
        }
      })

      // getPrefs('mobile_verified').then((value) => {
      //   if (value != undefined && value == '1') {
      //     this.props.navigation.navigate('EmailVerficationScreen');
      //     return;
      //   }
      // })

      // getPrefs('user_signup').then((value) => {
      //   if (value != undefined && value == '1') {
      //     this.props.navigation.navigate('MobileVerficationScreen');
      //     return;
      //   }
      // })

      this.props.navigation.navigate('ViewPagerScreen');

    }, 1000)
  }


  onMessage = (msg) => {
    debugLog(msg.nativeEvent.data)
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar
          hidden />
        <ImageBackground source={require('../../assets/splash.png')} style={styles.image}>
        </ImageBackground>

      </SafeAreaView>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0"
  }
});

export default SplashScreen;