import React, { Component } from "react";
import { View, Image, Text, ImageBackground, StyleSheet, SafeAreaView, StatusBar, KeyboardAvoidingView, Platform, Keyboard, TouchableWithoutFeedback, TouchableOpacity, FlatList } from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import { EventHeader } from "../components/EventHeader";
import { setEventDetails, setUpcomingEventList } from "../redux_store/actions/indexActionAkshita";
import { EventRow } from "../row_component/EventRow";

class EventScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            is_upcoming: true,
            is_past: false,

        }
    }


    onPressBack = () => {
        this.props.navigation.goBack()
    }

    //  on upcoming btn, set rest btn dehighlighted 
    onPressUpcoming = () => {
        this.setState({
            is_upcoming: true,
            is_past: false,
        })
    }

    //  on past btn, set rest btn dehighlighted 
    onPressPast = () => {
        this.setState({
            is_upcoming: false,
            is_past: true,
        })
    }

    // on press event info row
    onPressInfo = (data) => {
        this.props.setEventDetails(data)
        this.props.navigation.navigate('EventInfo')
    }

    // display event details
    renderItem = (dataItem) => {
        return (
            <EventRow
                index={dataItem.index}
                data_row={dataItem.item}
                onPress={this.onPressInfo}
            />

        )
    }

    // main component
    render() {


        return (

            <SafeAreaView style={myStyle.container} >
                <StatusBar
                    hidden={false}
                />

                <View style={myStyle.container}>
                    {/* ********************************************UPCOMING EVENTS************************************** */}
                    <View style={{ flex: 1 }} >
                        <EventHeader
                            onPressBack={() => this.onPressBack()}
                            title="Events"
                            onPressUpcoming={() => this.onPressUpcoming()}
                            onPressPast={() => this.onPressPast()}
                            is_upcoming={this.state.is_upcoming}
                            is_past={this.state.is_past}
                        />
                        <FlatList
                            contentContainerStyle={{ paddingBottom: 60 }}
                            data={this.state.is_upcoming ? this.props.eventList[0].upcoming : this.props.eventList[1].past}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => 'key' + index}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>

                </View>




            </SafeAreaView>

        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.backgroundColor
    },
    // i
});

const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;
    const jaswant = state.indexReducerJaswant;

    return {

        eventList: akshita.eventList,
        eventDetails: akshita.eventDetails
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({

        setUpcomingEventList: (eventList) => setUpcomingEventList(eventList),
        setEventDetails: (eventDetails) => setEventDetails(eventDetails)
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EventScreen)
