import React, { Component } from "react";
import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native";
import Colors from "../common/Colors";
import { myStyle } from "../common/myStyle";
import { setPrefs } from "../common/Prefs";

export default class SuccessScreen extends Component {
    constructor(props) {
        super(props)
    }

    
    componentDidMount() {
        setPrefs('user_login','1')

        setTimeout(() => {
            this.props.navigation.navigate('DashboardScreen')
        }, 1000);
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <Image
                        style={{ height: 98.41, width: 98.41 }}
                        source={require('../../assets/check1.png')}

                    />
                    <Text style={[myStyle.textBold, styles.txt]}>{'Completed'}</Text>

                </View>
            </SafeAreaView>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.backgroundColor
    },
    txt: {
        fontSize: 34,
        fontWeight: 'bold',
        paddingTop: 40,
        color: Colors.textColor
    }
})