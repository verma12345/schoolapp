import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

export const MoreNoticationRow = (props) => {
    return (

        <View style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginTop: 7,
            paddingHorizontal: 15,
            paddingVertical: 12,
        }}>
            <View>
                <Text style={[myStyle.textBold, { fontSize: 14 }]}>  {props.notification_title}</Text>
                <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor, paddingTop: 3 }]}>  {props.notification_description}</Text>
            </View>


            <View style={{ flexDirection: "row", alignItems: 'center' }} >
                {
                    props.isToggle ?
                        <TouchableOpacity
                            style={styles.onOff}
                            onPress={props.onPressToggle}>
                            <View style={styles.dot} />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity
                            style={[styles.onOff, { backgroundColor: Colors.blue, justifyContent: 'flex-end' }]}
                            onPress={props.onPressToggle}>
                            <View style={styles.dot} />
                        </TouchableOpacity>
                }
            </View>

        </View>

    )
}

const styles = StyleSheet.create({
    onOff: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'flex-start',
        borderRadius: 20,
        backgroundColor: Colors.grayColor,
        padding: 2,
        width: 32
    },

    dot: {
        height: 11,
        width: 11,
        borderRadius: 30,
        backgroundColor: "white"
    },
})