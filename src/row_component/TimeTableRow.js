import React from 'react'
import { Image, Text, TouchableOpacity, View, StyleSheet, Dimensions, ImageBackground } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

const TimeTableRow = (props) => {
    return (
        props.data_row.subject == "Interval" ?
            <View style={[myStyle.myshadow2, {
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginHorizontal: 20,
                backgroundColor: Colors.whiteText,
                borderRadius: 20,
                marginTop: 10,
                paddingHorizontal: 20,
                paddingVertical: 12
            }]}>
                <View>
                    <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.textColor }]}>
                        {'Lunch Break'}
                    </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>
                        {props.data_row.time_start + 'am'}  {props.data_row.time_end + 'am'}
                    </Text>
                </View>
                <Image
                    source={require('../../assets/lunch_break.png')}
                    style={{ height: 39, width: 38 }}
                />
            </View>
            :
            <View style={{
                flex: 1,
                marginHorizontal: 20,
                backgroundColor: Colors.whiteText,
                borderRadius: 20,
                marginTop: 10,
                elevation: 2
            }}>
                <View style={{ height: 55, justifyContent: 'center', paddingHorizontal: 20, paddingVertical: 7 }}>
                    <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.textColor }]}>{props.data_row.subject} </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor }]}>
                        {props.data_row.time_start + 'am'}  {props.data_row.time_end + 'am'}
                    </Text>
                    {/* <Text style={[myStyle.textNormal, { fontSize: 12, marginTop: 5, color: Colors.grayColor }]}>{props.data_row.subject_timing} </Text> */}

                </View>
                <View style={{ height: 1, width: '100%', backgroundColor: Colors.lightGray }} />
                <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 5, justifyContent: 'space-between', paddingVertical: 7, paddingHorizontal: 20 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            style={{ height: 20, width: 20, borderRadius: 100 }}
                            source={{uri:props.data_row.profile_pic}}
                        />
                        <Text style={[myStyle.textNormal, { fontSize: 12, color: Colors.grayColor, marginLeft: 5 }]}>{props.data_row.teacher} </Text>
                    </View>
                    <Text style={[myStyle.textBold, { fontSize: 12, color: "#333333" }]}>{'Period:'+ props.data_row.period} </Text>
                </View>
            </View>

    )
}

const styles = StyleSheet.create({
    container: {
        height: 105,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        marginHorizontal: 10,
        paddingHorizontal: 5,
        backgroundColor: '#FFFFFF',
        borderRadius: 14,
        marginTop: 18,

    },

});


export default TimeTableRow;