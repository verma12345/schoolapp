import React from 'react';
import { View, Text } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

export const HolidayRow = (props) => {
    console.log(props.data_row.name);
    return (
            <View style={{
                backgroundColor: "#F8FAFF",
                elevation: 1,
                marginHorizontal: 20,
                borderWidth: 1,
                borderColor: Colors.lightGray,
                paddingVertical: 10,
                marginTop: 17,
                paddingHorizontal: 20,
                borderRadius: 20,
                marginBottom: 5

            }}>
                <Text style={[myStyle.textBold, { fontSize: 14, color: Colors.textColor }]}>{props.data_row.name} </Text>
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'

                }} >
                    <Text style={[myStyle.textNormal, { fontSize: 14, color: Colors.grayColor }]}>{props.data_row.festival_date}</Text>
                    <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 14, marginTop: 5 }]}>{props.data_row.festival_day}</Text>
                </View>
            </View>
    )
}

