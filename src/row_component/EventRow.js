import React from 'react';
import { View, TouchableOpacity, Text, Image, StatusBar } from 'react-native';
import Colors from '../common/Colors';
import { debugLog } from '../common/Constants';
import { myStyle } from '../common/myStyle';

export const EventRow = (props) => {
    return (
       
            <TouchableOpacity
                onPress={() => props.onPress(props.data_row)}
                style={[{
                    paddingVertical: 10,
                    marginTop: 14,
                    elevation: 2,
                    marginHorizontal: 15,
                    borderRadius: 20,
                    backgroundColor: Colors.whiteText,
                    paddingHorizontal: 15
                }]}>


                <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                    <View style={{ paddingHorizontal: 18, paddingVertical: 12, backgroundColor: Colors.blue, borderRadius: 23 }}>
                        <Text style={[myStyle.textBold, { fontSize: 16, color: Colors.whiteText }]}>{props.data_row.event_month} </Text>
                        <Text style={[myStyle.textBold, { fontSize: 20, textAlign: 'center', color: Colors.whiteText }]}>{props.data_row.event_date} </Text>
                    </View>

                    <View style={{ paddingLeft: 13, marginLeft: 7 }} >
                        <Text style={[myStyle.textBold, { fontSize: 15, color: Colors.textDarkColor, marginLeft: 2, }]}> {props.data_row.event_name} </Text>
                        <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12, marginVertical: 3, marginLeft: 2, alignSelf: 'baseline', paddingHorizontal: 5 }]}> {props.data_row.event_timing} </Text>
                        <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                            <Image
                                style={{ height: 12, width: 12, resizeMode: 'contain', marginLeft: 7 }}
                                source={require('../../assets/location_red.png')}
                            />
                            <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]}> {props.data_row.event_address} </Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>

    )
}