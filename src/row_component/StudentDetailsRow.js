import React from 'react'
import { Image, Text, TouchableOpacity, View, StyleSheet, Dimensions, ImageBackground } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

const StudentDetailsRow = (props) => {
    return (
        <TouchableOpacity
            onPress={() => props.onStudentDetails(props.data_row)}
            style={[styles.container,myStyle.myshadow,{
                marginBottom: props.index == props.length - 1 ? 300 : 0,

            }]}>


            <ImageBackground
                imageStyle={{ borderRadius: 17 }}
                style={{ height: 52, width: 52, marginLeft: 10 }}
                source={props.data_row.profile}
            >

                <View style={{ width: 17, height: 17, backgroundColor: '#F22A2A', borderRadius: 30, justifyContent: 'center', alignItems: 'center', alignSelf: 'flex-end', right: -5, top: -5 }} >
                    <Text style={{ fontSize: 8, color: 'white' }} >{'12'} </Text>
                </View>

            </ImageBackground>

            <View style={{ flexDirection: 'row',flex:1, paddingHorizontal:20,paddingTop:10,justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{}}>
                    <Text style={[myStyle.textBold, { fontSize: 16 }]}>
                        {props.data_row.name}
                    </Text>
                    <Text style={[myStyle.textNormal, { fontSize: 12,marginTop:4 }]}>
                        {props.data_row.school}
                    </Text>

                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]}>
                            {`Class  ` + props.data_row.class}
                        </Text>
                        <View style={{ height: 7, width: 7, borderRadius: 100, backgroundColor: props.data_row.location_status == 'In Bus' ? Colors.greenColor : Colors.blue, marginLeft: 25 }} />
                        <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12, marginLeft: 10 }]}>
                            {props.data_row.location_status}
                        </Text>
                    </View>


                </View>
                <TouchableOpacity>
                    <Image
                        style={{ height: 15, width: 15, resizeMode: 'contain' }}
                        source={require('../../assets/next_button.png')}
                    />
                </TouchableOpacity>
            </View>

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        // height: 9,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        marginHorizontal: 12,
        paddingHorizontal: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        marginTop: 15,
        paddingBottom: 14,
        elevation:3

    },

});


export default StudentDetailsRow;