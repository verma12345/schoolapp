import React from 'react'
import { Image, Text, TouchableOpacity, View, StyleSheet, Dimensions, ImageBackground } from 'react-native';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';

const ELearningList_Row = (props) => {
    return (
        <TouchableOpacity
            onPress={() => props.onPress(props.dataRow)}
            style={[myStyle.myshadow2, {
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                backgroundColor: Colors.whiteText,
                paddingVertical: 17,
                paddingHorizontal: 20,
                flex: 1,
                marginHorizontal: 12,
                marginVertical: 6,
                borderRadius: 20,
            }]}>

            <View style={{
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'space-between'
            }}>
                <View>
                    <Text style={[myStyle.textBold, { fontSize: 14 }]}>
                        {props.dataRow.subject_name}
                    </Text>

                    <Text style={[myStyle.textNormal, { fontSize: 12 }]}>
                        {props.dataRow.chapters}
                    </Text>

                </View>
                <View>

                    <View
                    //  style={{flexDirection:'row',flex:1,backgroundColor:'yellow',alignItems:'center',justifyContent:'space-between'}}
                    >
                        <Image
                            style={{ height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/next_button.png')}
                        />
                    </View>
                </View>
            </View>

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        // height: 9,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1,
        marginHorizontal: 12,
        paddingHorizontal: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        marginTop: 15,
        paddingBottom: 14,
        elevation: 3

    },

});


export default ELearningList_Row;