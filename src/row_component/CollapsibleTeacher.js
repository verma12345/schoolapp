import React from 'react'
import { FlatList, Image, Text, PixelRatio, TouchableOpacity, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Colors from '../common/Colors';
import { myStyle } from '../common/myStyle';
import { setTeacherDetailsList } from '../redux_store/actions/indexActionsAkshita';


const CollapsibleTeacher = (props) => {

    // const onPressTeacher = (dataItem) => {
    //     alert(dataItem.teacher_name +'\n' +dataItem.subject)
    // }

    const _renderItem = (dataItem) => {
        return (
            // <View style={{ padding: 15, backgroundColor: Colors.backgroundColor }}>
            <TouchableOpacity
                // onPress={() => onPressTeacher(dataItem.item)}
                onPress={() => props.onPress(dataItem.item)}
                style={[myStyle.myshadow2,{
                    flexDirection: 'row',
                    marginHorizontal: 14,
                    marginVertical: 7,
                    paddingHorizontal: 10,
                    paddingVertical: 7,
                    borderRadius: 20,
                    alignItems: 'center',
                    backgroundColor: Colors.whiteText,
                }]} >
                <Image
                    style={{
                        height: 55,
                        width: 55
                    }}
                    source={dataItem.item.teacher_pic}
                />
                <View style={{ paddingHorizontal: 15 }} >
                    <Text style={[myStyle.textBold, { fontSize: 14 }]} >
                        {dataItem.item.teacher_name}
                    </Text>


                    <Text style={[myStyle.textNormal, { color: Colors.grayColor, fontSize: 12 }]} >
                        {dataItem.item.subject}
                    </Text>
                </View>
            </TouchableOpacity>


        )
    }
    return (
        <View
            onPress={() => props.onPress}>



            <FlatList
                data={props.teacher_list}
                renderItem={_renderItem}
                ListHeaderComponent={props.renderHeader}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item, index) => index + 'key'}

            />


        </View>
    )
}






const mapStateToProps = (state) => {
    const common = state.indexReducer;
    const akshita = state.indexReducerAkshita;


    return {
        teacher_list: akshita.teacher_list,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            setTeacherDetailsList: (list) => setTeacherDetailsList(list)
        }, dispatch);

};
export default connect(mapStateToProps, mapDispatchToProps)(CollapsibleTeacher);

